//
//  Program.swift
//  SPN_EMS_Client
//
//  Created by Prateek Kumar on 18/01/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import Foundation
import SPN_EMS

struct Program: Codable {

    let channelID: Int
    let showID: Int
    let pageID: Int
    let name: String
}
