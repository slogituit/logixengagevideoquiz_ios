//
//  ProgramTableViewCell.swift
//  SPN_EMS_Client
//
//  Created by Prateek Kumar on 18/01/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import UIKit

class ProgramTableViewCell: UITableViewCell {

    @IBOutlet weak var programTitleLabel: UILabel!
    @IBOutlet weak var channelIDLabel: UILabel!
    @IBOutlet weak var showIDLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
