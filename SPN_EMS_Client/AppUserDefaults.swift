//
//  AppUserDefaults.swift
//  SPN_EMS_Client
//
//  Created by Prateek Kumar on 22/01/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import Foundation

let programsKey = "Programs"
let programsCountKey = "ProgramsCount"

extension UserDefaults {

    /// Store and Get programs.
    class func getPrograms() -> [Program]? {

        let defaults = UserDefaults.standard
        if let programsData = defaults.object(forKey: programsKey) as? Data {
            let decoder = JSONDecoder()
            if let programs = try? decoder.decode([Program].self, from: programsData) {
                return programs
            }
        }
        return nil
    }

    class func storePrograms(programs: [Program]) {

        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(programs) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: programsKey)
        }
    }

    class func getDefaultProgramCount() -> Int {

        let defaults = UserDefaults.standard
        if let programsCount = defaults.object(forKey: programsCountKey) as? Int {
            return programsCount
        }
        return 0
    }

    class func storeDefaultProgramCount(count: Int) {

        let defaults = UserDefaults.standard
        defaults.set(count, forKey: programsCountKey)
    }
}
