//
//  ViewController.swift
//  SPN_EMS_Client
//
//  Created by Prateek Kumar on 07/08/18.
//  Copyright © 2018 Sourcebits. All rights reserved.
//

import UIKit
import SPN_EMS

class ViewController: UIViewController {

    @IBOutlet weak var programTableView: UITableView!

    let trackingID = "UA-34728540-15"
    var programs = [Program]()
    var details : LoginUserDetails!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        guard let gai = GAI.sharedInstance() else {
            assert(false, "Google Analytics not configured correctly")
            return
        }

        gai.tracker(withTrackingId: trackingID)
        // Optional: automatically report uncaught exceptions.
        gai.trackUncaughtExceptions = true

        // Optional: set Logger to VERBOSE for debug information.
        // Remove before app release.
        gai.logger.logLevel = .verbose;


//        if let programs = UserDefaults.getPrograms() {
//            self.programs = programs
//        } else {
            programs = [ Program(channelID: 6, showID: 50, pageID: 0, name: "Video Quiz"),
                        Program(channelID: 6, showID: 51, pageID: 0, name: "Video quiz with dry run"),
                        Program(channelID: 6, showID: 52, pageID: 0, name: "Video quiz for region Pune"),
                        Program(channelID: 6, showID: 53, pageID: 0, name: "Video quiz for region Mumbai")];
            UserDefaults.storePrograms(programs: programs)
            UserDefaults.storeDefaultProgramCount(count: programs.count)
       // }

        programTableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func addProgramAction(_ sender: Any) {

        let alertController = UIAlertController(title: "Add New Show", message: "", preferredStyle: .alert)

        alertController.addTextField(configurationHandler: { textField in
            textField.placeholder = "Enter Show Name"
        })
        alertController.addTextField(configurationHandler: { textField in
            textField.placeholder = "Enter Channel ID"
            textField.keyboardType = .numberPad
        })
        alertController.addTextField(configurationHandler: { textField in
            textField.placeholder = "Enter Show ID"
            textField.keyboardType = .numberPad
        })
        alertController.addTextField(configurationHandler: { textField in
            textField.placeholder = "Enter Page ID"
            textField.keyboardType = .numberPad
        })
        alertController.addAction(UIAlertAction(title: "Save", style: .default, handler: { [weak self] _ in
            let name = alertController.textFields![0].text ?? ""
            let channelId = alertController.textFields![1].text ?? ""
            let showId = alertController.textFields![2].text ?? ""
            let pageId = alertController.textFields![3].text ?? ""

            if name.isEmpty || channelId.isEmpty || showId.isEmpty || pageId.isEmpty {
                alertController.message = "Please enter valid show details"
                alertController.setValue(NSAttributedString(string: alertController.message!, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium), NSAttributedString.Key.foregroundColor : UIColor.red]), forKey: "attributedMessage")
                self?.present(alertController, animated: true)
            } else if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: channelId)) ||
                !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: showId)) ||
                !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: pageId)) {
                alertController.message = "Please enter valid show details"
                alertController.setValue(NSAttributedString(string: alertController.message!, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium), NSAttributedString.Key.foregroundColor : UIColor.red]), forKey: "attributedMessage")
                self?.present(alertController, animated: true)
            } else {
                let newProgram = Program(channelID: Int(channelId) ?? 0, showID: Int(showId) ?? 0, pageID: Int(pageId) ?? 0, name: name)
                self?.programs.append(newProgram)
                UserDefaults.storePrograms(programs: (self?.programs)!)
                self?.insertRowToProgramTable()
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(alertController, animated: true)
    }

    func insertRowToProgramTable() {

        programTableView.beginUpdates()
        programTableView.insertRows(at: [IndexPath(row: programs.count - 1, section: 0)], with: .automatic)
        programTableView.endUpdates()
    }

    func deleteRowFromProgramTable(index: Int) {

        programTableView.beginUpdates()
        programTableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        programTableView.endUpdates()
    }

    func openEmsSdk(channelID: Int, showID: Int, pageID: Int) {
        
        let clientId = GAI.sharedInstance().defaultTracker.get(kGAIClientId)
        
        // Test Code
        details = LoginUserDetails(id: nil, firstName: "Pratik", lastName: "Bhogaokar", fullName: "Pratik Bhogaokar", profileImageUrl: "http://34.237.136.122/monster.png", email: "pratikb472@gmail.com", isDefaultProfileImage: true)
        
        details.loginType       = 1
        details.gender          = "male"
        details.emailVerified   = true
        details.mobileVerified  = true
        details.socialId        = "67676767676"
        details.dateOfBirth     = "749779200000"
        details.mobileNumber    = "8149186575"
        details.operatorName    = "Airtel"
        details.latitude        = 18.5204
        details.longitude       = 73.8567
        details.appVersion      = "4.1"
        
        let eventId = "" //5cfe4f3f5bd0f6ed69bac3f0" // "" for no events
        let pageId = ""
        let propertyId = ""
        
        let auth_Token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxOTA2MDMwODI2ODI1MTgxIiwidG9rZW4iOiJUYVE3LXV5UEMtN2xwWi1ud0dKLUlXOE8tdWNMTC12bSIsImV4cGlyYXRpb25Nb21lbnQiOiIyMDIwLTA4LTMwVDIxOjQ4OjM2LjAwMFoiLCJpc1Byb2ZpbGVDb21wbGV0ZSI6InRydWUiLCJjaGFubmVsUGFydG5lcklEIjoiTVNNSU5EMSIsImZpcnN0TmFtZSI6Ik1pbmFrc2hpIHZhc2FudCBuaWthbSIsImVtYWlsIjoiIiwic2Vzc2lvbkNyZWF0aW9uVGltZSI6IjIwMTktMDctMTFUMDY6NTI6NDYuOTAxWiIsImlhdCI6MTU2MjgyNzk2NiwiZXhwIjoxNTk4ODI0MDY2fQ.JV5AP6JsNxX1eAuXz1NfOIuKLnr2tu8LAtf4YOhQWy8"
        
        details.authToken = auth_Token
        
        let appDetails = AppDetails(cpCustomerID: "455645", socialID: "8767676887", adID: "123", deviceID: "234567")
        if showID == 50 {
            //Region default
            let isTestEvent = false
            details.appRegion = 0
            LogixEngageMainView.launch(eventID: eventId, isTestEvent: isTestEvent, pageID: pageId, propertyID: propertyId, analyticsClientID: clientId!, appDetails: appDetails, userDetails: details)

        } else if showID == 52 {
            // Region Pune
            let isTestEvent = false
            details.appRegion = 1
            LogixEngageMainView.launch(eventID: eventId, isTestEvent: isTestEvent, pageID: pageId, propertyID: propertyId, analyticsClientID: clientId!, appDetails: appDetails, userDetails: details)
       
        } else if showID == 53 {
            // Region Mumbai
            let isTestEvent = false
            details.appRegion = 3
            LogixEngageMainView.launch(eventID: eventId, isTestEvent: isTestEvent, pageID: pageId, propertyID: propertyId, analyticsClientID: clientId!, appDetails: appDetails, userDetails: details)
            
        } else if showID == 51 {
            self.launchVideoQuizWithTestRun()
        }
    }
    
    func launchVideoQuizWithTestRun(){
        let clientId = GAI.sharedInstance().defaultTracker.get(kGAIClientId)

        let isTestEvent = true
        let eventId = "" //5cfe4f3f5bd0f6ed69bac3f0" // "" for no events
        let pageId = ""
        let propertyId = ""
        
        let appDetails1 = AppDetails(cpCustomerID: "455650", socialID: "8767676887", adID: "123", deviceID: "234567")
        details = LoginUserDetails(id: "5dd77fe5ff8badadd81ef13b", firstName: "IOS", lastName: "Tester", fullName: "Test User 2", profileImageUrl: "http://219.91.251.234/images/jobswoz.png", email: "testuser2@gmail.com", isDefaultProfileImage: true)
        
        details.loginType       = 1
        details.gender          = "male"
        details.emailVerified   = true
        details.mobileVerified  = true
        details.socialId        = "67676767676"
        details.dateOfBirth     = "749779200000"
        details.mobileNumber    = "9705863280"
        details.operatorName    = "Airtel"
        details.latitude        = 12.22242
        details.longitude       = 12.2247
        details.appVersion      = "4.1"
        
        LogixEngageMainView.launch(eventID: eventId, isTestEvent: isTestEvent, pageID: pageId, propertyID: propertyId, analyticsClientID: clientId!, appDetails: appDetails1, userDetails: details)
    }
}

extension ViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return programs.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "\(ProgramTableViewCell.self)", for: indexPath) as! ProgramTableViewCell
        cell.separatorInset = UIEdgeInsets.zero
        let program = programs[indexPath.row]
        cell.programTitleLabel.text = program.name
        cell.channelIDLabel.text = String(program.channelID)
        cell.showIDLabel.text = String(program.showID)
        return cell
    }
}

extension ViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let selectedProgram = programs[indexPath.row]
        openEmsSdk(channelID: selectedProgram.channelID, showID: selectedProgram.showID, pageID: selectedProgram.pageID)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row > UserDefaults.getDefaultProgramCount() - 1 {
            return true
        }
        return false
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {
            programs.remove(at: indexPath.row)
            UserDefaults.storePrograms(programs: programs)
            deleteRowFromProgramTable(index: indexPath.row)
        }
    }
}

