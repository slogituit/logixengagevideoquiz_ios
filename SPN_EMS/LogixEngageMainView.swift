//
//  LogixEngageMainView.swift
//  SPN_EMS
//
//  Created by Apple on 31/08/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import UIKit
import LogixEngageFramework

@objc public class LogixEngageMainView: NSObject {
    
    
    static var eventId: String!
    static var loginUserDetails: LoginUserDetails!
    
    //local prod
    public static var BASE_URL = "https://dua4c71866.execute-api.ap-south-1.amazonaws.com/prod/"
    public static var WEB_SOCKET_URL = "ws://52.66.205.195:8000/"
    public static var Prod_KEY = "eDw4Pcqjtg9wTFVVQyubUaQcmJRQy5sk46TESS7e"
    
    //SonyLiv_prod
//    public static var BASE_URL = "https://videoquiz-prodapi.sonyliv.com/"
//    public static var WEB_SOCKET_URL = "wss://videoquiz.sonyliv.com/"
//    public static var Prod_KEY = "qJsSi4VaKc9BOJ6SwV2Nr5TYPFyQhN3E1KXWxX2h"
    
    //sony dev
//    public static var BASE_URL = "https://drvblt1880.execute-api.ap-south-1.amazonaws.com/sony/"
//    public static var WEB_SOCKET_URL = "ws://alb-sony-683152306.ap-south-1.elb.amazonaws.com"
//    public static var Prod_KEY  = "q64SP1G85aSgqAnsSTCi5OwviEGpCHJ9IbjTU6dj"
  
    //QA Urls
//         public static var BASE_URL = "https://ngbzza4l8l.execute-api.us-east-1.amazonaws.com/qa/"
//        public static var WEB_SOCKET_URL  = "ws://219.91.251.234:8000/"
//        public static var Prod_KEY        = "CkwiGtOy40862H3ypkO8V8G1C7cZFKJWafUGPGkV"
    
    //local Dev
    /*public static var BASE_URL = "https://t04t7g40k5.execute-api.us-east-1.amazonaws.com/dev/"
    public static var WEB_SOCKET_URL = "ws://219.91.251.234:8001/"
    public static var Prod_KEY = "Mv9owrH9Qy6vTeTBqsGey8B6cjzvEuRkaOn5YtAg"*/
    
    @objc public class func launch(eventID: String, isTestEvent: Bool, pageID: String, propertyID: String, analyticsClientID: String, appDetails: AppDetails, userDetails: LoginUserDetails) {
        
        loginUserDetails = userDetails
        let parentAppDetails = appDetails
        parentAppDetails.googleAnalyticsClientID = analyticsClientID
        UserDefaults.storeParentAppDetails(details: parentAppDetails)

        UserDefaults.standard.set(nil, forKey: "liveEventId")
        eventId = eventID
        if eventId != "" {
            UserDefaults.standard.set(eventId, forKey: "liveEventId")
        }
        UserDefaults.standard.set(pageID, forKey: "pageID")
        UserDefaults.standard.set(propertyID, forKey: "propertyID")
        UserDefaults.standard.set(isTestEvent, forKey: "isTestEvent")
        UserDefaults.standard.set(userDetails.appRegion, forKey: "regionNumber")

        let isUserLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        let cpCustomerId   = UserDefaults.getVideoQuizDetails()?.cpCustomerID
        let userId         = UserDefaults.getVideoQuizDetails()?.id
        
        self.startSession()
        if !isUserLoggedIn {
            showActivityIndicator()
            preapareObjectForSignInRequest(appDetails: appDetails, userDetails: userDetails)
        } else if userId == nil || cpCustomerId != appDetails.cpCustomerID {
            showActivityIndicator()
            preapareObjectForSignInRequest(appDetails: appDetails, userDetails: userDetails)
        } else {
            presentHomeView()
        }
    }
    
    class func preapareObjectForSignInRequest(appDetails: AppDetails, userDetails: LoginUserDetails) {
        var signInRequest = SignInRequest()
        signInRequest.setName(name: userDetails.fullName)
        signInRequest.setPhoneNumber(phoneNumber: userDetails.mobileNumber)
        signInRequest.setEmail(email: userDetails.email)
        signInRequest.setGender(gender: userDetails.gender)
        signInRequest.setUniqueId(uniqueId: appDetails.cpCustomerID!)
        //self.signInRequest.setBirthDate(birthDate: <#T##String#>)
        signInRequest.setOsVersion(osVersion: DeviceOS)
        signInRequest.setDeviceType(deviceType: DeviceType)
        signInRequest.setProfilePic(profilePic: userDetails.profileImageUrl)
        
        let lgEngageManager = LGEngageManagerImp.shared
        lgEngageManager.signIn(request: signInRequest, delegate: LogixEngageMainView.self())
    }
    
    /// Getting the Current Top View Controller
    class func getCurrentTopViewController() -> UIViewController? {
        
        guard var topController = UIApplication.shared.keyWindow?.rootViewController else { return nil }
        while topController.presentedViewController != nil {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    /// Presenting View Controller on the Current Top View Controller
    class func presentViewController(_ viewController: UIViewController) {
        
        if let topViewController = getCurrentTopViewController() {
            topViewController.present(viewController, animated: true, completion: nil)
        }
    }
    
    class func presentHomeView() {
        let mainVC = UIStoryboard.init(name: StringConstants().videoQuizStoryboard, bundle: SPN_EMS_Bundle)
        let homeVC = mainVC.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        presentViewController(homeVC)
    }
    
    class func startSession() {
        // start websocket session
        EngageEventHandler.shared.startWebSocketSession(baseUrl: BASE_URL, webSocketUrl: WEB_SOCKET_URL, socketKey: Prod_KEY)
    }
}

//#MARK: Sign In response delegate methods
extension LogixEngageMainView: OnSignInResponseDelegate {
    public func onResponse(response: LoginData?) {
        let authToken = LGEngageUtility.shared.getAuthToken()
        UserDefaults.standard.set(authToken, forKey: Urls.Authorization)
        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
        let appDetails = UserDefaults.getParentAppInfo()
        
        let userDetails = UserDetailsForQuiz(id: response?._id ?? "",
                        fullName        : response?.name  ?? "",
                        profileImageUrl : response?.profilePic ?? "",
                        email           : response?.name  ?? "",
                        mobileNumber    : response?.phoneNumber ?? "",
                        appVersion      : LogixEngageMainView.loginUserDetails.appVersion,
                        advId           : appDetails?.adID ?? "",
                        cpCustomerID    : appDetails?.cpCustomerID ?? "",
                        latitude        : LogixEngageMainView.loginUserDetails?.latitude.description ?? "" ,
                        longitude       : LogixEngageMainView.loginUserDetails.longitude.description ?? "")
        
        UserDefaults.storeVideoQuizDetails(details: userDetails)
        
        hideActivityIndicator()
        LogixEngageMainView.presentHomeView()
    }
    
    public func onFailure(error: String?) {
        //self.view.makeToast(error, duration: 3.0, position: .bottom)
        print("Login falied: " + (error?.description)!)
        hideActivityIndicator()
    }
}
