//
//  SPN_EMS.h
//  SPN_EMS
//
//  Created by Prateek Kumar on 07/08/18.
//  Copyright © 2018 Sourcebits. All rights reserved.
//

#import <UIKit/UIKit.h>


//! Project version number for SPN_EMS.
FOUNDATION_EXPORT double SPN_EMSVersionNumber;

//! Project version string for SPN_EMS.
FOUNDATION_EXPORT const unsigned char SPN_EMSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SPN_EMS/PublicHeader.h>
#import <SPN_EMS/TAGManager.h>
#import <SPN_EMS/TAGContainer.h>
#import <SPN_EMS/TAGContainerOpener.h>
#import <SPN_EMS/TAGDataLayer.h>
#import <SPN_EMS/TAGLogger.h>
