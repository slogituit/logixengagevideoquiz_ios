//
//  LoginUserDetails.swift
//  SPN_EMS
//
//  Created by Apple on 30/07/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import Foundation

@objc public class LoginUserDetails : NSObject {
    
    var id                      = ""
    var firstName               = ""
    var lastName                = ""
    var fullName                = ""
    var profileImageUrl         = ""
    var email                   = ""
    var isDefaultProfileImage   = false
    
    @objc public var gender           = ""
    @objc public var emailVerified    = false
    @objc public var mobileVerified   = false
    @objc public var mobileNumber     = ""
    @objc public var operatorName     = ""
    
    //extra params
    @objc public var loginType        = 0
    @objc public var socialId         = ""
    @objc public var dateOfBirth      = ""
    @objc public var latitude         = 0.0
    @objc public var longitude        = 0.0
    @objc public var authToken        = ""
    @objc public var appVersion       = ""
    @objc public var appRegion        = 0

    
    @objc public init(id: String?, firstName: String?, lastName: String?, fullName: String?, profileImageUrl: String?, email: String?, isDefaultProfileImage: Bool){
        self.id         = id ?? ""
        self.firstName  = firstName ?? ""
        self.lastName   = lastName ?? ""
        self.fullName   = fullName ?? ""
        self.email      = email ?? ""
        self.profileImageUrl        = profileImageUrl ?? ""
        self.isDefaultProfileImage  = isDefaultProfileImage
    }
    
}

