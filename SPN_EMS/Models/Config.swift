//
//  Config.swift
//  SPN_EMS
//
//  Created by Prateek Kumar on 08/08/18.
//  Copyright © 2018 Sourcebits. All rights reserved.
//

import Foundation


struct ConfigDetails: Codable {

    let programId: Int?
    let channelId: Int?
    let pageId: Int?
    let programName: String?
    let description: String?
    let fbLogin: Bool?
    let twitterLogin: Bool?
    let googleLogin: Bool?
    let backgroundImageUrl: String?
    let splashScreenUrl: String?
    let termsOfServiceUrl: String?
    let introVideoUrl: String?
    let verifyMobile: Bool?
    let verifyEmail: Bool?
    let serviceWebUrl: String?
    let mobileNumberRequired: Bool?
    let mobileNumberDisplayRequired: Bool?
    let userConfigList: [UserDetailsConfig]?
    let videoPlayRequired: Bool?
    let videoCloseRequired: Bool?
    let profileCompleteReminder: Bool?
    let profileCompleteReminderCount: Int?
    var userProfile: User?
    let analyticsBaseUrl: String?
    let loginRequired: Bool?
    let segmentRequired: Bool?
    let gaRequired: Bool?
    let evergentRequired: Bool?
    let videoMaxDuration: Int?
    let videoUploadUrl: String?
    let picConsentText: String?
    let termsAndCondText: String?
    let loginPageText: String?
}

struct UserDetailsConfig: Codable {

    let type: Int
    let name: String
    let mandatory: Bool
    let position: Int

}
