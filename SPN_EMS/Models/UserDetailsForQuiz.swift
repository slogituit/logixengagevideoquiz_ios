//
//  UserDetailsForQuiz.swift
//  SPN_EMS
//
//  Created by Apple on 11/09/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//


import Foundation

@objc public class UserDetailsForQuiz : NSObject, NSCoding {
        
    public var id               = ""
    public var fullName         = ""
    public var profileImageUrl  = ""
    public var email            = ""
    public var mobileNumber     = ""
    public var cpCustomerID     = ""
    public var appVersion       = ""
    public var advId            = ""
    public var latitude         = ""
    public var longitude        = ""

    
    @objc public init(id : String?, fullName : String?, profileImageUrl : String?, email : String?, mobileNumber : String?, appVersion : String?, advId : String?, cpCustomerID : String?, latitude: String?, longitude: String?) {
        self.cpCustomerID       = cpCustomerID ?? ""
        self.id                 = id ?? ""
        self.fullName           = fullName ?? ""
        self.profileImageUrl    = profileImageUrl ?? ""
        self.email              = email ?? ""
        self.mobileNumber       = mobileNumber ?? ""
        self.appVersion         = appVersion ?? ""
        self.advId              = advId ?? ""
        self.latitude           = latitude ?? ""
        self.longitude          = longitude ?? ""

    }
    
    public required init?(coder aDecoder: NSCoder) {
        self.id                 = aDecoder.decodeObject(forKey: "id") as? String ?? ""
        self.fullName           = aDecoder.decodeObject(forKey: "fullName") as? String ?? ""
        self.profileImageUrl    = aDecoder.decodeObject(forKey: "profileImageUrl") as? String ?? ""
        self.email              = aDecoder.decodeObject(forKey: "email") as? String ?? ""
        self.mobileNumber       = aDecoder.decodeObject(forKey: "mobileNumber") as? String ?? ""
        self.appVersion         = aDecoder.decodeObject(forKey: "appVersion") as? String ?? ""
        self.advId              = aDecoder.decodeObject(forKey: "advId") as? String ?? ""
        self.cpCustomerID       = aDecoder.decodeObject(forKey: "cpCustomerID") as? String ?? ""
        self.latitude           = aDecoder.decodeObject(forKey: "latitude") as? String ?? ""
        self.longitude          = aDecoder.decodeObject(forKey: "longitude") as? String ?? ""
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.cpCustomerID, forKey: "cpCustomerID")
        aCoder.encode(self.fullName, forKey: "fullName")
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.advId, forKey: "advId")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.mobileNumber, forKey: "mobileNumber")
        aCoder.encode(self.appVersion, forKey: "appVersion")
        aCoder.encode(self.profileImageUrl, forKey: "profileImageUrl")
        aCoder.encode(self.latitude, forKey: "latitude")
        aCoder.encode(self.longitude, forKey: "longitude")

    }
}
