//
//  AppDetails.swift
//  SPN_EMS
//
//  Created by Ganesh Kumar on 16/08/18.
//  Copyright © 2018 Sourcebits. All rights reserved.
//

import Foundation

@objc public class AppDetails: NSObject, NSCoding {

    let cpCustomerID: String?
    let socialID: String?
    let adID: String?
    let deviceID: String?
    var anonymousID: String = ""
    var parentAppID: String = ""
    var googleAnalyticsClientID: String = ""
    var region: String = ""

    @objc public init(cpCustomerID : String?, socialID : String?, adID : String?, deviceID : String?) {
        self.cpCustomerID = cpCustomerID
        self.socialID = socialID
        self.adID = adID
        self.deviceID = deviceID
    }

    required public init(coder aDecoder: NSCoder) {
        self.cpCustomerID = aDecoder.decodeObject(forKey: "cpCustomerID") as? String
        self.socialID = aDecoder.decodeObject(forKey: "socialID") as? String
        self.adID = aDecoder.decodeObject(forKey: "adID") as? String
        self.deviceID = aDecoder.decodeObject(forKey: "deviceID") as? String
        self.anonymousID = aDecoder.decodeObject(forKey: "anonymousID") as! String
        self.parentAppID = aDecoder.decodeObject(forKey: "parentAppID") as! String
        self.googleAnalyticsClientID = aDecoder.decodeObject(forKey: "googleAnalyticsClientID") as! String
    }

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.cpCustomerID, forKey: "cpCustomerID")
        aCoder.encode(self.socialID, forKey: "socialID")
        aCoder.encode(self.adID, forKey: "adID")
        aCoder.encode(self.deviceID, forKey: "deviceID")
        aCoder.encode(self.anonymousID, forKey: "anonymousID")
        aCoder.encode(self.parentAppID, forKey: "parentAppID")
        aCoder.encode(self.googleAnalyticsClientID, forKey: "googleAnalyticsClientID")
    }
}
