//
//  User.swift
//  SPN_EMS
//
//  Created by Prateek Kumar on 08/08/18.
//  Copyright © 2018 Sourcebits. All rights reserved.
//

import Foundation


struct User: Codable {

    var profileId: Int
    var authToken: String?
    var loginType: Int?
    var slAccountId: String?
    var parentAppUserId: String?
    var firstName: String?
    var lastName: String?
    var name: String?
    var dateOfBirth: String?
    var dobString: String?
    var gender: String?
    var emailId: String?
    var emailVerified: Int?
    var mobileNumber: String?
    var mobileVerified: Int?
    var description: String?
    var parentAppId: String?
    var profilePicUrl: String?
    var status: Int?
    var picApproved: Int?
    var ageRange: String?
    var relationShipStatus: String?
    var state: String?
    var city: String?
    var defaultImage: Bool?
}

struct LocalUser: Codable {

    var profileId: Int
    var authToken: String
    var name: String?
    var loginType: Int?
    var socialLoginId: String?
}
