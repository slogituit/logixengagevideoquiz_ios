//
//  AppDelegate.swift
//  LogixEngage
//
//  Created by Abbas on 06/05/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import LogixEngageFramework

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var reachability:Reachability!;

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.reachability = Reachability()
        do
        {
            try reachability?.startNotifier()
        }
        catch
        {
            print( "ERROR: Could not start reachability notifier." )
        }
        IQKeyboardManager.shared.enable = true
        UIApplication.shared.statusBarStyle = .lightContent
        self.splashScreen()
        // start websocket session
        EngageEventHandler.shared.startWebSocketSession()
      
        return true
        
    }
    class func sharedAppDelegate() -> AppDelegate?{
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    private func splashScreen() {
        let launchscreenVC = UIStoryboard.init(name: "LaunchScreen", bundle: nil)
        let rootVC = launchscreenVC.instantiateViewController(withIdentifier: "splashController")
        
        self.window?.rootViewController = rootVC
        self.window?.makeKeyAndVisible()
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(dismissSplashController), userInfo: nil, repeats: false)
    }
    
    @objc func dismissSplashController() {
            // goto home vc
        if UserDefaults.standard.string(forKey: "userId") != nil {
            let storyboard = UIStoryboard.init(name: StringConstants().logix_storyboard, bundle: nil)
            let rootVC = storyboard.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
           // let rootVC = storyboard.instantiateViewController(withIdentifier: "QuestionViewController") as! QuestionViewController
            self.window?.rootViewController = rootVC
            self.window?.makeKeyAndVisible()
        } else {
            let mainVC = UIStoryboard.init(name: StringConstants().logix_storyboard, bundle: nil)
            let rootVC = mainVC.instantiateViewController(withIdentifier: "LoginViewController") //LoginViewController
            self.window?.rootViewController = rootVC
            self.window?.makeKeyAndVisible()
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is= about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
}

