//
//  SegmentAnalytics.swift
//  SPN_EMS
//
//  Created by Abbas's Mac Mini on 09/09/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import Foundation
import Analytics

public class SegmentAnalytics: NSObject {

    public static let shared = SegmentAnalytics()
    public override init() {}
    
    // When user lands on the quiz page.
    public func quizEntry(userId: String, cpCustomerId: String, appVersion: String, advId: String, eventId: String, eventName: String, entryPoint: String, latitude: String , longitude: String) {

        let dict: [String: Any] = [

            SAConstant.SEGMENT_ENTRY_POINT      : entryPoint,
            SAConstant.SEGMENT_QUIZ_PAGE_ID     : eventId,
            SAConstant.SEGMENT_QUIZ_PAGE_NAME   : eventName,
            SAConstant.SEGMENT_PLATFORM         : SAConstant.SEGMENT_PLATFORM_VALUE,
            SAConstant.SEGMENT_APP_VERSION      : appVersion,
            SAConstant.SEGMENT_ADV_ID           : advId,
            SAConstant.SEGMENT_TV_CHANNEL       : SAConstant.SEGMENT_CHANNEL_VALUE,
            SAConstant.SEGMENT_USER_ID          : userId,
            SAConstant.SEGMENT_CPID             : cpCustomerId,
            SAConstant.SEGMENT_USER_ACCOUNT_STATUS : "",
            SAConstant.GA_USER_ID               : "",
            SAConstant.SEGMENT_USER_LOCATION    : latitude + " , " + longitude
        ]
        
        SEGAnalytics.shared()?.track(SAConstant.SEGMENT_EVENT_QUIZ_ENTRY, properties: dict)
    }
    
    // When user submits an answer.
    public func quizAnswer(userId: String, cpCustomerId: String, appVersion: String, advId: String, eventId: String, eventName: String, quizType: String, answerSubmitted: String, answerCorrect: String, timeToAnswer: String, latitude: String , longitude: String) {

        let dict: [String: Any] = [

            SAConstant.SEGMENT_QUIZ_NAME        : eventName,
            SAConstant.SEGMENT_QUIZ_TYPE        : quizType,
            SAConstant.SEGMENT_QUIZ_ID          : eventId,
            SAConstant.SEGMENT_ANSWER_SUBMITTED : answerSubmitted,
            SAConstant.SEGMENT_ANSWER_CORRECT   : answerCorrect,
            SAConstant.SEGMENT_TIME_TO_ANSWER   : timeToAnswer,
            SAConstant.SEGMENT_PLATFORM         : SAConstant.SEGMENT_PLATFORM_VALUE,
            SAConstant.SEGMENT_APP_VERSION      : appVersion,
            SAConstant.SEGMENT_ADV_ID           : advId,
            SAConstant.SEGMENT_TV_CHANNEL       : SAConstant.SEGMENT_CHANNEL_VALUE,
            SAConstant.SEGMENT_USER_ID          : userId,
            SAConstant.SEGMENT_CPID             : cpCustomerId,
            SAConstant.SEGMENT_USER_ACCOUNT_STATUS :  "",
            SAConstant.GA_USER_ID               :  "",
            SAConstant.SEGMENT_USER_LOCATION    : latitude + " , " + longitude
        ]

        SEGAnalytics.shared()?.track(SAConstant.SEGMENT_EVENT_QUIZ_ANSWER, properties: dict)
        
    }

    // When user clicks on any quiz from the listing page
    public func quizClick(userId: String, cpCustomerId: String, appVersion: String, advId: String, eventId: String, eventName: String, isLive: String, latitude: String , longitude: String) {

        let dict: [String: Any] = [

            SAConstant.SEGMENT_CLIENT_ID            : "",
            SAConstant.SEGMENT_QUIZ_NAME            : eventName,
            SAConstant.SEGMENT_QUIZ_ID              : eventId,
            SAConstant.SEGMENT_PLATFORM             : SAConstant.SEGMENT_PLATFORM_VALUE,
            SAConstant.SEGMENT_APP_VERSION          : appVersion,
            SAConstant.SEGMENT_ADV_ID               : advId,
            SAConstant.SEGMENT_TV_CHANNEL           : SAConstant.SEGMENT_CHANNEL_VALUE,
            SAConstant.SEGMENT_USER_ID              : userId,
            SAConstant.SEGMENT_QUIZ_LIVE            : isLive,
            SAConstant.SEGMENT_CPID                 : cpCustomerId,
            SAConstant.SEGMENT_USER_ACCOUNT_STATUS  : "",
            SAConstant.GA_USER_ID                   : "",
            SAConstant.SEGMENT_USER_LOCATION        : latitude + " , " + longitude
        ]
        
        SEGAnalytics.shared()?.track(SAConstant.SEGMENT_EVENT_QUIZ_CLICK, properties: dict)
    }
    
    // When user Exits from Quiz
    public func quizExit(userId: String, cpCustomerId: String, appVersion: String, advId: String, eventId: String, eventName: String, quizExitPage: String, quizExitTime: String, quizSessionDuration: String, latitude: String , longitude: String) {

        let dict: [String: Any] = [

            SAConstant.SEGMENT_CLIENT_ID            : "",
            SAConstant.SEGMENT_QUIZ_EXIT_TIME       : quizExitTime,
            SAConstant.SEGMENT_QUIZ_EXIT_PAGE       : quizExitPage,
            SAConstant.SEGMENT_QUIZ_SESSION_DURATION : quizSessionDuration,
            SAConstant.SEGMENT_PLATFORM             : SAConstant.SEGMENT_PLATFORM_VALUE,
            SAConstant.SEGMENT_APP_VERSION          : appVersion,
            SAConstant.SEGMENT_ADV_ID               : advId,
            SAConstant.SEGMENT_TV_CHANNEL           : SAConstant.SEGMENT_CHANNEL_VALUE,
            SAConstant.SEGMENT_USER_ID              : userId,
            SAConstant.SEGMENT_CPID                 : cpCustomerId,
            SAConstant.SEGMENT_USER_ACCOUNT_STATUS  : "",
            SAConstant.GA_USER_ID                   : "",
            SAConstant.SEGMENT_USER_LOCATION        : latitude + " , " + longitude
        ]
        
        SEGAnalytics.shared()?.track(SAConstant.SEGMENT_EVENT_QUIZ_EXIT, properties: dict)
    }

    // When user Clicks on Leaderboard
    public func leaderboardClick(userId: String, cpCustomerId: String, appVersion: String, advId: String, latitude: String , longitude: String) {

        let dict: [String: Any] = [

            SAConstant.SEGMENT_CLIENT_ID            : "",
            SAConstant.SEGMENT_PLATFORM             : SAConstant.SEGMENT_PLATFORM_VALUE,
            SAConstant.SEGMENT_APP_VERSION          : appVersion,
            SAConstant.SEGMENT_ADV_ID               : advId,
            SAConstant.SEGMENT_TV_CHANNEL           : SAConstant.SEGMENT_CHANNEL_VALUE,
            SAConstant.SEGMENT_USER_ID              : userId,
            SAConstant.SEGMENT_CPID                 : cpCustomerId,
            SAConstant.SEGMENT_USER_ACCOUNT_STATUS  : "",
            SAConstant.GA_USER_ID                   : "",
            SAConstant.SEGMENT_USER_LOCATION        : latitude + " , " + longitude
        ]
        
        SEGAnalytics.shared()?.track(SAConstant.SEGMENT_EVENT_LEADERBOARD_CLICK, properties: dict)
    }

    // When user Clicks on Leaderboard tab
    public func leaderboardView(userId: String, cpCustomerId: String, appVersion: String, advId: String, tabName: String, scrolls: String, latitude: String , longitude: String) {

        let dict: [String: Any] = [

            SAConstant.SEGMENT_CLIENT_ID            : "",
            SAConstant.SEGMENT_TAB_NAME             : tabName,
            SAConstant.SEGMENT_SCROLLS              : scrolls,
            SAConstant.SEGMENT_PLATFORM             : SAConstant.SEGMENT_PLATFORM_VALUE,
            SAConstant.SEGMENT_APP_VERSION          : appVersion,
            SAConstant.SEGMENT_ADV_ID               : advId,
            SAConstant.SEGMENT_TV_CHANNEL           : SAConstant.SEGMENT_CHANNEL_VALUE,
            SAConstant.SEGMENT_USER_ID              : userId,
            SAConstant.SEGMENT_CPID                 : cpCustomerId,
            SAConstant.SEGMENT_USER_ACCOUNT_STATUS  : "",
            SAConstant.GA_USER_ID                   : "",
            SAConstant.SEGMENT_USER_LOCATION        : latitude + " , " + longitude
        ]

        SEGAnalytics.shared()?.track(SAConstant.SEGMENT_EVENT_LEADERBOARD_VIEW, properties: dict)
    }

    // When user uses Lifeline
    public func lifelineClick(userId: String, cpCustomerId: String, appVersion: String, advId: String, eventId: String, eventName: String, latitude: String , longitude: String) {
        
        let dict: [String: Any] = [

            SAConstant.SEGMENT_CLIENT_ID            : "",
            SAConstant.SEGMENT_QUIZ_NAME            : eventName,
            SAConstant.SEGMENT_QUIZ_ID              : eventId,
            SAConstant.SEGMENT_PLATFORM             : SAConstant.SEGMENT_PLATFORM_VALUE,
            SAConstant.SEGMENT_APP_VERSION          : appVersion,
            SAConstant.SEGMENT_ADV_ID               : advId,
            SAConstant.SEGMENT_TV_CHANNEL           : SAConstant.SEGMENT_CHANNEL_VALUE,
            SAConstant.SEGMENT_USER_ID              : userId,
            SAConstant.SEGMENT_CPID                 : cpCustomerId,
            SAConstant.SEGMENT_USER_ACCOUNT_STATUS  : "",
            SAConstant.GA_USER_ID                   : "",
            SAConstant.SEGMENT_USER_LOCATION        : latitude + " , " + longitude
        ]
        
        SEGAnalytics.shared()?.track(SAConstant.SEGMENT_EVENT_LIFELINE_CLICK, properties: dict)
    }

    // When user Clicks on Continue Button
    public func continueButtonClick(userId : String,cpCustomerId : String,appVersion : String,advId : String,eventId : String,eventName : String,questionNo : String, latitude: String, longitude: String) {
        
        let dict: [String: Any] = [
            SAConstant.SEGMENT_QUIZ_NAME     : eventName,
            SAConstant.SEGMENT_QUIZ_ID       : eventId,
            SAConstant.SEGMENT_APP_VERSION   : appVersion,
            SAConstant.SEGMENT_CPID          : cpCustomerId,
            SAConstant.SEGMENT_ADV_ID        : advId,
            SAConstant.SEGMENT_TV_CHANNEL    : SAConstant.SEGMENT_CHANNEL_VALUE,
            SAConstant.SEGMENT_PLATFORM      : SAConstant.SEGMENT_PLATFORM_VALUE,
            SAConstant.SEGMENT_USER_ID       : userId,
            SAConstant.SEGMENT_USER_LOCATION : latitude + " , " + longitude,
            SAConstant.SEGMENT_QUESTION_NO   : questionNo,
            ]
        
    SEGAnalytics.shared()?.track(SAConstant.SEGMENT_EVENT_CONTINUE_BUTTON_CLICK, properties: dict)
    }
}
