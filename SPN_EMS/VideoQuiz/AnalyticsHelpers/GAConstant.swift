//
//  GAConstant.swift
//  SPN_EMS
//
//  Created by Abbas's Mac Mini on 09/09/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import Foundation

struct GAConstant {
//    private init()
    
    static let event                 = "event"
    static let chromecast            = "Chromecast"
    static let timestamp             = "timestamp"
    static let subscriptionStatus    = "subscription_status"
    static let version               = "version"
    static let advId                 = "adv_id"
    static let cpId                  = "CPID"
    static let appName               = "AppName"
    static let tvChannel             = "TVChannel"
    static let city                  = "City"
    static let userId                = "user_id"
    static let platform              = "platform"
    static let quizPageId            = "quiz_page_id"
    static let quizPageName          = "quiz_page_name"
    
    // Quiz Entry
    static let eventQuizEntry        = "quiz_entry"
    static let entryPoint            = "entry_point"
    
    // Submit Answer
    static let eventQuizAnswer       = "quiz_answer"
    static let quizId                = "quiz_id"
    static let quizName              = "quiz_name"
    static let quizType              = "quiz_type"
    static let answerSubmitted       = "answer_submitted"
    static let timeToAnswer          = "time_to_answer"
    static let answerCorrect         = "correct"
    
    // Quiz Click
    static let eventQuizClick        = "quiz_click"
    static let question_no           = "question_no"
    
    // Quiz Exit
    static let eventQuizExit         = "quiz_exit"
    static let quizExitTime          = "exit_time"
    static let quizSessionDuration   = "quiz_session_duration"
    static let quizExitPage          = "exit_page"
    
    // Leaderboard View
    static let eventLeaderboardView  = "leaderboard_view"
    static let tabName               = "tab_name"
    static let scrolls               = "scrolls"
    
    // Leaderboard Click
    static let eventLeaderboardClick = "leaderboard_click"
    
    // LifeLine Click
    static let eventLifelineClick    = "lifeline_click"
    
    //continue playing
    static let continuePlayingClick    = "continue_playing_click"
    
    static let platformValue         = "ios"
    static let tvChannelValue        = "ios"
}
