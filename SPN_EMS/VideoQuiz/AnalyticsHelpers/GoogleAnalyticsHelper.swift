//
//  GoogleAnalyticsHelper.swift
//  SPN_EMS
//
//  Created by Abbas's Mac Mini on 09/09/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import Foundation

public class GoogleAnalyticsHelper: NSObject {

    public static let shared = GoogleAnalyticsHelper()
    public override init() {}

//     When user lands on the quiz page.

    public func quizEntry(userId: String, cpCustomerId: String, appVersion: String, advId: String, eventId: String, eventName: String, entryPoint: String) {
        
        let dataLayer: TAGDataLayer = TAGManager.instance().dataLayer

        let dict: [String: Any] = [
            GAConstant.event: GAConstant.eventQuizEntry,
            GAConstant.entryPoint: entryPoint,
            GAConstant.quizPageId: eventId,
            GAConstant.quizPageName: eventName,
            GAConstant.platform: GAConstant.platformValue,
            GAConstant.version: appVersion,
            GAConstant.advId: advId,
            GAConstant.city: "",
            GAConstant.userId: userId,
            GAConstant.cpId: cpCustomerId,
            GAConstant.timestamp: getTimeStamp(),
            GAConstant.appName: "SonyLiv Video Quiz",
            GAConstant.chromecast: "",
            GAConstant.subscriptionStatus: ""
        ]
       // print("google analytics " + GAConstant.eventQuizEntry + "\(dict as Any)")
        dataLayer.push(dict)
    }

    // When user submits an answer.
    public func quizAnswer(userId: String, cpCustomerId: String, appVersion: String, advId: String, eventId: String, eventName: String, quizType: String, answerSubmitted: String, answerCorrect: String, timeToAnswer: String) {
        
        let dataLayer: TAGDataLayer = TAGManager.instance().dataLayer

        let dict: [String: Any] = [
            GAConstant.event: GAConstant.eventQuizAnswer,
            GAConstant.quizName: eventName,
            GAConstant.quizId: eventId,
            GAConstant.answerSubmitted: answerSubmitted,
            GAConstant.answerCorrect: answerCorrect,
            GAConstant.timeToAnswer: timeToAnswer,
            GAConstant.platform: GAConstant.platformValue,
            GAConstant.version: appVersion,
            GAConstant.advId: advId,
            GAConstant.city: "",
            GAConstant.userId: userId,
            GAConstant.cpId: cpCustomerId,
            GAConstant.timestamp: getTimeStamp(),
            GAConstant.quizType: quizType,
            GAConstant.appName: "SonyLiv Video Quiz",
            GAConstant.chromecast: "",
            GAConstant.subscriptionStatus: ""
        ]
       // print("google analytics " + GAConstant.eventQuizAnswer + "\(dict as Any)")
        dataLayer.push(dict)
    }

//     When user clicks on any quiz from the listing page
    public func quizClick(userId: String, cpCustomerId: String, appVersion: String, advId: String, eventId: String, eventName: String) {
        let dataLayer: TAGDataLayer = TAGManager.instance().dataLayer

        let dict: [String: Any] = [
            GAConstant.event: GAConstant.eventQuizClick,
            GAConstant.timestamp: getTimeStamp(),
            GAConstant.quizName: eventName,
            GAConstant.quizId: eventId,
            GAConstant.version: appVersion,
            GAConstant.advId: advId,
            GAConstant.cpId: cpCustomerId,
            GAConstant.appName: "SonyLiv Video Quiz",
            GAConstant.platform: GAConstant.platformValue,
            GAConstant.userId: userId,
            GAConstant.city: "",
            GAConstant.chromecast: "",
            GAConstant.subscriptionStatus: ""
        ]
       // print("google analytics " + GAConstant.eventQuizClick + "\(dict as Any)")
        dataLayer.push(dict)
    }

//     When user Exits from Quiz
    public func quizExit(userId: String, cpCustomerId: String, appVersion: String, advId: String, eventId: String, eventName: String, quizExitPage: String, quizExitTime: String, quizSessionDuration: String, quizType: String) {
        let dataLayer: TAGDataLayer = TAGManager.instance().dataLayer

        let dict: [String: Any] = [
            GAConstant.event: GAConstant.eventQuizExit,
            GAConstant.timestamp: getTimeStamp(),
            GAConstant.quizName: eventName,
            GAConstant.quizId: eventId,
            GAConstant.quizType: quizType,
            GAConstant.version: appVersion,
            GAConstant.advId: advId,
            GAConstant.cpId: cpCustomerId,
            GAConstant.appName: "SonyLiv Video Quiz",
            GAConstant.platform : GAConstant.platformValue,
            GAConstant.userId: userId,
            GAConstant.quizExitPage: quizExitPage,
            GAConstant.quizExitTime: getTimeStamp(),
            GAConstant.quizSessionDuration: quizSessionDuration,
            GAConstant.city: "",
            GAConstant.chromecast: "",
            GAConstant.subscriptionStatus: ""
        ]
        //print("google analytics " + GAConstant.eventQuizExit + "\(dict as Any)")
        dataLayer.push(dict)
    }

//     When user Clicks on Leaderboard
    public func leaderboardClick(userId: String, cpCustomerId: String, appVersion: String, advId: String) {
        let dataLayer: TAGDataLayer = TAGManager.instance().dataLayer

        let dict: [String: Any] = [
            GAConstant.event: GAConstant.eventLeaderboardClick,
            GAConstant.timestamp: getTimeStamp(),
            GAConstant.version: appVersion,
            GAConstant.advId: advId,
            GAConstant.cpId: cpCustomerId,
            GAConstant.appName: "SonyLiv Video Quiz",
            GAConstant.platform: GAConstant.platformValue,
            GAConstant.userId: userId,
            GAConstant.city: "",
            GAConstant.chromecast: "",
            GAConstant.subscriptionStatus: ""
        ]
       // print("google analytics " + GAConstant.eventLeaderboardClick + "\(dict as Any)")
        dataLayer.push(dict)
    }

//     When user Clicks on Leaderboard tab
    public func leaderboardView(userId: String, cpCustomerId: String, appVersion: String, advId: String, tabName: String, scrolls: String) {
        let dataLayer: TAGDataLayer = TAGManager.instance().dataLayer

        let dict: [String: Any] = [
            GAConstant.event: GAConstant.eventLeaderboardView,
            GAConstant.timestamp: getTimeStamp(),
            GAConstant.version: appVersion,
            GAConstant.advId: advId,
            GAConstant.cpId: cpCustomerId,
            GAConstant.appName: "SonyLiv Video Quiz",
            GAConstant.platform: GAConstant.platformValue,
            GAConstant.userId: userId,
            GAConstant.tabName: tabName,
            GAConstant.scrolls: scrolls,
            GAConstant.city: "",
            GAConstant.chromecast: "",
            GAConstant.subscriptionStatus: ""
        ]
        // print("google analytics " + GAConstant.eventLeaderboardView + "\(dict as Any)")
        dataLayer.push(dict)
    }

//     When user uses Lifeline
    public func lifelineClick(userId: String, cpCustomerId: String, appVersion: String, advId: String, eventId: String, eventName: String) {
        let dataLayer: TAGDataLayer = TAGManager.instance().dataLayer

        let dict: [String: Any] = [
            GAConstant.event: GAConstant.eventLifelineClick,
            GAConstant.timestamp: getTimeStamp(),
            GAConstant.quizId: eventId,
            GAConstant.quizName: eventName,
            GAConstant.version: appVersion,
            GAConstant.advId: advId,
            GAConstant.cpId: cpCustomerId,
            GAConstant.appName: "SonyLiv Video Quiz",
            GAConstant.platform: GAConstant.platformValue,
            GAConstant.userId: userId,
            GAConstant.city: "",
            GAConstant.chromecast: "",
            GAConstant.subscriptionStatus: ""
        ]
         // print("google analytics " + GAConstant.eventLifelineClick + "\(dict as Any)")
        dataLayer.push(dict)
    }
    
    //when user clicks continue playing
    public func continuePlayingClick(userId : String,cpCustomerId : String,appVersion : String,advId :  String,eventId : String,eventName : String,questionNo : String) {
        
    let dataLayer: TAGDataLayer = TAGManager.instance().dataLayer
        let dict: [String: Any] = [
            GAConstant.event: GAConstant.continuePlayingClick,
            GAConstant.quizName : eventName,
            GAConstant.timestamp : getTimeStamp(),
            GAConstant.quizId : eventId,
            GAConstant.version: appVersion,
            GAConstant.advId: advId,
            GAConstant.cpId: cpCustomerId,
            GAConstant.appName: "SonyLiv Video Quiz",
            GAConstant.platform: GAConstant.platformValue,
            GAConstant.userId: userId,
            GAConstant.question_no : questionNo,
            GAConstant.city: "",
            GAConstant.chromecast: "",
            GAConstant.subscriptionStatus: ""
        ]
      //  print("google analytics " + eventName + "\(dict as Any)")
        dataLayer.push(dict)
        
    }

    private func getTimeStamp() -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return df.string(from: Date())
    }
}
