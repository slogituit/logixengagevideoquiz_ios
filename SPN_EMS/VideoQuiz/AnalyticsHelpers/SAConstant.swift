//
//  SAConstant.swift
//  SPN_EMS
//
//  Created by Abbas's Mac Mini on 09/09/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import Foundation

struct SAConstant {
    private init() {}
    
    static let SEGMENT_APP_VERSION             = "app_version"
    static let SEGMENT_ADV_ID                  = "advertising_id"
    static let SEGMENT_CPID                    = "cp_customer_id"
    static let SEGMENT_TV_CHANNEL              = "channel"
    static let SEGMENT_USER_ID                 = "user_id"
    static let SEGMENT_PLATFORM                = "platform"
    static let SEGMENT_QUIZ_PAGE_ID            = "quiz_page_id"
    static let SEGMENT_QUIZ_PAGE_NAME          = "quiz_page_name"
    static let SEGMENT_USER_LOCATION           = "user_loc"
    static let SEGMENT_USER_ACCOUNT_STATUS     = "user_ac_status"
    static let GA_USER_ID                      = "GA_User_Id"
    static let SEGMENT_CLIENT_ID               = "Client_Id"
    static let SEGMENT_QUESTION_NO             = "question_no"
    
    // Quiz Entry
    static let SEGMENT_EVENT_QUIZ_ENTRY        = "quiz_entry"
    static let SEGMENT_ENTRY_POINT             = "entry_point"
    
    // Submit Answer
    static let SEGMENT_EVENT_QUIZ_ANSWER       = "quiz_answer"
    static let SEGMENT_QUIZ_ID                 = "quiz_id"
    static let SEGMENT_QUIZ_NAME               = "quiz_name"
    static let SEGMENT_QUIZ_TYPE               = "quiz_type"
    static let SEGMENT_ANSWER_SUBMITTED        = "answer_submitted"
    static let SEGMENT_TIME_TO_ANSWER          = "time_to_answer"
    static let SEGMENT_ANSWER_CORRECT          = "correct"
    
    // Quiz Click
    static let SEGMENT_EVENT_QUIZ_CLICK        = "quiz_click"
    static let SEGMENT_QUIZ_LIVE               = "live"
    
    // Quiz Exit
    static let SEGMENT_EVENT_QUIZ_EXIT         = "quiz_exit"
    static let SEGMENT_QUIZ_EXIT_TIME          = "exit_time"
    static let SEGMENT_QUIZ_SESSION_DURATION   = "quiz_session_duration"
    static let SEGMENT_QUIZ_EXIT_PAGE          = "exit_page"
    
    // Leaderboard View
    static let SEGMENT_EVENT_LEADERBOARD_VIEW  = "leaderboard_view"
    static let SEGMENT_TAB_NAME                = "tab_name"
    static let SEGMENT_SCROLLS                 = "scrolls"
    
    // Leaderboard Click
    static let SEGMENT_EVENT_LEADERBOARD_CLICK = "leaderboard_click"
    
    // LifeLine Click
    static let SEGMENT_EVENT_LIFELINE_CLICK    = "lifeline_click"
    
    // Continue Button Click
    static let SEGMENT_EVENT_CONTINUE_BUTTON_CLICK  = "continue_playing_button_click"
    
    static let SEGMENT_PLATFORM_VALUE          = "ios"
    static let SEGMENT_CHANNEL_VALUE           = "ios"
}
