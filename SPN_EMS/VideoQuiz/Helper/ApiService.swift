//
//  BaseApiService.swift
//  LogixEngage
//
//  Created by Apple on 09/07/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire
import LogixEngageFramework

public class ApiService: NSObject {
    
    static var shared = ApiService()
    
    var baseURL             : String
    let decoder             : JSONDecoder
    let sessionManager      = SessionManager.default
    var authToken  : String? = nil
    
    var headers: HTTPHeaders  {
        if LGEngageUtility.shared.getAuthToken() != "" {
            return [
                "Authorization": LGEngageUtility.shared.getAuthToken(),
                "x-api-key"    : Urls.API_KEY!
            ]
        }
        return [
            "x-api-key" : Urls.API_KEY!
        ]
    }
    
    init(domain: String = "", decoder: JSONDecoder = JSONDecoder()) {
        self.baseURL = domain
        self.decoder = decoder
    }
}

typealias OtpHandler = (OTPResponse?, String?) -> (Void)

protocol OtpRequestProtocol {
    func otpRequest(with data: [String: Any], onResult: @escaping OtpHandler)
}

extension ApiService: OtpRequestProtocol {
    
    func otpRequest(with data: [String: Any], onResult: @escaping OtpHandler) {
        
        let url = Urls.BASE_URL + "user"
        sessionManager.request(url, method: .post, parameters: data, encoding: JSONEncoding.default, headers: self.headers).responseDecodableObject { (response:DataResponse<OTPResponse>) in
            
            if let error = response.result.error {
                onResult(nil, error.localizedDescription)
                return
            }
            if let status = response.result.value?.status {
                if status != 0 {
                    onResult(nil, response.result.value?.message)
                    return
                }
            }
            
            onResult(response.result.value, nil)
        }
    }
}

