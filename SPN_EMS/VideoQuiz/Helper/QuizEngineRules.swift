//
//  QuizEngineRules.swift
//  SPN_EMS
//
//  Created by Apple on 09/09/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import Foundation

public class QuizEngineRules: NSObject {
    
    private static let TAG              : String            = "QuizEngine";
    private let        MAX_HASH_SIZE    : Int               = 10;
    private var        totalQuestions   : Int               = -1;
    private var        quizMap          : [Int: QuizData]   = [:]
    
    static var shared = QuizEngineRules()
    
    public override init() {
        
    }
    
    public func setTotalQuestions(totalQuestions: Int) {
        self.totalQuestions = totalQuestions
    }
    
    public func getTotalQuestions() -> Int {
        return self.totalQuestions
    }
    
    public func canUseLifeLine(currentQuestion: Int) throws -> Bool {
        var canUseLifeLine: Bool = false
        if currentQuestion == 0 {
            throw QuizError.InvalidQuestionId
        }
        
        if currentQuestion == self.totalQuestions {
            canUseLifeLine = false
        } else if currentQuestion == 1 {
            canUseLifeLine = true
        } else {
            var inGame: Bool = false
            for i in 0 ..< currentQuestion - 1 {
                if (quizMap[i]?.lifelineApplied)! {
                    inGame = false
                    break
                } else if (quizMap[i]?.correctAnswer)! {
                    inGame = true
                }
                else {
                    inGame = false
                    break
                }
            }
            if inGame == true {
                canUseLifeLine = true
            }
        }
        return canUseLifeLine
    }

    public func canPlayForCashPrize(currentQuestion: Int) throws -> Bool {
        var canPlay: Bool = false
        if currentQuestion == 0 {
            throw QuizError.InvalidQuestionId
        }
        if currentQuestion == 1 {
            canPlay = true
            return canPlay
        }
        for i in 0 ... currentQuestion - 1 {
            if (quizMap[i]?.correctAnswer)! || (quizMap[i]?.lifelineApplied)! {
                canPlay = true
            } else {
                canPlay = false
                break
            }
        }
        return canPlay
    }
    
    public func isWinner(currentQuestion: Int) throws -> Bool {
        var isWinner: Bool = false
        if currentQuestion == 0 {
            throw QuizError.InvalidQuestionId
        }
        
        if currentQuestion == self.totalQuestions {
            for i in 0 ..< currentQuestion {
                if (quizMap[i]?.correctAnswer)! || (quizMap[i]?.lifelineApplied)! {
                    isWinner = true
                } else {
                    isWinner = false
                    break
                }
            }
        }
        return isWinner
    }
    
    public func setResult(currentQuestion: Int, is_correct: Bool) {
        if currentQuestion > 0 && currentQuestion <= self.totalQuestions {
            quizMap[currentQuestion - 1]?.correctAnswer = is_correct
        }
    }
    
    public func getResult(currentQuestion: Int) -> Bool {
        var result: Bool = false
        if currentQuestion > 0 && currentQuestion <= self.totalQuestions {
            result = (quizMap[currentQuestion - 1]?.correctAnswer)!
        }
        return result
    }
    
    public func setLifeLineUsed(currentQuestion: Int, lifeline: Bool) {
        if currentQuestion > 0 && currentQuestion <= self.totalQuestions {
            quizMap[currentQuestion - 1]?.lifelineApplied = lifeline
        }
    }
    
    public func getLifeLineUsed(currentQuestion: Int) -> Bool {
        var lifeline: Bool = false
        if currentQuestion > 0 && currentQuestion <= self.totalQuestions {
            lifeline = (quizMap[currentQuestion - 1]?.lifelineApplied)!
        }
        return lifeline
    }
    
    public func setIsAnswered(currentQuestion: Int, is_answered: Bool) {
        if currentQuestion > 0 && currentQuestion <= self.totalQuestions {
            quizMap[currentQuestion - 1]?.isAnswered = is_answered
        }
    }
    
    public func getIsAnswered(currentQuestion: Int) -> Bool {
        var result: Bool = false
        if currentQuestion > 0 && currentQuestion <= self.totalQuestions {
            result = (quizMap[currentQuestion - 1]?.isAnswered)!
        }
        return result
    }
    
    public func reset() {
        self.totalQuestions = -1;
        for i in 0 ..< self.MAX_HASH_SIZE {
            self.quizMap[i] = QuizData()
        }
    }
    
    //#MARK: Class QuizData
    private class QuizData {
        var correctAnswer   : Bool = false
        var lifelineApplied : Bool = false
        var isAnswered      : Bool = false
        
        init() {
            self.correctAnswer   = false
            self.lifelineApplied = false
            self.isAnswered      = false
        }
    }
    
    //#MARK: Enum QuizError
    public enum QuizError: Error {
        case InvalidQuestionId
    }
    
}
