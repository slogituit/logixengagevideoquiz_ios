//
//  QuizUtilities.swift
//  SPN_EMS
//
//  Created by Apple on 06/09/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import Foundation

class QuizUtilities: NSObject {
    
    static var shared = QuizUtilities()

    // resize image with given size
    public func imageWithSize( _ image: UIImage, newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    // Convert local time to UTC (or GMT)
    public func toGlobalTime(date: Date) -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: date))
        return Date(timeInterval: seconds, since: date)
    }
    
    // Convert UTC (or GMT) to local time
    public func toLocalTime(date: Date) -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: date))
        return Date(timeInterval: seconds, since: date)
    }

}
