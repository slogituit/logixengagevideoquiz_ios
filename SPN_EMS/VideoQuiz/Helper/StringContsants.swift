//
//  StringContsants.swift
//  LogixEngage
//
//  Created by Apple on 15/07/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import Foundation

class StringConstants: NSObject {
    //Stryboard
    let videoQuizStoryboard = "LogixEngage"
    // Answer view
    let correctAnswerTitle = "Great! You got it right!"
    let wrongAnswerTitle = "Oops! Incorrect Answer!"
    let knocked_out = "Knocked Out"
    //let you_are_late = "Sorry! You are late"
    let knocked_out_sorry = "Sorry! You are"
    let answer_submitted = "Answer Submitted"
    let time_up = "TIME UP"
    let knockedOut_toast = "Can not answer the question. You are knocked out..."
    let otp_error = "OTP Does Not Match"
    let life_used = "One Life Used"
    
    //Time up
    var timeUpTitle = "Opps!! Times up!"
    var timeUpDescr = "No money this time but you can still accumulate points and get goodies."
    var timeUpBtnTitle = "Continue Playing"
    
    //with lifeline
    var useLifelineTitle = "Ohh!! That was a wrong answer!"
    var useLifelineText = "You still have one chance! Use this lifeline and be in the race to win the grand prize!"
    var useLifeBtnTitle = "Use a life line"

    
    //without lifeline
    let wrongAnsTitle = "Ohh!! That was a wrong answer!"
    let wrongAnsDescription = "No money this time but you can still accumulate points and get goodies."
    var wrongAnsBtnTitle = "Continue Playing"
    
    var defaultProfilePicUrl = "https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_player-512.png"
    
    //for joining the quiz late
    var youAreLate = "Oh! You’re late. Quiz has already started."
    var youAreLateDescription = "No money this time but you can still accumulate points and get goodies.'"
    
    //Lifeline cont array
    var countArr = ["0": "zero", "1": "one", "2": "two", "3": "three", "4": "four", "5": "five",
                    "6": "six", "7": "seven", "8": "eight", "9": "nine", "10": "ten"]
    
    //internet
    var offlineMsg = "The Internet connection appears to be offline"

    //for sponsor
    let broughtToYou = "Brought to you by"
    let PoweredBy = "Powered by"
    
    //ad url string
    let defaultUrlForAd = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&" +
        "iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&" +
        "output=vast&unviewed_position_start=1&" + "cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator="
}
