//
//  Colors.swift
//  LogixEngage
//
//  Created by Apple on 14/07/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import AVFoundation
import UIKit

typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

// Custom color
extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        if (hex.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 255.0,
            green: CGFloat(g) / 255.0,
            blue: CGFloat(b) / 255.0, alpha: 1
        )
    }
}

enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint: CGPoint {
        return points.startPoint
    }
    
    var endPoint: CGPoint {
        return points.endPoint
    }
    
    var points: GradientPoints {
        get {
            switch(self) {
            case .topRightBottomLeft:
                return (CGPoint(x: 0.0, y: 1.0), CGPoint(x: 1.0, y: 0.0))
            case .topLeftBottomRight:
                return (CGPoint(x: 0.0, y: 0.0), CGPoint(x: 1, y: 1))
            case .horizontal:
                return (CGPoint(x: 0.0, y: 0.5), CGPoint(x: 1.0, y: 0.5))
            case .vertical:
                return (CGPoint(x: 0.0, y: 0.0), CGPoint(x: 0.0, y: 1.0))
            }
        }
    }
}

extension UIView {
    func applyGradient(colours: [UIColor], gradientOrientation orientation: GradientOrientation) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradient(colours: [UIColor], gradientOrientation orientation: GradientOrientation, gradientLayer: CAGradientLayer) {
        let gradient = gradientLayer
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func removeGradient(gradientLayer: CAGradientLayer) {
        gradientLayer.removeFromSuperlayer()
    }
}


class Colors: NSObject {
    
    
    //#MARK: Original color codes
    
    var colorPrimary = UIColor(hex: "#a36efd")
    var colorPrimaryDark = UIColor(hex: "#5027ac")
    var red = UIColor(hex: "#D81B60")
    var white = UIColor(hex: "#FFFFFF")
    var background_dark_blue = UIColor(hex: "#3f51b5")
    var hint_color = UIColor(hex: "#C992929C")
    var disable_color = UIColor(hex: "#C9535252")
    var hint_color_1 = UIColor(hex: "#C9D5D5D5")
    var green = UIColor(hex: "#6ac259")
    var black = UIColor(hex: "#000000")
    var gray = UIColor(hex: "#929090")
    var lightblue = UIColor(hex: "#3333B5E5")
    var lightblueforAnswer = UIColor(hex: "#00BCD4")
    var progressbarCountColor = UIColor(hex: "#333333")
    var progressbarColor = UIColor(hex: "#9f6dfc")
    var optionBackground = UIColor(hex: "#efefef")
    var yellow = UIColor(hex: "#FFD700")
    var yellow_1 = UIColor(hex: "#FFE662")
    var orange = UIColor(hex: "#FF8C00")
    
    var unselectedTabColor = UIColor(hex: "#969696")
    var staringTextColor = UIColor(hex: "#D9008D")
    var selectedTabColor = UIColor(hex: "#d9008d") //pink
    var false_question = UIColor(hex: "#f9c2c1")
    var true_question = UIColor(hex: "#c3e7bc")
    var oops = UIColor(hex: "#f16865")
    var textcolorafterselection = UIColor(hex: "#979797")
    
    //<!--theme backgroun-->
    var theme_up = UIColor(hex: "#a36efd")
    var theme_dowm = UIColor(hex: "#605fef")
    var card_background = UIColor(hex: "#5027ac")
    var dark_blue = UIColor(hex: "#3e1f84")
    var button_background = UIColor(hex: "#966bfa")
    
    //#MARK: color codes for Sony Quiz
    var warmGray = UIColor(hex: "#818181")
    var blackText = UIColor(hex: "#212121")
    var dividerBackgroundGrey = UIColor.init(hex: "#dedede")
    var shadowColor = UIColor.init(hex: "#3d000000")
    
    
    //Homescreen- how it works view
    var gradientGray1 = UIColor.init(hex: "#d8d8d8")
    var gradientGray2 = UIColor.init(hex: "#eeeeee")
    
    // navigation bar
    var gradientGray3 = UIColor.init(hex: "#f0eeec")
    
    var gradientOrange1 = UIColor.init(hex: "#f86f12")
    var gradientOrange2 = UIColor.init(hex: "#f04920")
    
    //Leaderboard nav and buttons gradient
    var gradientBlack1 = UIColor.init(hex: "#000000")
    var gradientBlack2 = UIColor.init(hex: "#00c8c8c8")
    
    //WinnerCongratsView
    var orangeText = UIColor.init(hex: "#f86d13")
    var gradientOrange3 = UIColor.init(hex: "#ee4023")
    var gradientOrange4 = UIColor.init(hex: "#f97211")
    
    //Leaderboard view
    var backgroundYellow = UIColor.init(hex: "#ffc900")
    var backgroundGreen = UIColor.init(hex: "#45ed7f")
    var backgroundOrange = UIColor.init(hex: "#f76a14")
    var backgroundGrey = UIColor.init(hex: "#141414")

    //QuestionsViewController
    //var btnBackgroundGrey = UIColor.lightGray //UIColor.init(hex: "#80ffffff")
    var btnBackgroundGrey = UIColor.init(hex: "#ffffff").withAlphaComponent(0.5)

    //correct option
    var trueOptionGradient1 = UIColor.init(hex: "#c7f04a")
    var trueOptionGradient2 = UIColor.init(hex: "#95dd24")
    //Wrong option
    var wrongOptionGradient1 = UIColor.init(hex: "#ff0000")
    var wrongOptionGradient2 = UIColor.init(hex: "#c50b0b")
    //coinsViewBackground
    var coinsViewBackground = UIColor.init(hex: "#9e9e9e")
    
    //Gradient Color for event image
    var eventImgGradient1 = UIColor.init(hex: "#000000").withAlphaComponent(0.0)
    var eventImgGradient2 = UIColor.init(hex: "#000000")
    
    //Gradient Color for title view on share controller
    
    var shareTitleGradient1 = UIColor.init(hex: "#000000")
    var shareTitleGradient2 = UIColor.init(hex: "#000000").withAlphaComponent(0.0)
    
    //background image on video quiz
    var imageBackgroundGradient1 = UIColor.init(hex: "#ffffff").withAlphaComponent(0.5)
    //80ffffff
    var imageBackgroundGradient2 = UIColor.init(hex: "#000000")
}

class RoundShadowView: UIView {
    
    private var shadowLayer: CAShapeLayer!
    private var cornerRadius: CGFloat = 2
    private var fillColor: UIColor = .white // the color applied to the shadowLayer, rather than the view's backgroundColor
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = fillColor.cgColor
            
            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            shadowLayer.shadowOpacity = 0.2
            shadowLayer.shadowRadius = 3
            
            shadowLayer.shouldRasterize = true
            shadowLayer.rasterizationScale = UIScreen.main.scale
            
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
}

class RoundShadowImageView: UIImageView {
    
    private var shadowLayer: CAShapeLayer!
    private var cornerRadius: CGFloat = 2
    private var fillColor: UIColor = .white // the color applied to the shadowLayer, rather than the view's backgroundColor
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = fillColor.cgColor
            
            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            shadowLayer.shadowOpacity = 0.2
            shadowLayer.shadowRadius = 3
            
            shadowLayer.shouldRasterize = false
//            shadowLayer.rasterizationScale = UIScreen.main.scale
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
}
