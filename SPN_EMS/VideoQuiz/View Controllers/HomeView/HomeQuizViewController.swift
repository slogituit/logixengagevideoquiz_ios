//
//  ViewController.swift
//  LogixEngage
//
//  Created by Apple on 21/08/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit
import os.log
import NVActivityIndicatorView
import LogixEngageFramework
//import SonyAR

class HomeQuizViewController: UIViewController {
    
    @IBOutlet weak var btnHowItWork   : UIButton!
    
    @IBOutlet weak var navigationView           : UIView!
    @IBOutlet weak var navigationContentsView   : UIView!
    @IBOutlet weak var howItWorksView           : UIView!
    @IBOutlet weak var cardView                 : UIView!
    @IBOutlet weak var bgView                   : UIView!
    
    @IBOutlet weak var myProfilePic             : UIImageView!
    @IBOutlet weak var myTotalCoins             : UILabel!
    @IBOutlet weak var myRank                   : UILabel!
    @IBOutlet weak var tableView                : UITableView!
    @IBOutlet weak var upcomingLabel            : UILabel!
    
    //Top bar buttons
    @IBOutlet weak var quizLeaderboardView      : UIView!
    @IBOutlet weak var redeemMoneyView          : UIView!
    @IBOutlet weak var shareView                : UIView!
    @IBOutlet weak var dividerView1             : UIView!
    @IBOutlet weak var dividerView2             : UIView!
    
    //Ongoing views
    @IBOutlet weak var ongoingQuizPrize         : UILabel!
    @IBOutlet weak var ongoingQuizView          : CardView!
    @IBOutlet weak var ongoingViewHeight        : NSLayoutConstraint!
    @IBOutlet weak var ongoingViewBackground    : UIImageView!
    @IBOutlet weak var quizLabel                : UILabel!

    let myActivityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
    var engageManager           : LGEngageManager!  = LGEngageManagerImp.shared
    var eventList               : [Events]          = []
    var pastEventList           : [Events]          = []
    var pastRewardList          : [Rewards]         = []
    open var eventStrtedMessage : EventStartedMessage!
    var state                   : LGEngageMessageUtils.EVENT_STATE = LGEngageMessageUtils.EVENT_STATE.EVENT_STATE_NOT_SET
    let userDefaults                                = UserDefaults.standard
    var isOngoingBtnSelected                        = true
    var isPastEventBtnSelected                      = false
    var vc                                          = HowItWorksVC()
    var liveEventId             : String!
    var selfLiveEvent           : Events!
    var selfLiveReward          : [Rewards]         = []
    var pageId                  : String            = ""
    var propertyId              : String            = ""
    var entryPoint              : String            = ""
    var isTestEvent             : Bool              = false
    var timer                   : Timer!
    var aliveMsgSent            : Bool              = false
    var regionNo                : Int               = 0
    var reachability            : Reachability!
    var gradient                : CAGradientLayer!
    var gradient2               : CAGradientLayer!
    var eventResponse           : GetEventsResponse!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let userId       = UserDefaults.getVideoQuizDetails()?.id
        let cpCustomerId = UserDefaults.getVideoQuizDetails()?.cpCustomerID
        let appVersion   = UserDefaults.getVideoQuizDetails()?.appVersion
        let advId        = UserDefaults.getVideoQuizDetails()?.advId
        let lat          = UserDefaults.getVideoQuizDetails()?.latitude
        let long         = UserDefaults.getVideoQuizDetails()?.longitude
        
        self.liveEventId = UserDefaults.standard.string(forKey: "liveEventId")
        self.pageId      = UserDefaults.standard.string(forKey: "pageID") ?? ""
        self.propertyId  = UserDefaults.standard.string(forKey: "propertyID") ?? "AllIndia"
        self.isTestEvent = UserDefaults.standard.bool(forKey: "isTestEvent")
        self.regionNo = UserDefaults.standard.integer(forKey: "regionNumber")

        //5d64f16e6b3381863a00c32f
        if self.pageId == "0"{
            entryPoint = "Listing"
        }
        else if self.pageId == "1" {
            entryPoint = "Quiz"
        }
        else if self.pageId == "2" {
            entryPoint = "Leaderboard"
        }
        
        GoogleAnalyticsHelper.shared.quizEntry(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: "", eventName: propertyId, entryPoint: entryPoint)
        SegmentAnalytics.shared.quizEntry(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: "", eventName: propertyId, entryPoint: entryPoint, latitude: lat ?? "", longitude: long ?? "")
        
        tableView.rowHeight         = 110
        tableView.separatorStyle    = .none
        tableView.bounces           = false
        engageManager = LGEngageManagerImp.shared
        
        if liveEventId != nil && self.pageId == "1" {
            self.tableView.isHidden      = true
            self.navigationView.isHidden = true
            self.howItWorksView.isHidden = true
            self.cardView.isHidden       = true
        } else if self.pageId == "2" {
            //navigate to leaderboard
            let leaderboard = storyboard?.instantiateViewController(withIdentifier: "LeaderboardViewController") as! LeaderboardViewController
            self.navigationController?.pushViewController(leaderboard, animated: true)
        }
        
        self.gradient = CAGradientLayer()
        self.btnHowItWork?.applyGradient(colours: [Colors().gradientGray2, Colors().gradientGray1], gradientOrientation: .vertical, gradientLayer: self.gradient)
        self.gradient2 = CAGradientLayer()
        self.navigationContentsView.applyGradient(colours: [Colors().gradientOrange1, Colors().gradientOrange2], gradientOrientation: .vertical)
        
        self.startCountDownTimer()
        self.setupNetworkHandling()  //for network handling
        
        let buttonHeight = navigationView.frame.height
        let buttonWidth = navigationView.frame.width
        
        let shadowSize: CGFloat = 5
        let contactRect = CGRect(x: -shadowSize, y: buttonHeight - (shadowSize * 0.2), width: buttonWidth + shadowSize * 2, height: shadowSize)
        navigationView.layer.shadowPath = UIBezierPath(ovalIn: contactRect).cgPath
        navigationView.layer.shadowOpacity = 0.3
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.willEnterForeground()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.gradient.frame = self.btnHowItWork.frame
        self.gradient2.frame = self.navigationContentsView.frame
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func willEnterForeground() {
        
        self.dividerView1.backgroundColor = Colors().dividerBackgroundGrey
        self.dividerView2.backgroundColor = Colors().dividerBackgroundGrey
        
        //self.quizLabel.font = UIFont.textStyle4
        myTotalCoins.text = "0"
        self.navigationContentsView.asRect()
        self .navigationContentsView.clipsToBounds = true
        
        self.quizLeaderboardView.isUserInteractionEnabled = true
        let touchGesture1 = UITapGestureRecognizer(target: self, action:  #selector (self.viewLeaderboard (_:)))
        self.quizLeaderboardView.addGestureRecognizer(touchGesture1)
        
        self.redeemMoneyView.isUserInteractionEnabled = true
        let touchGesture2 = UITapGestureRecognizer(target: self, action:  #selector (self.redeemMoneyClicked (_:)))
        self.redeemMoneyView.addGestureRecognizer(touchGesture2)
        
        self.shareView.isUserInteractionEnabled = true
        let touchGesture3 = UITapGestureRecognizer(target: self, action:  #selector (self.shareButtonClick (_:)))
        self.shareView.addGestureRecognizer(touchGesture3)
        
        self.ongoingQuizView.isUserInteractionEnabled = true
        let touchGesture4 = UITapGestureRecognizer(target: self, action:  #selector (self.ongoingQuizViewClick (_:)))
        self.ongoingQuizView.addGestureRecognizer(touchGesture4)
        
        myActivityIndicator.center = view.center
        myActivityIndicator.color = Colors().orangeText
        myActivityIndicator.startAnimating()
        self.view.addSubview(myActivityIndicator)
        tableView.isHidden = true
        upcomingLabel.isHidden = true
        ongoingQuizView.isHidden = true
        ongoingViewHeight.constant = 0
        self.pastEventList.removeAll()
        self.pastRewardList.removeAll()
        self.pastEventList = []
        self.pastRewardList = []
        
        //update events table after event is started
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.updateEvents),
            name: NSNotification.Name(rawValue: "updateEvents"),
            object: nil)
        
        EngageEventHandler.shared.delegate = self
        if (self.engageManager != nil) {
            // get events api call
            self.engageManager.getEvents(limit: 15, delegate: self, region: self.regionNo, testEvents: self.isTestEvent)
        }
        
        //call Lives and Coins api
        if UserDefaults.getVideoQuizDetails()?.id != nil && UserDefaults.getVideoQuizDetails()?.id != "" {
            if let userId = UserDefaults.getVideoQuizDetails()?.id {
                LGEngageManagerImp.shared.getLives(user_id: userId, delegate: self)
                LGEngageManagerImp.shared.getCoins(user_id: userId, delegate: self)
            }
        }
        
        let profilePic = UserDefaults.getVideoQuizDetails()?.profileImageUrl ?? StringConstants().defaultProfilePicUrl
        
        if profilePic != "" {
            let url = URL(string: (profilePic))
            self.myProfilePic.sd_setImage(with: url)
        }
        self.myProfilePic.asCircle()
        self.startCountDownTimer()
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(resetTimer),
        name: UIApplication.didEnterBackgroundNotification, object: nil)

        NotificationCenter.default.addObserver(self,
        selector: #selector(willEnterForeground),
        name: UIApplication.didBecomeActiveNotification, object: nil)
        
        NotificationCenter.default.addObserver( self, selector: #selector(self.startCountDownTimer),
        name:  NSNotification.Name(rawValue: "appActiveStateNotification"), object:nil )
        
    }
    
    @objc func resetTimer() {
        if self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
        }
    }
    
    @objc func startCountDownTimer() {
        if timer == nil {
            self.timer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
        }
    }
    
    @objc func fireTimer() {
        if self.aliveMsgSent {
            self.aliveMsgSent = false
            return
        } else {
            LGEngageManagerImp.shared.sendMessage(message: "KeepAlive")
            print("KeepAlive msg sent")
            self.aliveMsgSent = true
            return
        }
    }
    
    @IBAction func howItWorksBtnClicked(sender : UIButton){
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "HowItWorksVC") as? HowItWorksVC {
            self.vc = vc
            if let eventResponse = self.eventResponse {
                if let region = eventResponse.region {
                    if let terms = region.terms {
                        self.vc.infoArray = terms
                    }
                }
            }
            self.bgView.isHidden = false
            self.view.bringSubviewToFront(bgView)
            self.addChild(vc)
            self.vc.view.animShow() //for showing view from bottom to top
            self.view.addSubview(vc.view)
            self.view.bringSubviewToFront(vc.view)
            vc.view.anchor(top: self.quizLeaderboardView.bottomAnchor, leading: self.view.leadingAnchor, bottom: self.view.bottomAnchor, trailing: self.view.trailingAnchor,padding: .init(top: 5, left: 0, bottom: 0, right: 0))
            vc.didMove(toParent: self)
            vc.cancelBtn.addTarget(self, action: #selector(self.dismissHowItWorksVC), for: .touchUpInside)
        }
    }
    
    //for removing howItWorksVC
    @objc func dismissHowItWorksVC(){
        self.bgView.isHidden = true
        self.view.sendSubviewToBack(bgView)
        self.vc.willMove(toParent: nil)
        self.vc.view.removeFromSuperview()
        self.vc.removeFromParent()
    }
    
    //update table after event is started
    @objc func updateEvents(){
        EngageEventHandler.shared.delegate = self
        if (self.engageManager != nil) {
            // get events api call
            tableView.isHidden = true
            upcomingLabel.isHidden = true
            ongoingQuizView.isHidden = true
            ongoingViewHeight.constant = 0
            pastEventList.removeAll()
            pastRewardList.removeAll()
            self.pastEventList = []
            self.pastRewardList = []
            upcomingLabel.isHidden = true
            self.ongoingViewBackground.image = nil
            myActivityIndicator.startAnimating()
            self.engageManager.getEvents(limit: 15, delegate: self, region: self.regionNo, testEvents: self.isTestEvent)
        }
    }
    
    // Redeem money clicked Button Click
    @objc func redeemMoneyClicked(_ recognizer: UITapGestureRecognizer) {
        //Do something when notifyMeButtonClick
        let vc = storyboard?.instantiateViewController(withIdentifier: "WinnerCongratsViewController") as! WinnerCongratsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Share button click
    @objc func shareButtonClick(_ recognizer: UITapGestureRecognizer) {
        
    }
    
    //ongoing quiz view click
    @objc func ongoingQuizViewClick(_ recognizer: UITapGestureRecognizer) {
        self.joinLiveEvent(event: selfLiveEvent)
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        //TBD
        let userId          = UserDefaults.getVideoQuizDetails()?.id
        let cpCustomerId    = UserDefaults.getVideoQuizDetails()?.cpCustomerID
        let appVersion      = UserDefaults.getVideoQuizDetails()?.appVersion
        let advId           = UserDefaults.getVideoQuizDetails()?.advId
        let type            = UserDefaults.standard.string(forKey: "QuizType")
        let lat             = UserDefaults.getVideoQuizDetails()?.latitude
        let long            = UserDefaults.getVideoQuizDetails()?.longitude
        
        if let userId = userId {
            let quizExitTime = Date().timeIntervalSince1970
            let quizEntryTimeInterval = UserDefaults.standard.double(forKey: "quizEntryTimeStamp")
            let quizSessionDuration = quizExitTime - quizEntryTimeInterval
            
            
            let quizSessionDurationFormatted  = String(format: "%.2f", quizSessionDuration)
            GoogleAnalyticsHelper.shared.quizExit(userId: userId, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: "", eventName: propertyId, quizExitPage: propertyId, quizExitTime: Date().description, quizSessionDuration: quizSessionDurationFormatted, quizType: type ?? "")
            
            SegmentAnalytics.shared.quizExit(userId: userId, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: "", eventName: propertyId, quizExitPage: propertyId, quizExitTime: Date().description, quizSessionDuration: quizSessionDurationFormatted, latitude: lat ?? "", longitude: long ?? "")
            
            UserDefaults.standard.set("", forKey: "quizEntryTimeStamp")
        }
        NotificationCenter.default.removeObserver(self)
        self.resetTimer()
        LGEngageManagerImp.shared.stopSession()
        UserDefaults.standard.set(nil, forKey: "eventId")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: {
            UIApplication.shared.isIdleTimerDisabled = false
        })
    }
    
    // view leaderboard btn clicked
    @objc func viewLeaderboard(_ recognizer: UITapGestureRecognizer) {
        let leaderboard = storyboard?.instantiateViewController(withIdentifier: "LeaderboardViewController") as! LeaderboardViewController
        self.navigationController?.pushViewController(leaderboard, animated: true)
        let userId = UserDefaults.getVideoQuizDetails()?.id
        let cpCustomerId =  UserDefaults.getVideoQuizDetails()?.cpCustomerID
        let appVersion =  UserDefaults.getVideoQuizDetails()?.appVersion
        let advId =  UserDefaults.getVideoQuizDetails()?.advId
        let lat = UserDefaults.getVideoQuizDetails()?.latitude
        let long = UserDefaults.getVideoQuizDetails()?.longitude
        GoogleAnalyticsHelper.shared.leaderboardClick(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!)
        
        SegmentAnalytics.shared.leaderboardClick(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, latitude: lat ?? "", longitude: long ?? "")
        
        GoogleAnalyticsHelper.shared.leaderboardView(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, tabName: "This Week", scrolls: "0")
        SegmentAnalytics.shared.leaderboardView(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, tabName: "This Week", scrolls: "0", latitude: lat ?? "", longitude: long ?? "")
    }
    
    func setUIForOngoingQuiz() {
        self.ongoingQuizView.isHidden = false
        self.ongoingViewHeight.constant = 200
        self.ongoingQuizPrize.text = selfLiveReward[0].priceAvailable.withCommas()
        
        if selfLiveEvent.bannerImage != nil {
            let url = URL(string: (selfLiveEvent.bannerImage)!)
            if let url = url {
                self.ongoingViewBackground.sd_setImage(with: url)
            }
        }
    }
    
    func joinLiveEvent(event: Events) {
        
        if(!event.isLive){
            self.joinNonLiveEvent(quizPrize: (event.rewards.first?.priceAvailable.withCommas())!, quizName: event.eventName, quizStartTime: event.time.getNewTimeFormat(), event: event)
            return
        }
        UserDefaults.standard.set(event._id, forKey: "LiveEventId")
        UserDefaults.standard.set(event.eventName, forKey: "LiveEventName")
        UserDefaults.standard.set(Date().timeIntervalSince1970, forKey: "quizEntryTimeStamp")
        
        let eventId = event._id
        
        if event._id != nil {
            LGEngageManagerImp.shared
                .connectToEvent(user_id: UserDefaults.getVideoQuizDetails()?.id ?? "", event_id: eventId!)
            
            // Store Event Id...
            UserDefaults.standard.set(eventId, forKey: "eventId")
            let eventMsg = EventStartedMessage(eventId: event._id, liveTime: event.liveTime, videoUrl: event.videoUrl, eventName: event.eventName)
            let currentTime = Date()
            let diffTime = LGEngageMessageUtils.isWithInEventStartTimeThreshold(q_time: event.liveTime.toDate()!, c_time: QuizUtilities.shared.toLocalTime(date: currentTime))
            UserDefaults.standard.set(eventMsg.eventName, forKey: "currentEventName")
            UserDefaults.standard.set(event.eventImage, forKey: "currentEventImage")

            // Check against the Start Timer...
            if (diffTime != -1) {
                let vc = UIStoryboard(name: StringConstants().videoQuizStoryboard, bundle:SPN_EMS_Bundle).instantiateViewController(withIdentifier: "ShareController") as! ShareController
                //vc.price = self.ongoingQuizPrize.text
                vc.price = (event.rewards.first?.priceAvailable.withCommas())!
                vc.timeToStart = event.liveTime
                vc.quizTitleStr = event.eventName
                vc.eventStrtedMessage = eventMsg
                vc.eventImgUrl = event.eventImage
                //set url of Ad
                ShareController.kTestAppAdTagUrl = ""
                if let reward = event.rewards.first {
                    vc.rewardDescription = reward.descriptionString
                }
                self.pushController(vc: vc)
                
            } else {
                let vc = UIStoryboard(name: StringConstants().videoQuizStoryboard, bundle:SPN_EMS_Bundle).instantiateViewController(withIdentifier: "VideoViewController") as! VideoViewController
                vc.eventStrtedMessage = eventMsg
                self.pushController(vc: vc)
            }
        }
    }
    
    func setupNetworkHandling(){
        reachability = Reachability()!
        // Register an observer for the network status
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        
        do {
            // Start the network status notifier
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        
        let userId          = UserDefaults.getVideoQuizDetails()?.id
        if(reachability.connection == .none){
            UIApplication.getTopViewController()?.view.makeToast(StringConstants().offlineMsg, duration: 3.0, position: .bottom)
        }
        else {
            if self.selfLiveEvent != nil {
                if let userId = userId {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                        
                        if let eventID = UserDefaults.standard.value(forKey: "eventId") {
                            LGEngageManagerImp.shared.disconnectFromEvent(user_id: userId, event_id: eventID as! String)
                            UserDefaults.standard.set(true, forKey: "DisconnectedDueToNetwork")
                            EngageEventHandler.isBackBtnPressed = true
                            LGEngageManagerImp.shared.connectToEvent(user_id: userId, event_id: eventID as! String)
                        }
                        
                    })
                }
            }
        }
        if self.selfLiveEvent != nil {
            //UIApplication.shared.windows.first?.visibleViewController
            if let topVC = UIApplication.getTopViewController() {
                if topVC.isKind(of: VideoViewController.self) || topVC.isKind(of: QuestionViewController.self){
                    NotificationCenter.default.post(name: .rechabilityChangedNotification, object: self.reachability)
                }
                if topVC.isKind(of: WinnerCongratsViewController.self) || topVC.isKind(of: NonWinnersViewController.self) ||
                    topVC.isKind(of: LeaderboardViewController.self) {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
}


extension HomeQuizViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pastEventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = String(describing: indexPath.row)
        let cellIdentifier = "QuizCell"+index
        
        var cell:QuizListCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? QuizListCell
        
        if (cell == nil) {
            let nib: NSArray = SPN_EMS_Bundle.loadNibNamed("QuizListCellNib", owner: self, options: nil)! as NSArray
            
            cell = nib[0] as? QuizListCell
        }
        if(pastEventList.count != 0) {
            let newEvent = pastEventList[indexPath.row]
            let newReward = pastRewardList[indexPath.row]
            cell!.setUiForNonLivEvent(newEvent: newEvent, newReward: newReward)
            cell!.event = newEvent
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //let cell : QuizListCell = tableView.cellForRow(at: indexPath) as! QuizListCell
        if(pastEventList.count != 0){
            let event = pastEventList[indexPath.row]
            if event.isLive {
                return 210
            }
            if event.eventSponsor != nil && event.eventSponsor.count != 0{
                return 140
            }
            return 110
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell : QuizListCell = tableView.cellForRow(at: indexPath) as! QuizListCell
        let event = pastEventList[indexPath.row]
       
        self.joinNonLiveEvent(quizPrize: cell.quizPrize.text!, quizName: cell.quizName.text!, quizStartTime: cell.quizStartTime.text!, event: cell.event!)
    }
    
    func joinNonLiveEvent(quizPrize : String, quizName : String,quizStartTime : String, event : Events){
        let userId = UserDefaults.getVideoQuizDetails()?.id
        let cpCustomerId =  UserDefaults.getVideoQuizDetails()?.cpCustomerID
        let appVersion =  UserDefaults.getVideoQuizDetails()?.appVersion
        let advId =  UserDefaults.getVideoQuizDetails()?.advId
        let lat = UserDefaults.getVideoQuizDetails()?.latitude
        let long = UserDefaults.getVideoQuizDetails()?.longitude
        GoogleAnalyticsHelper.shared.quizClick(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: event._id, eventName: event.eventName)
        SegmentAnalytics.shared.quizClick(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: event._id, eventName: event.eventName, isLive: event.isLive.description, latitude: lat ?? "", longitude: long ?? "")
        
        let vc = UIStoryboard(name: StringConstants().videoQuizStoryboard, bundle:SPN_EMS_Bundle).instantiateViewController(withIdentifier: "ShareController") as! ShareController
        vc.price = quizPrize
        vc.quizTitleStr = quizName
        if !self.canShow30MinsTimer(on: vc,forEvent : event){
            vc.timeToStart = quizStartTime
        }
        vc.eventImgUrl = event.eventImage
        //set url of Ad
        if let addUrl = event.addUrls.first {
            ShareController.kTestAppAdTagUrl = addUrl
        }
        else{
            ShareController.kTestAppAdTagUrl = ""
        }
        if let reward = event.rewards.first {
            vc.rewardDescription = reward.descriptionString
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func canShow30MinsTimer(on vc: ShareController,forEvent : Events) -> Bool {
        
        let timeDifference = forEvent.time.toDate()!.timeIntervalSince1970 - QuizUtilities.shared.toLocalTime(date: Date()).timeIntervalSince1970
        if(timeDifference <= 1800 && timeDifference >= 0){
            vc.ANIMATION_DURATION_FOR_30MIN = Int(timeDifference)
            let eventMsg = EventStartedMessage(eventId: forEvent._id, liveTime: forEvent.liveTime, videoUrl: forEvent.videoUrl, eventName: forEvent.eventName)
            vc.eventStrtedMessage = eventMsg
            vc.checkForLiveEvent = true
            self.selfLiveEvent = forEvent
            return true
        }
        return false
    }
}

extension HomeQuizViewController: QuizHomeTableViewCellProtocol {
    func pushController(vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

//#MARK: OnGetEventsResponseDelegate implementation
extension HomeQuizViewController : OnGetEventsResponseDelegate {
    
    func onResponse(response: GetEventsResponse) {
        tableView.isHidden = true
        upcomingLabel.isHidden = true
        ongoingQuizView.isHidden = true
        ongoingViewHeight.constant = 0
        self.pastEventList.removeAll()
        self.pastRewardList.removeAll()
        self.pastEventList = []
        self.pastRewardList = []
        self.selfLiveEvent = nil
        self.eventList = response.events
        self.eventResponse = response
        var cnt = 0
        for event in eventList {
            if cnt == 0 {
                if liveEventId != nil && self.pageId == "1" {
                    if liveEventId == event._id {
                        joinLiveEvent(event: event)
                        liveEventId = nil
                        break
                    } else {
                        if event.isLive {
                            selfLiveEvent = event
                            for reward in event.rewards {
                                selfLiveReward.append(reward)
                            }
                        }
                        else {
                            selfLiveEvent = nil
                        }
                    }
                } else {
                    if event.isLive {
                        selfLiveEvent = event
                        for reward in event.rewards {
                            selfLiveReward.append(reward)
                        }
                    }
                    else {
                        selfLiveEvent = nil
                    }
                }
            }
            
            if !event.isLive {
                if liveEventId != nil && self.pageId == "1" {
                    if liveEventId == event._id {
                        joinLiveEvent(event: event)
                        liveEventId = nil
                        break
                    }
                }
                pastEventList.append(event)
                for reward in event.rewards {
                    pastRewardList.append(reward)
                }
            }
            cnt = cnt + 1
        }
        //tableView.dataSource = self
        tableView.reloadData()
        self.tableView.isHidden      = false
        self.navigationView.isHidden = false
        self.howItWorksView.isHidden = false
        self.cardView.isHidden       = false
        self.upcomingLabel.isHidden  = false
        
        if self.selfLiveEvent != nil {
            self.setUIForOngoingQuiz()
        }
        myActivityIndicator.stopAnimating()
    }
    
    func onFailure(error: String) {
        //self.view.makeToast(error, duration: 3.0, position: .bottom)
        myActivityIndicator.stopAnimating()
    }
}

extension HomeQuizViewController: EngageEventHandlerProtocol {
    
    func pushVewController(vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushVewControllerModally(vc: UIViewController, animated: Bool) {
        vc.view.backgroundColor = UIColor.clear
        vc.view.isOpaque = false
        //vc.view.alpha = 0.7
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //self.navigationController?.pushViewController(vc, animated: animated)
        self.navigationController?.present(vc, animated: false, completion: nil)
    }
        
    func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func popToRootViewController() {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension HomeQuizViewController : OnGetLivesResponseDelegate {
    
    func onLivesResponse(response: UserLivesData) {
        
        UserDefaults.standard.set(response.lifeline, forKey: "life")
        UserDefaults.standard.synchronize()
        //self.livesLabel.text = response.lifeline.description
    }
    
    func onLivesFailure(error: String) {
        //  self.view.makeToast(error, duration: 3.0, position: .bottom)
        UserDefaults.standard.set(0, forKey: "life")
    }
}

extension HomeQuizViewController : OnGetCoinsResponseDelegate {
    
    func onCoinsResponse(response: UserCoinsData) {
        if let coins = Int(response.livCoins.description) {
            let coins = Int(response.livCoins.description) ?? 0
            let formattedCoinsValue = coins.getFormmatedCoins()
            self.myTotalCoins.text = formattedCoinsValue
            self.myRank.text = response.rank?.description ?? "-"
            UserDefaults.standard.set(response.livCoins, forKey: "coins")
        }
        UserDefaults.standard.set(response.rank, forKey: "myRank")
        UserDefaults.standard.synchronize()
    }
    
    func onCoinsFailure(error: String) {
        //self.view.makeToast(error, duration: 3.0, position: .bottom)
    }
}
