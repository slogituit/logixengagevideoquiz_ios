//
//  QuizListCell.swift
//  LogixEngage
//
//  Created by Apple on 22/08/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit
import LogixEngageFramework
import SDWebImage

protocol QuizHomeTableViewCellProtocol {
    func pushController(vc: UIViewController)
}

class QuizListCell: UITableViewCell {
    
    @IBOutlet weak var leaderboardView      : UIView!
    @IBOutlet weak var quizName             : UILabel!
    @IBOutlet weak var quizPrize            : UILabel!
    @IBOutlet weak var quizStartTime        : UILabel!
    @IBOutlet weak var anchorImageView      : UIImageView!
    @IBOutlet weak var sponsorImageView     : UIImageView!
    @IBOutlet weak var sponsorDescr         : UILabel!
    @IBOutlet weak var sponsorView          : UIView!
    var urlString : String!
    open var event: Events!
    var delegate: QuizHomeTableViewCellProtocol!
    open var eventStrtedMessage: EventStartedMessage!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.quizName.textColor         = Colors().blackText
        self.quizPrize.textColor        = Colors().blackText
        self.quizStartTime.textColor    = Colors().warmGray
        sponsorView.isUserInteractionEnabled = true
        sponsorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openUrl)))
    }
    
    //Empty the views before reusing by the cell again
    override func prepareForReuse() {
        self.sponsorImageView.image = nil
        self.sponsorDescr.text = nil
        self.anchorImageView.image = nil
    }
    
    func setUiForNonLivEvent(newEvent: Events, newReward: Rewards) {
        
        self.leaderboardView.isHidden = false
        self.quizPrize.text = newReward.priceAvailable.withCommas()
        self.quizName.text = newEvent.eventName
        
        if newEvent.time != nil {
            let time = newEvent.time.getNewTimeFormat()
        
            self.quizStartTime.text = time
        } else {
            self.quizStartTime.text = "00:00:00"
        }
        
        if newEvent.anchorImage != nil {
            let url = URL(string: (newEvent.anchorImage)!)
            self.anchorImageView.sd_setImage(with: url)
        }
        if newEvent.bannerImage != nil {
            let url = URL(string: (newEvent.bannerImage)!)
//            DispatchQueue.main.async {
//                if let data = try? Data(contentsOf: url!) {
//                   // self.sponsorImageView.image = UIImage(data: data)
//                }
//            }
        }
        
        if let newEventSponsor = newEvent.eventSponsor {
            
            if let eventSponsor = newEventSponsor.first {
                if eventSponsor.descriptionKey != nil {
                    let attributedString = NSMutableAttributedString(string: eventSponsor.descriptionKey, attributes: [
                        .font: UIFont.systemFont(ofSize: 11.0, weight: .regular),
                        .foregroundColor:UIColor(white: 129.0 / 255.0, alpha: 1.0),
                        .kern: 0.0
                        ])
                    self.sponsorDescr.textAlignment = .right
                    //self.sponsorDescr.numberOfLines = 0
                    self.sponsorDescr.attributedText = attributedString
                }
                if eventSponsor.logoSmall != nil {
                    if let url = URL(string: eventSponsor.logoSmall){
                        self.sponsorImageView.sd_setImage(with: url)
                        self.sponsorImageView.isHidden = false
                    }
                }
                if let urlString = eventSponsor.url {
                        self.urlString = urlString
                    }
                }
            }
        }
    
    
    @objc func openUrl(){
        if self.urlString != nil {
            self.urlString.openUrl()
        }
    }
}

public extension String {
    //#MARK: utility methods
    func getNewTimeFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let convertedDate = dateFormatter.date(from: self)
        
        dateFormatter.dateFormat = "d MMM h:mma"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let timeStamp = dateFormatter.string(from: convertedDate!)
        return timeStamp
    }
}
