//
//  HowItWorksVC.swift
//  LogixEngage
//
//  Created by Abbas on 03/09/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit

class HowItWorksVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var cancelBtn : UIButton!
    
    var infoArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HowItWorkscell
        cell.sequenceLabel.text = (indexPath.row + 1).description
        cell.infoLabel.numberOfLines = 0
        cell.infoLabel.text = infoArray[indexPath.row]
        return cell
    }
}
