//
//  HowItWorkscell.swift
//  LogixEngage
//
//  Created by Abbas on 03/09/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit

class HowItWorkscell: UITableViewCell {

    @IBOutlet weak var sequenceLabel : UILabel!
    @IBOutlet weak var infoLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
