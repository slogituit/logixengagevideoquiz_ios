//
//  LifelineWithoutSponsorView.swift
//  SPN_EMS
//
//  Created by Abbas on 08/11/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import UIKit

class LifelineWithoutSponsorView: UIView {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var youHaveLifelineText: UILabel!
    
    static var indentifier = "LifelineWithoutSponsorView"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: 25)
        self.outerView.asCircle()
    }

}
