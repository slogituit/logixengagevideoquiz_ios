//
//  QuestionViewController.swift
//  LogixEngage
//
//  Created by Abbas's Mac Mini on 09/05/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import LogixEngageFramework
import Toast_Swift

class QuestionViewController: UIViewController {
    
    @IBOutlet weak var questionTimer        : SRCountdownTimer!
    @IBOutlet weak var buttonOptionOne      : UIButton!
    @IBOutlet weak var buttonOptionTwo      : UIButton!
    @IBOutlet weak var buttonOptionThree    : UIButton!
    @IBOutlet weak var buttonOptionFour     : UIButton!
    @IBOutlet weak var questionNumberLabel  : UILabel!
    @IBOutlet weak var numberOfPlayersLabel : UILabel!
    @IBOutlet weak var timerLabel           : UILabel!
    @IBOutlet weak var lockBtnImg1          : UIImageView!
    @IBOutlet weak var lockBtnImg2          : UIImageView!
    @IBOutlet weak var lockBtnImg3          : UIImageView!
    @IBOutlet weak var lockBtnImg4          : UIImageView!
    @IBOutlet weak var quizTitleLabel       : UILabel!
    @IBOutlet weak var liveUsersView        : UIView!
    @IBOutlet weak var questionLabel        : UILabel!
    @IBOutlet weak var earnCoinsView        : UIView!
    @IBOutlet weak var lifeLineView         : UIView!
    @IBOutlet weak var lifeLineLabel        : UILabel!
    @IBOutlet weak var earnCoinsLabel       : UILabel!
    @IBOutlet weak var containerView        : UIView!
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var youHavelifeLifeHeight        : NSLayoutConstraint!
    @IBOutlet weak var activitySponsorViweHeight        : NSLayoutConstraint!
    @IBOutlet weak var backgroundImageView        : UIImageView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var backgroundTransperentViewForVideo: UIView!
    @IBOutlet weak var sponsorTitle : UILabel! //beneath Live users
    @IBOutlet weak var sponsorTitle2 : UILabel! // next to you have lifeline view
    @IBOutlet weak var lifeLineCount : UILabel!
    
    var progressCount: Float = 0.0
    @IBOutlet weak var displayProgressView: UIProgressView!
    
    @IBOutlet weak var questionScroolView: UIScrollView!
    @IBOutlet weak var videoDisplayView: UIView!
    @IBOutlet weak var imageDisplayImageView: UIImageView!
    @IBOutlet weak var imgVideoDisplayView: UIView!
    @IBOutlet weak var lifeSponsorImg : UIImageView!
    @IBOutlet weak var activitySponsorImg : UIImageView!
    @IBOutlet weak var lifeSponsorView: UIView!
    @IBOutlet weak var activitySponsorView: UIView!
    @IBOutlet weak var buttonOneHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonTwoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonThreeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonFourHeightConstraint: NSLayoutConstraint!
    
    var displayTimer : Timer?
    @objc static var shared                     = QuestionViewController()
    
    public static var nextActivityId : String!
    public static var correct_ans_cnt: Int      = 0
    public static var outOfTheGame   : Bool!    = false
    public static var nextActivityMap           = [String: String]()
    public static var optionsArray              = [String]()
    public static var coinsToEarn    : Int      = 0
    public static var isLateJoined = false
    var lengthToDisplay : Float!
    var differenceInMediaPosted : Float!
    let questionImgView     = UIImageView()
    //Questions Views
    open var activityPostedMessage: ActivityDetails!
    open var timeToAnswer   = 10 //10 sec
    open var activityResultMessage: ActivityResultMessage!
    // Answers views
    var isAnswer            : Bool    = false
    var answerCountdownTimer: Timer!
    var questionVisibleTime : Date    = Date()
    var isQuestion          : Bool    = false
    var isAnswerClicked     : Bool    = false
    var quizQuestion        : String  = ""
    var height_constant     : CGFloat = 35
    var image_constant      : CGFloat = 100
    var isPlaying           : Bool    = true
    var optionsButtonsArray : [UIButton]   = [UIButton]()
    var tempButtonsArray : [UIButton]   = [UIButton]()
    var tempButtonsHeightArray : [NSLayoutConstraint] = [NSLayoutConstraint]()
    var ANSWER_TIMER        : Int          = 8
    var usersConnected      : Int          = 1
    var activityId          : String  = ""
    var audioPlayer         : AVAudioPlayer!
    var questionAudio       : AVAudioPlayer!
    var flag                          = 1
    var tickTimer                    : Timer!
    var mediaContentUrl              : String?
    var timeForQuestionWatch                    = 0
    var timerArray                              = [Int]()
    var player                       : AVPlayer!
    var playerLayer                  : AVPlayerLayer!
    var isQuestionAudioPlaying                  = false
    var success                                 = 0
    var lifeCount                               = 0
    var useLifeTimer                 : Timer!
    var lifeView                     : UIImageView!
    var userRequest                  : UseLifeRequest!
    var wrongAnsPopupView            : WrongAnswerAndUseLife!
    let containerViewConstant        : CGFloat = 140                 //height for contentview
    var currentActivity              : Int!
    var canUseLifeLineVar : Bool!
    var bgViewForPopup : UIView!
    var activitySponsorUrl : String!
    var lifeSponsorUrl : String!
    var gradientLayer1       : CAGradientLayer!
    var gradientLayer2       : CAGradientLayer!
    var gradientLayer3       : CAGradientLayer!
    var gradientLayer4       : CAGradientLayer!
    var gradientLayer5       : CAGradientLayer!
    var gradientLayer6       : CAGradientLayer!
    var tempView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        self.tempButtonsArray = [self.buttonOptionOne, self.buttonOptionTwo, self.buttonOptionThree, self.buttonOptionFour]
        self.tempButtonsHeightArray = [self.buttonOneHeightConstraint,self.buttonTwoHeightConstraint,self.buttonThreeHeightConstraint,self.buttonFourHeightConstraint]
        for i in tempButtonsHeightArray {
            i.constant = 0
        }
        self.gradientLayer1 = CAGradientLayer()
        self.gradientLayer2 = CAGradientLayer()
        self.gradientLayer4 = CAGradientLayer()
        self.gradientLayer5 = CAGradientLayer()
        self.gradientLayer6 = CAGradientLayer()
        self.imgVideoDisplayView.isHidden = true
        //set initial height for container view
        self.containerViewHeightConstraint.constant = 0
        self.usersConnected = Int(UserDefaults.standard.string(forKey: "users_connected") ?? "1") ?? 1
        self.lifeCount = UserDefaults.standard.integer(forKey: "life")
        
        if self.activityPostedMessage != nil {
            isQuestion = true
            if(activityPostedMessage.getOptions().count > 0 ){
                for i in 0 ..< activityPostedMessage.getOptions().count {
                    let btn = tempButtonsArray[i]
                    btn.tag = i
                    btn.asRect()
                    btn.isHidden = false
                    btn.backgroundColor = Colors().btnBackgroundGrey
                    self.tempButtonsHeightArray[i].constant = 45
                    optionsButtonsArray.append(btn)
                }
            }
            UserDefaults.standard.set(-1, forKey: "selectedOption")
            
        } else {
            isAnswer = true
            if(activityResultMessage.getOptionsResult().count > 0 ){
                for i in 0 ..< activityResultMessage.getOptionsResult().count {
                    let btn = tempButtonsArray[i]
                    btn.tag = i
                    btn.asRect()
                    btn.isHidden = false
                    btn.backgroundColor = Colors().btnBackgroundGrey
                    self.tempButtonsHeightArray[i].constant = 45
                    optionsButtonsArray.append(btn)
                }
            }
        }
        let users_connected = UserDefaults.standard.string(forKey: "users_connected") ?? "1"
        let formattedUsers = Int(users_connected)?.getFormmatedCoins()
        numberOfPlayersLabel.text = formattedUsers!
        self.isQuestionAudioPlaying = false
        
        if QuestionViewController.coinsToEarn == 0 {
            self.earnCoinsView.isHidden = true
            self.earnCoinsView.translatesAutoresizingMaskIntoConstraints = false
            self.earnCoinsView.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.earnCoinsView.updateConstraints()
        } else {
            if let coins : Int = QuestionViewController.coinsToEarn {
                self.earnCoinsLabel.text = "Earn " + coins.description + " coins"
            } else {
                self.earnCoinsLabel.text = "Earn 0 coins"
            }
        }
        
        self.questionTimer.isHidden = true
        if isQuestion {
            loadQuestionData()
        } else {
            loadAnswerData()
        }
        //setup audio player
        do {
            let soundEffect = SPN_EMS_Bundle.path(forResource: "tick", ofType: "mp3")
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: soundEffect!))
        } catch {
            // couldn't load
        }
        self.lifeSponsorView.isUserInteractionEnabled = true
        self.activitySponsorView.isUserInteractionEnabled = true
        lifeSponsorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openUrl)))
        activitySponsorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openUrl)))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.gradientLayer1.frame = self.navigationView.bounds
        self.gradientLayer2.frame = self.backgroundTransperentViewForVideo.bounds
        self.gradientLayer4.frame = self.buttonOptionOne.bounds
        self.gradientLayer5.frame = self.buttonOptionOne.bounds
        self.gradientLayer6.frame = self.buttonOptionOne.bounds
        if(playerLayer != nil){
            playerLayer.frame = self.videoDisplayView.bounds
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationView.applyGradient(colours: [Colors().shareTitleGradient1, Colors().shareTitleGradient2], gradientOrientation: .vertical, gradientLayer: self.gradientLayer1)
        
        self.usersConnected = Int(UserDefaults.standard.string(forKey: "users_connected") ?? "1") ?? 1
        
        for i in 0 ..< optionsButtonsArray.count {
            let btn = optionsButtonsArray[i]
            btn.tag = i
            btn.asRect()
            btn.backgroundColor = Colors().btnBackgroundGrey
        }
        do{
            self.canUseLifeLineVar = try QuizEngineRules.shared.canUseLifeLine(currentQuestion: currentActivity)
        }
        catch {
            print(error)
        }
        // view for you have -- lifelines
        if(lifeCount > 0 && canUseLifeLineVar && !(QuestionViewController.isLateJoined)){
            self.lifeLineView.isHidden = true
            self.youHavelifeLifeHeight.constant = 0
            self.lifeLineLabel.text = "You have " + self.numberInWords(number: lifeCount) + " lifeline"
            self.lifeLineView.asCircle()
            self.lifeLineCount.text = lifeCount.description
            
            if let activityPostedMessage = self.activityPostedMessage {
                if activityPostedMessage.lifeSponsor != nil {
                    self.setLifeSponsor()
                }
                else{
                    setNoLifeSponsorView()
                }
            }
            if let activityResultMessage = self.activityResultMessage {
                if activityResultMessage.lifeSponsor != nil {
                    self.setLifeSponsor()
                }
                else{
                    setNoLifeSponsorView()
                }
            }
        }
        else{
            self.youHavelifeLifeHeight.constant = 0
            self.lifeLineView.isHidden = true
        }
        
        self.liveUsersView.asCircle()
        self.lifeLineView.asCircle()
        self.lifeLineView.backgroundColor = Colors().coinsViewBackground
        self.questionLabel.layer.cornerRadius = self.questionLabel.frame.size.height / 2
        
        self.containerView.backgroundColor = .clear
        
        let currentEventName = UserDefaults.standard.string(forKey: "currentEventName")
        self.quizTitleLabel.text = currentEventName
        
        self.bgViewForPopup = UIView(frame: self.view.frame) // frame for background view for popup
        self.bgViewForPopup.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        
        // Set Activity Sponsors
        self.setActivitySponsor()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //self.questionTimer.labelFont = UIFont(name: "Arial", size: 30)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: .unmuteVideo, object: nil)
        self.cleanupView()
    }
    
    func cleanupView(){
        if(tickTimer != nil){
            tickTimer.invalidate()
            tickTimer = nil
            audioPlayer.stop()
        }
        if displayTimer != nil {
            displayTimer?.invalidate()
            displayTimer = nil
        }
        if(self.isQuestionAudioPlaying){
            questionAudio.stop()
        }
        if(player != nil){
            player.pause()
        }
        if(playerLayer != nil){
            playerLayer.removeFromSuperlayer()
        }
        NotificationCenter.default.removeObserver(self)
    }
    
    func startTickTimer(){
        if tickTimer == nil {
            tickTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTickTimer), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTickTimer(){
        audioPlayer.play()
        audioPlayer.volume = 1.0
    }
    
    func loadQuestionData() {
        
        self.quizQuestion = self.activityPostedMessage.question
        
        if self.activityPostedMessage.subType != "TEXT" {
            if self.timeToAnswer > self.activityPostedMessage.timeToAnswer {
                if let urlString = self.activityPostedMessage.url {
                    self.setContent(urlString: urlString) //set image,audio video to question
                }
            }
        } else {
            self.backgroundImageView.alpha = 0.5
        }
        
        if QuestionViewController.nextActivityMap.count != 0 {
            var key: String!
            for act in QuestionViewController.nextActivityMap {
                key = act.key
                break
            }
        }
        
        if self.activityPostedMessage.getOptions().count != 0 {
            QuestionViewController.optionsArray = []
            for opt in self.activityPostedMessage.getOptions() {
                QuestionViewController.optionsArray.append(opt)
            }
        }
        
        //for showing current question number out of total activity
        currentActivity = UserDefaults.standard.integer(forKey: "currentActivity")
        let totalActivity = QuizEngineRules.shared.getTotalQuestions()
        questionNumberLabel.text = "Question " + currentActivity.description + " of " + totalActivity.description
        
        if QuestionViewController.optionsArray.count > 0 {
            // Display question and option
            questionLabel.text = self.quizQuestion
            self.setAnswerOptionsToButtons()
            if self.activityPostedMessage.subType == "TEXT" {
                self.setTimer(timeToAnswer: self.timeToAnswer)
                self.showTimerView()
            } else {
                if  self.timeToAnswer <= self.activityPostedMessage.timeToAnswer {
                    // show question directly
                    self.setTimer(timeToAnswer: self.timeToAnswer)
                    self.showTimerView()
                } else {
                    //Do nothing
                }
            }
        }
    }
    
    func showTimerView() {
        self.questionTimer.isHidden = false
        self.questionTimer.delegate = self
        self.questionTimer.lineWidth = 5.0
        self.questionTimer.isLabelHidden = true
        self.questionTimer.lineColor = Colors().orangeText
        self.questionTimer.trailLineColor = UIColor.clear
        self.timerLabel.asCircle()
    }
    
    @objc func updateProgressTimer() {
        let count = self.activityPostedMessage.length
        if self.progressCount <= 1.0{
            UIView.animate(withDuration: TimeInterval(Float(differenceInMediaPosted))) {
                self.displayProgressView.setProgress(1.0, animated: true)
                self.progressCount = self.progressCount + 0.1
            }
        }
        if (self.progressCount) >= 1.0 {
            self.progressCount = 0.0
            self.questionScroolView.isHidden = false
            self.imgVideoDisplayView.isHidden = true
            displayTimer?.invalidate()
            if player != nil{
                self.player.pause()
            }
            self.tempView.isHidden = false
            let newT = self.timeToAnswer - Int(differenceInMediaPosted)
            if  newT <= self.timeToAnswer {
                self.setTimer(timeToAnswer: newT)
                self.showTimerView()
                NotificationCenter.default.post(name: .unmuteVideo, object: nil)
            } else {
                self.setTimer(timeToAnswer: self.timeToAnswer)
                self.showTimerView()
                NotificationCenter.default.post(name: .unmuteVideo, object: nil)
            }
        }
    }
    
    func resolutionSizeForLocalVideo(url:NSURL) -> CGSize? {
        guard let track = AVAsset(url: url as URL).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: fabs(size.width), height: fabs(size.height))
    }
    
    //for setting audio,video,image & GIF content into the question
    func setContent(urlString : String){
        lengthToDisplay = Float(self.activityPostedMessage.length)
        differenceInMediaPosted = Float(self.timeToAnswer - self.activityPostedMessage.timeToAnswer)
        if let eventImgUrl = UserDefaults.standard.string(forKey: "currentEventImage") {
            let url = URL(string: eventImgUrl)
        }
        let url = URL(string: urlString)
        let ext = url?.pathExtension
        if(ext == "cms" || ext == "gif" || ext == "jpg" || ext == "png"  || ext == "jpeg" || self.activityPostedMessage.subType == "IMAGE"){
            containerView.isHidden = true
            self.questionScroolView.isHidden = true
            self.imgVideoDisplayView.isHidden = false
            videoDisplayView.isHidden = true
            self.displayProgressView.isHidden = false
            // Progress
            self.addGradienForImgVideoDisplayView(frontView: imageDisplayImageView)
            if differenceInMediaPosted == lengthToDisplay{
                displayTimer = Timer.scheduledTimer(timeInterval: TimeInterval(lengthToDisplay/10), target: self, selector: #selector(updateProgressTimer), userInfo: nil, repeats: true)
            } else {
                displayTimer = Timer.scheduledTimer(timeInterval: TimeInterval(differenceInMediaPosted/10), target: self, selector: #selector(updateProgressTimer), userInfo: nil, repeats: true)
            }
            self.containerViewHeightConstraint.constant = 0
            imageDisplayImageView.frame = self.imgVideoDisplayView.bounds
            imgVideoDisplayView.addSubview(imageDisplayImageView)
            self.mediaContentUrl = urlString
            self.imageDisplayImageView.contentMode = .scaleAspectFit
            if let url = url {
                if (!((self.mediaContentUrl?.contains("gif"))!)){
                    self.imageDisplayImageView.sd_setImage(with: url)
                } else {
                    DispatchQueue.main.async {
                        let data = try? Data(contentsOf: url)
                        if let data = data {
                            self.imageDisplayImageView.image = UIImage.gifImageWithData(data)
                        }
                    }
                }
            }
        }
        else if ext == "mp4" {
            if let url = url {
                player = AVPlayer(url: url)
                self.imgVideoDisplayView.isHidden = false
                self.videoDisplayView.isHidden = false
                self.questionScroolView.isHidden = true
                self.displayProgressView.isHidden = false
                if differenceInMediaPosted == lengthToDisplay{
                    displayTimer = Timer.scheduledTimer(timeInterval: TimeInterval(lengthToDisplay/10), target: self, selector: #selector(updateProgressTimer), userInfo: nil, repeats: true)
                } else {
                    displayTimer = Timer.scheduledTimer(timeInterval: TimeInterval(differenceInMediaPosted/10), target: self, selector: #selector(updateProgressTimer), userInfo: nil, repeats: true)
                }
                playerLayer = AVPlayerLayer(player: player)
                self.containerViewHeightConstraint.constant = 0
                self.addGradienForImgVideoDisplayView(frontView: videoDisplayView)
                videoDisplayView.backgroundColor = .clear
                videoDisplayView.isHidden = false
                playerLayer.frame = self.videoDisplayView.bounds
                playerLayer.videoGravity = .resizeAspect
                
                self.videoDisplayView.layer.addSublayer(playerLayer)
                self.imageDisplayImageView.isHidden = true
                NotificationCenter.default.post(name: .muteVideo, object: nil)
                player.play()
                NotificationCenter.default.addObserver(self, selector: #selector(pauseVideoPlayer), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
            }
        }
        else if ext == "mp3" {
            do {
                let data = try? Data(contentsOf: url!)
                if let data = data {
                    questionAudio = try AVAudioPlayer(data: data)
                    self.isQuestionAudioPlaying = true
                    NotificationCenter.default.post(name: .muteVideo, object: nil)
                    questionAudio.play()
                    questionAudio.volume = 1.0
                }
            } catch {
                return
            }
        }
    }
    
    
    @objc func pauseVideoPlayer(){
        self.player.pause()
        NotificationCenter.default.post(name: .unmuteVideo, object: nil)
    }
    
    func setAnswerOptionsToButtons() {
        for i in 0 ..< optionsButtonsArray.count {
            
            let btn = optionsButtonsArray[i]
            if QuestionViewController.optionsArray.count > i && QuestionViewController.optionsArray[i] != "" {
                btn.setTitle(QuestionViewController.optionsArray[i], for: .normal)
                btn.setTitleColor(Colors().white, for: .normal)
            }
        }
    }
    
    func endTimerForQuiz(){
        if (isAnswerClicked) {
            EngageEventHandler.shared.questionViewController = nil
            self.dismiss(animated: false, completion: nil)
            //self.navigationController?.popViewController(animated: true)
        } else {
            UserDefaults.standard.set(-1, forKey: "selectedOption")
            self.setOptionsDisabled()
            // QuestionViewController.outOfTheGame = true
            // If answer is not given show 'Time Up' message and close Quiz UI after 3 sec
            self.isAnswerClicked = false
            if(QuestionViewController.nextActivityId != nil){
                self.sendAnswer(tag: -1)
            }
            self.showPopUpForTimeUp()
            QuestionViewController.isLateJoined = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                EngageEventHandler.shared.questionViewController = nil
                self.dismiss(animated: false, completion: nil)
                //self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    //    //to convert number to words
    func numberInWords(number : Int) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .spellOut
        let english = formatter.string(from: NSNumber(integerLiteral: number))
        return english!
    }
    
    func setDisabledUI() {
        //Setting UI when user is not able play quiz
        for i in 0 ..< optionsButtonsArray.count {
            let btn = optionsButtonsArray[i]
            btn.backgroundColor = Colors().btnBackgroundGrey
            btn.setTitleColor(Colors().white, for: .normal)
        }
    }
    
    func setOptionsDisabled() {
        for i in 0 ..< optionsButtonsArray.count {
            let btn = optionsButtonsArray[i]
            btn.isUserInteractionEnabled = false
        }
    }
    
    func setEnabledUI(){
        //Setting UI when user can play quiz
        for i in 0 ..< optionsButtonsArray.count {
            let btn = optionsButtonsArray[i]
            btn.backgroundColor = Colors().btnBackgroundGrey
        }
        self.setOptionsEnabled()
    }
    
    func setOptionsEnabled() {
        for i in 0 ..< optionsButtonsArray.count {
            let btn = optionsButtonsArray[i]
            btn.isUserInteractionEnabled = true
        }
    }
    
    // 10 Sec timer for questions
    func setTimer(timeToAnswer : Int) {
        questionVisibleTime = Date()
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            if self.activityPostedMessage != nil {
                self.startTickTimer()
            }
            self.questionTimer.start(beginingValue: timeToAnswer, interval: 1)
        }
    }
    
    //#MARK: buttons action handling
    @IBAction func backQuiz(_ sender: Any) {
        if(tickTimer != nil){
            tickTimer.invalidate()
            tickTimer = nil
        }
        NotificationCenter.default.post(name: .unmuteVideo, object: nil)
        self.dismiss(animated: false, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func optionOneAction(_ sender: UIButton) {
        self.lockBtnImg1.isHidden = false
        self.isAnswerClicked = true
        self.setBtnViewAfterAnsClicked(sender: sender)
        self.sendAnswer(tag: sender.tag)
    }
    
    @IBAction func optionTwoAction(_ sender: UIButton) {
        self.lockBtnImg2.isHidden = false
        self.isAnswerClicked = true
        self.setBtnViewAfterAnsClicked(sender: sender)
        self.sendAnswer(tag: sender.tag)
    }
    
    @IBAction func optionThreeAction(_ sender: UIButton) {
        self.lockBtnImg3.isHidden = false
        self.isAnswerClicked = true
        self.setBtnViewAfterAnsClicked(sender: sender)
        self.sendAnswer(tag: sender.tag)
    }
    
    @IBAction func optionFourAction(_ sender: UIButton) {
        self.lockBtnImg4.isHidden = false
        self.isAnswerClicked = true
        self.setBtnViewAfterAnsClicked(sender: sender)
        self.sendAnswer(tag: sender.tag)
    }
    
    func setBtnViewAfterAnsClicked(sender: UIButton) {
        self.setAnswerViewForGivenAnswer(cnt: sender.tag)
        for i in 0 ..< optionsButtonsArray.count {
            let btn = optionsButtonsArray[i]
            if btn.tag == sender.tag {
                btn.applyGradient(colours: [Colors().gradientOrange3, Colors().gradientOrange4], gradientOrientation: .vertical)
                btn.setTitleColor(Colors().white, for: .normal)
                btn.isEnabled = false
            } else {
                btn.isEnabled = false
            }
        }
    }
    
    //View in Answer Result for given answer
    func setAnswerViewForGivenAnswer(cnt : Int){
        let str1 = NSMutableAttributedString(string: "Your Answer\n", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10.0)])
        let str2 = NSMutableAttributedString(string: "\((optionsButtonsArray[cnt].titleLabel?.text)!)", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.0)])
        str1.append(str2)
        optionsButtonsArray[cnt].titleLabel?.numberOfLines = 0
        optionsButtonsArray[cnt].titleLabel?.textAlignment = .center
        optionsButtonsArray[cnt].setAttributedTitle(str1, for: .normal)
    }
    
    @objc func openUrl(){
        if self.activitySponsorUrl != nil {
            self.activitySponsorUrl.openUrl()
        }
        if self.lifeSponsorUrl != nil {
            self.lifeSponsorUrl.openUrl()
        }
    }
}

//#MARK: SRCountDownTimerDelegate methods
extension QuestionViewController: SRCountdownTimerDelegate {
    
    func timerDidUpdateCounterValue(newValue: Int) {
        self.timerLabel.text = newValue.description
    }
    
    func timerDidEnd() {
        audioPlayer.stop()
        if(tickTimer != nil){
            tickTimer.invalidate()
            tickTimer = nil
        }
        self.endTimerForQuiz()
    }
}

//#MARK: Answering the quiz
extension QuestionViewController {
    func useLife() {
        let userId = UserDefaults.getVideoQuizDetails()?.id
        let eventId = UserDefaults.standard.string(forKey: "eventId")
        userRequest = UseLifeRequest(activityId: LGEngageMessageUtils.WS_ACTION_TYPE_USE_LIFE, eventId: eventId!, userId: userId!)
        if(lifeCount > 0){
            LGEngageManagerImp.shared.useLife(request: userRequest, delegate: self)
        }
    }
    
    func setAnswerViews() {
        self.setOptionsDisabled()
        let selectedOption = UserDefaults.standard.integer(forKey: "selectedOption")
        self.quizQuestion = self.activityResultMessage.activity
        
        self.lifeCount = UserDefaults.standard.integer(forKey: "life")
        
        QuestionViewController.optionsArray = []
        for opt in self.activityResultMessage.getOptionsResult() {
            QuestionViewController.optionsArray.append(opt.getOption())
        }
        
        if QuestionViewController.optionsArray.count > 0 {
            // Display question and option
            questionLabel.text = self.quizQuestion
            self.setAnswerOptionsToButtons()
        }
        
        //for showing current question number out of total activity
        let totalActivity = QuizEngineRules.shared.getTotalQuestions()
        questionNumberLabel.text = "Question " + self.currentActivity.description + " of " + totalActivity.description
        
        //for lives
        var correctAnsPosition = 0
        let optionRes =  activityResultMessage.getOptionsResult()
        for i in 0 ..< optionRes.count {
            if(optionRes[i].isCorrect){
                correctAnsPosition = i
                break
            }
        }
        var cnt = 0
        var correctOption  = 0
        var totalUsers = 0
        var arr = [Int]()
        var optionResult = [CGFloat]()
        var count = 0
        
        for option in self.activityResultMessage.getOptionsResult(){
            totalUsers = totalUsers + Int(option.result)!
            arr.append(Int(option.result) ?? 0)
        }
        
        for option in self.activityResultMessage.getOptionsResult(){
            optionResult.append(CGFloat(arr[count])/CGFloat(totalUsers))
            count = count + 1
        }
        
        for option in self.activityResultMessage.getOptionsResult() {
            //let optionStr = option.option
            if(cnt == 4){
                break
            }
            let result = Int(option.result) ?? 0
            let isCorrect = option.isCorrect ?? false
            
            if totalUsers == 0 {
                totalUsers = 1
            }
            
            let total_users_percentage : CGFloat = CGFloat(result)/CGFloat(totalUsers) * 100
            let btn = optionsButtonsArray[cnt]
            if QuestionViewController.optionsArray.count >= cnt && QuestionViewController.optionsArray[cnt] != "" {
                btn.setTitle(QuestionViewController.optionsArray[cnt] + "(" + Int(total_users_percentage).description + "%)" , for: .normal)
            }
            if isCorrect {
                correctOption = cnt
            }
            
            if selectedOption == cnt && isCorrect {
                QuestionViewController.correct_ans_cnt += 1
                self.optionsButtonsArray[cnt].applyGradient(colours: [Colors().trueOptionGradient1, Colors().trueOptionGradient2], gradientOrientation: .vertical, gradientLayer: gradientLayer4)
                self.setAnswerViewForGivenAnswer(cnt: cnt)
                
            } else if selectedOption == cnt && !isCorrect {
                //                QuizEngineRules.shared.setResult(currentQuestion: self.currentActivity, is_correct: isCorrect)
                self.optionsButtonsArray[cnt].applyGradient(colours: [Colors().wrongOptionGradient1, Colors().wrongOptionGradient2], gradientOrientation: .vertical, gradientLayer: gradientLayer5)
                self.setAnswerViewForGivenAnswer(cnt: cnt)
                
                // show popup for last question
                if self.currentActivity == QuizEngineRules.shared.getTotalQuestions() && !QuestionViewController.outOfTheGame {
                    self.showPopUpToGetPointsOnly()
                } else if QuestionViewController.outOfTheGame {
                    self.view.makeToast(StringConstants().wrongAnsTitle, duration: 3.0, position: .bottom)
                }
                if QuestionViewController.outOfTheGame {
                    self.view.makeToast(StringConstants().wrongAnsTitle, duration: 3.0, position: .bottom)
                }
                else if self.currentActivity < QuizEngineRules.shared.getTotalQuestions() {
                    
                    let lifeCount = UserDefaults.standard.integer(forKey: "life")
                    do {
                        let canUseLifeline = try QuizEngineRules.shared.canUseLifeLine(currentQuestion: self.currentActivity)
                        
                        if lifeCount > 0 && canUseLifeline && !QuestionViewController.outOfTheGame {
                            //Call pop up to show use life option for wrong answer
                            if(!activityResultMessage.getIsLastQuestion() && selectedOption != correctAnsPosition && selectedOption != -1){
                                self.showPopUpToUseLife()
                            }
                            else if try QuizEngineRules.shared.canPlayForCashPrize(currentQuestion: self.currentActivity) && !QuestionViewController.outOfTheGame {
                                QuestionViewController.outOfTheGame = true
                                self.showPopUpToGetPointsOnly()
                            }
                        } else if try QuizEngineRules.shared.canPlayForCashPrize(currentQuestion: self.currentActivity) && !QuestionViewController.outOfTheGame {
                            QuestionViewController.outOfTheGame = true
                            self.showPopUpToGetPointsOnly()
                        }
                        else{
                            if(!QuestionViewController.outOfTheGame){
                                self.showPopUpToGetPointsOnly()
                                QuestionViewController.outOfTheGame = true
                            }
                        }
                        
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }
            else if selectedOption != cnt && !isCorrect{
                self.optionsButtonsArray[cnt].backgroundColor = Colors().btnBackgroundGrey
            }
            else if selectedOption != cnt && isCorrect{
                self.optionsButtonsArray[cnt].backgroundColor = Colors().btnBackgroundGrey
            }
            cnt += 1
        }
        
        self.optionsButtonsArray[correctOption].applyGradient(colours: [Colors().trueOptionGradient1, Colors().trueOptionGradient2], gradientOrientation: .vertical, gradientLayer: gradientLayer6)
        
        do {
            if self.currentActivity == QuizEngineRules.shared.getTotalQuestions() {
                if try QuizEngineRules.shared.isWinner(currentQuestion: self.currentActivity) {
                    UserDefaults.standard.set(true, forKey: "isLastAnswerCorrect")
                } else {
                    UserDefaults.standard.set(false, forKey: "isLastAnswerCorrect")
                }
            }
        } catch {
            
        }
        UserDefaults.standard.set(-1, forKey: "selectedOption")
    }
    
    func loadAnswerData() {
        self.questionTimer.isHidden = true
        if self.activityResultMessage != nil {
            if self.activityResultMessage.getOptionsResult().count > 0 {
                // TBD set answer views
                self.setAnswerViews()
            }
        }
        self.startAnswerTimer()
    }
    
    func startAnswerTimer() {
        answerCountdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateAnswerTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateAnswerTime() {
        if self.ANSWER_TIMER >= 0 {
            self.ANSWER_TIMER -= 1
        } else {
            answerCountdownTimer.invalidate()
            QuestionViewController.optionsArray = []
            self.dismiss(animated: false, completion: nil)
            //self.navigationController?.popViewController(animated: true)
        }
    }
    
    func answerQuestion(answer: ActivityResponseMessage) {
        do {
            let jsonparams = [LGEngageMessageUtils.WS_MSG_FIELD_ACTION : answer.getAction(),
                              LGEngageMessageUtils.WS_MSG_FIELD_USER_ID: answer.getUserId(),
                              LGEngageMessageUtils.WS_MSG_FIELD_EVENT_ID: answer.getEventId(),
                              LGEngageMessageUtils.WS_MSG_FIELD_NEW_ACTIVITY_ID: answer.activityId,
                              LGEngageMessageUtils.WS_MSG_FIELD_ANSWER: answer.getAnswerIndex()] as [String : Any]
            
            let request = try JSONSerialization.data(withJSONObject: jsonparams, options: [])
            let msgString = String(data: request, encoding: String.Encoding.ascii)!
            LGEngageManagerImp.shared.sendMessage(message: msgString)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func sendAnswer(tag: Int) {
        var timeToAnsTaken = self.timeToAnswer.description
        if ((self.timerLabel.text != "") && (self.timerLabel.text != "0")) {
            timeToAnsTaken = (self.timeToAnswer - Int(self.timerLabel.text ?? "0")!).description
        }
        UserDefaults.standard.set(timeToAnsTaken, forKey: "timeTakenToAnswer")
        let answerDuration = LGEngageMessageUtils.getDiffTimeInSec(q_time: questionVisibleTime, c_time: Date())
        
        if QuizEngineRules.shared.getIsAnswered(currentQuestion: self.currentActivity) == false {
            UserDefaults.standard.set(tag, forKey: "selectedOption")
            QuizEngineRules.shared.setIsAnswered(currentQuestion: self.currentActivity, is_answered: true)
            if isAnswerClicked {
                let answer: ActivityResponseMessage = ActivityResponseMessage(action: LGEngageMessageUtils.WS_ACTION_TYPE_ANSWER_QUESTION, activityId: QuestionViewController.nextActivityId ?? "0", eventId: UserDefaults.standard.string(forKey: "eventId") ?? "", userId: (UserDefaults.getVideoQuizDetails()?.id)!, answer: tag, answerDuration: Double(answerDuration))
                answerQuestion(answer: answer)
            } else {
                let answer: ActivityResponseMessage = ActivityResponseMessage(action: LGEngageMessageUtils.WS_ACTION_TYPE_ANSWER_QUESTION, activityId: QuestionViewController.nextActivityId, eventId: UserDefaults.standard.string(forKey: "eventId") ?? "", userId: (UserDefaults.getVideoQuizDetails()?.id)!, answer: -1)
                UserDefaults.standard.set(-1, forKey: "selectedOption")
                answerQuestion(answer: answer)
            }
        }
    }
    
    @objc func stopUseLifeAnimation(){
        lifeView.isHidden = true
    }
}

extension QuestionViewController : OnUseLifeResponseDelegate {
    
    func onResponse(response: UseLifeResponse) {
        QuestionViewController.outOfTheGame = false
        self.success = 1;
        self.view.makeToast(StringConstants().life_used, duration: 3.0, position: .bottom)
        QuestionViewController.correct_ans_cnt += 1
        self.sendMessageForLifeUsed()
        UserDefaults.standard.set(self.lifeCount - 1, forKey: "life")
        UserDefaults.standard.synchronize()
        lifeLineView.isHidden = true
        self.youHavelifeLifeHeight.constant = 0
    }
    
    func onFailure(error: String) {
        self.view.makeToast(error, duration: 3.0, position: .bottom)
    }
    
    func sendMessageForLifeUsed(){
        
        let userId = UserDefaults.getVideoQuizDetails()?.id
        let eventId = UserDefaults.standard.string(forKey: "eventId")
        
        let userRequest = UseLifeRequest(activityId: LGEngageMessageUtils.WS_ACTION_TYPE_USE_LIFE, eventId: eventId!, userId: userId!)
        
        do {
            let jsonparams = [LGEngageMessageUtils.WS_MSG_FIELD_ACTION : userRequest.activityId,
                              LGEngageMessageUtils.WS_MSG_FIELD_USER_ID: userRequest.userId,
                              LGEngageMessageUtils.WS_MSG_FIELD_EVENT_ID: userRequest.eventId]
            
            let request = try JSONSerialization.data(withJSONObject: jsonparams, options: [])
            let msgString = String(data: request, encoding: String.Encoding.ascii)!
            
            LGEngageManagerImp.shared.sendMessage(message: msgString)
            
        } catch {
            print(error.localizedDescription)
        }
    }
}

extension QuestionViewController {
    
    func setLifeSponsor(){
        /***************Life sponsor for Question***********************/
        
        if(activityPostedMessage != nil){
            if let lifeSponsor =  activityPostedMessage.lifeSponsor {
                self.lifeLineView.isHidden = false
                self.youHavelifeLifeHeight.constant = 40
                //for life sponsor image
                if let urlString = lifeSponsor.logoSmall {
                    if let url = URL(string: urlString){
                        self.lifeSponsorImg.sd_setImage(with: url)
                        self.lifeSponsorImg.isHidden = false
                    }
                }
                //for life sponsor name
                if let descriptionKey = lifeSponsor.descriptionKey {
                    let attributedString2 = NSMutableAttributedString(string: descriptionKey, attributes: [
                        .font: UIFont.systemFont(ofSize: 9.0, weight: .regular),
                        .foregroundColor: UIColor(white: 220.0 / 255.0, alpha: 1.0),
                        .kern: 0.0
                        ])
                    self.sponsorTitle2.attributedText = attributedString2
                }
                //set url for redirection
                if let lifeSponsor = activityPostedMessage.lifeSponsor{
                    if let url = lifeSponsor.url {
                        self.lifeSponsorUrl = url
                    }
                }
            }
        }
        
        /***************Life sponsor for Answer***********************/
        
        if(activityResultMessage != nil){
            if let lifeSponsor = activityResultMessage.lifeSponsor{
                self.lifeLineView.isHidden = false
                self.youHavelifeLifeHeight.constant = 40
                if let urlString = lifeSponsor.logoSmall {
                    if let url = URL(string: urlString){
                        self.lifeSponsorImg.sd_setImage(with: url)
                        self.lifeSponsorImg.isHidden = false
                    }
                }
                if let descriptionKey = lifeSponsor.descriptionKey {
                    let attributedString2 = NSMutableAttributedString(string: descriptionKey, attributes: [
                        .font: UIFont.systemFont(ofSize: 9.0, weight: .regular),
                        .foregroundColor: UIColor(white: 220.0 / 255.0, alpha: 1.0),
                        .kern: 0.0
                        ])
                    self.sponsorTitle2.attributedText = attributedString2
                }
                if let url = lifeSponsor.url {
                    self.lifeSponsorUrl = url
                }
            }
        }
    }
    
    func setActivitySponsor(){
        /***************Activity sponsor for Question***********************/
        
        if(activityPostedMessage != nil){
            if let activitySponsor = activityPostedMessage.activitySponsor {
                if let url = URL(string: activitySponsor.logoBig) {
                   // self.activitySponsorImg.sd_setImage(with: url)
                    
                    if let data = try? Data(contentsOf: url) {
                        let i = UIImage(data: data)
                        let img = activitySponsorImg.resizeImage(image: i!, targetSize: CGSize(width: 40, height: 40))
                        self.activitySponsorImg.image = img
                    }
                    self.activitySponsorImg.isHidden = false
                }
                if let descriptionKey = activitySponsor.descriptionKey {
                    let attributedString1 = NSMutableAttributedString(string: descriptionKey, attributes: [
                        .font: UIFont.systemFont(ofSize: 11.0, weight: .regular),
                        .foregroundColor: UIColor(white: 220.0 / 255.0, alpha: 1.0),
                        .kern: 0.0
                        ])
                    self.sponsorTitle.attributedText = attributedString1
                    self.sponsorTitle.numberOfLines = 0
                }
                if let url = activitySponsor.url{
                    self.activitySponsorUrl = url
                }
            }
            else{
                self.activitySponsorViweHeight.constant = 0
            }
        }
        /***************Activity sponsor for Answer***********************/
        
        if(activityResultMessage != nil){
            if let activitySponsor =  activityResultMessage.activitySponsor {
                if let urlString = activitySponsor.logoSmall {
                    if let url = URL(string: urlString){
                        self.activitySponsorImg.sd_setImage(with: url)
                        self.activitySponsorImg.isHidden = false
                    }
                }
                if let descriptionKey = activitySponsor.descriptionKey {
                    let attributedString2 = NSMutableAttributedString(string: descriptionKey, attributes: [
                        .font: UIFont.systemFont(ofSize: 11.0, weight: .regular),
                        .foregroundColor: UIColor(white: 220.0 / 255.0, alpha: 1.0),
                        .kern: 0.0
                        ])
                    self.sponsorTitle.attributedText = attributedString2
                    self.sponsorTitle.numberOfLines = 0
                }
                if let activitySponsor = activityResultMessage.activitySponsor{
                    if let url = activitySponsor.url{
                        self.activitySponsorUrl = url
                    }
                }
            }
            else{
                self.activitySponsorViweHeight.constant = 0
            }
        }
    }
    
    func setNoLifeSponsorView(){
        
        self.lifeLineView.isHidden = true
        self.youHavelifeLifeHeight.constant = 25
        if let v = SPN_EMS_Bundle.loadNibNamed(LifelineWithoutSponsorView.indentifier, owner: self, options: nil)?.first as? LifelineWithoutSponsorView{
            self.view.addSubview(v)
            if let activityPostedMessage = self.activityPostedMessage {
                if !(activityPostedMessage.subType == "TEXT"){
                    self.tempView = v
                    v.isHidden = true
                    tempView.isHidden = true
                }
            }
            self.tempView = v
            v.anchor(top: containerView.bottomAnchor, leading: buttonOptionOne.leadingAnchor, bottom: nil, trailing: buttonOptionOne.trailingAnchor,padding: .zero,size: .init(width: 0, height: 25))
            v.youHaveLifelineText.text = "You have " + self.numberInWords(number: lifeCount) + " lifeline"
        }
    }
    
    func addGradienForImgVideoDisplayView(frontView : UIView) {
        let v = UIView(frame: .init(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        v.applyGradient(colours: [Colors().eventImgGradient1, Colors().eventImgGradient2], gradientOrientation: .vertical)
        self.imgVideoDisplayView.addSubview(v)
        self.imgVideoDisplayView.bringSubviewToFront(frontView)
        self.imgVideoDisplayView.bringSubviewToFront(displayProgressView)
    }
}
