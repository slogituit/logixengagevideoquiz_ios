//
//  WrongAnswerAndUseLife.swift
//  SonyLivQuizIos
//
//  Created by Abbas on 23/08/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit

class WrongAnswerAndUseLife: UIView {

    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var keyImageVIew: UIImageView!
    var gradient: CAGradientLayer!
    var descrText: String!
    var title: String!
    var btnTitle: String!

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        self.keyImageVIew.isHidden = true
        gradient = CAGradientLayer()
        self.doneBtn.applyGradient(colours: [Colors().gradientOrange3,Colors().gradientOrange4], gradientOrientation: .horizontal, gradientLayer: gradient)
        self.doneBtn.asRect()
        self.gradient.frame = self.doneBtn.bounds
        
        self.doneBtn.titleLabel?.textColor = Colors().white
        if title != nil {
            titleLabel.text = title
        }
        if descrText != nil {
            descriptionLabel.text = descrText
        }
        if btnTitle != nil {
            self.doneBtn.setTitle(btnTitle, for: .normal)
        }
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
        self.animShowForPopUp()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = self.doneBtn.bounds
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
