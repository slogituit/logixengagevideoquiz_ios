//
//  QuizExtensions.swift
//  SPN_EMS
//
//  Created by Apple on 09/09/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import Foundation
import LogixEngageFramework

//#MARK: Lifeline handling
extension QuestionViewController {
    func showPopUpForTimeUp() {
        if let useLifeLinePopView = SPN_EMS_Bundle.loadNibNamed("WrongAnswerAndUseLife", owner: self, options: nil)?.first as? WrongAnswerAndUseLife {
            self.wrongAnsPopupView = useLifeLinePopView
            self.setFrameForPopUp()
            self.view.addSubview(self.wrongAnsPopupView)
            self.wrongAnsPopupView.titleLabel.text = StringConstants().timeUpTitle
            self.wrongAnsPopupView.descriptionLabel.text = StringConstants().timeUpDescr
            self.setBackgoundForPopup(useLifeLinePopView: useLifeLinePopView)
            self.wrongAnsPopupView.doneBtn.setTitle(StringConstants().timeUpBtnTitle, for: .normal)
            self.wrongAnsPopupView.doneBtn.tag = 2
            self.wrongAnsPopupView.keyImageVIew.isHidden = true
            self.wrongAnsPopupView.doneBtn.addTarget(self, action: #selector(self.popUpBtnPressed(_:)), for: .touchUpInside)
            self.addTapGestureForPopUp(view: useLifeLinePopView)
        }
    }
    
    func showPopUpToGetPointsOnly() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            if let useLifeLinePopView = SPN_EMS_Bundle.loadNibNamed("WrongAnswerAndUseLife", owner: self, options: nil)?.first as? WrongAnswerAndUseLife {
                self.wrongAnsPopupView = useLifeLinePopView
                self.setFrameForPopUp()
                self.view.addSubview(self.wrongAnsPopupView)
                self.wrongAnsPopupView.titleLabel.text = StringConstants().wrongAnsTitle
                self.wrongAnsPopupView.descriptionLabel.text = StringConstants().wrongAnsDescription
                self.setBackgoundForPopup(useLifeLinePopView: useLifeLinePopView)
                self.wrongAnsPopupView.doneBtn.setTitle(StringConstants().wrongAnsBtnTitle, for: .normal)
                self.wrongAnsPopupView.doneBtn.tag = 1
                self.wrongAnsPopupView.keyImageVIew.isHidden = true
                self.wrongAnsPopupView.doneBtn.addTarget(self, action: #selector(self.popUpBtnPressed(_:)), for: .touchUpInside)
                self.addTapGestureForPopUp(view: useLifeLinePopView)
            }
        })
    }
    
    func showPopUpToUseLife() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            if let useLifeLinePopView = SPN_EMS_Bundle.loadNibNamed("WrongAnswerAndUseLife", owner: self, options: nil)?.first as? WrongAnswerAndUseLife {
                self.wrongAnsPopupView = useLifeLinePopView
                self.setFrameForPopUp()
                self.view.addSubview(self.wrongAnsPopupView )
                self.wrongAnsPopupView.titleLabel.text = StringConstants().useLifelineTitle
                self.wrongAnsPopupView.descriptionLabel.text = StringConstants().useLifelineText
                self.setBackgoundForPopup(useLifeLinePopView: useLifeLinePopView)
                self.wrongAnsPopupView.doneBtn.setTitle(StringConstants().useLifeBtnTitle, for: .normal)
                self.wrongAnsPopupView.doneBtn.tag = 3
                self.wrongAnsPopupView.keyImageVIew.isHidden = false
                self.wrongAnsPopupView.doneBtn.addTarget(self, action: #selector(self.popUpBtnPressed(_:)), for: .touchUpInside)
                self.addTapGestureForPopUp(view: useLifeLinePopView)
            }
        })
    }
    
    func showPopUpForYouAreLate() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            if let useLifeLinePopView = SPN_EMS_Bundle.loadNibNamed("WrongAnswerAndUseLife", owner: self, options: nil)?.first as? WrongAnswerAndUseLife {
                self.wrongAnsPopupView = useLifeLinePopView
                self.setFrameForPopUp()
                self.view.addSubview(self.wrongAnsPopupView)
                self.wrongAnsPopupView.titleLabel.text = StringConstants().youAreLate
                self.wrongAnsPopupView.descriptionLabel.text = StringConstants().youAreLateDescription
                self.setBackgoundForPopup(useLifeLinePopView: useLifeLinePopView)
                self.wrongAnsPopupView.doneBtn.setTitle(StringConstants().wrongAnsBtnTitle, for: .normal)
                self.wrongAnsPopupView.doneBtn.tag = 4
                self.wrongAnsPopupView.keyImageVIew.isHidden = true
                self.wrongAnsPopupView.doneBtn.addTarget(self, action: #selector(self.popUpBtnPressed(_:)), for: .touchUpInside)
                self.addTapGestureForPopUp(view: useLifeLinePopView)
            }
        })
    }
    
    func setFrameForPopUp(){
        //+ self.buttonOptionTwo.frame.size.height
        // let height = self.view.frame.height - self.buttonOptionTwo.frame.origin.y - 30
        let height = (self.view.frame.height / CGFloat(integerLiteral: 4)) - CGFloat(integerLiteral: 15)
        let topAnchor = self.view.frame.height - height
        self.wrongAnsPopupView.frame = CGRect(x:0, y:topAnchor, width: self.view.frame.width, height: height)
    }
    
    @objc func popUpBtnPressed(_ sender: UIButton) {
        if sender.tag == 3 {
            self.useLife()
            // to hide lifebar of no sponsor when lifeline is used
            self.hideLifeBar()
            QuizEngineRules.shared.setLifeLineUsed(currentQuestion: self.currentActivity, lifeline: true)
            self.youHavelifeLifeHeight.constant = 0
            self.liveUsersView.isHidden = true
            
            let userId = UserDefaults.getVideoQuizDetails()?.id
            let cpCustomerId =  UserDefaults.getVideoQuizDetails()?.cpCustomerID
            let appVersion =  UserDefaults.getVideoQuizDetails()?.appVersion
            let advId =  UserDefaults.getVideoQuizDetails()?.advId
            let lat = UserDefaults.getVideoQuizDetails()?.latitude
            let long = UserDefaults.getVideoQuizDetails()?.longitude
            let eventId = UserDefaults.standard.object(forKey: "LiveEventId") as? String ?? ""
            let eventName = UserDefaults.standard.object(forKey: "LiveEventName") as? String ?? ""
            
            GoogleAnalyticsHelper.shared.lifelineClick(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: eventId, eventName: eventName)
            
            SegmentAnalytics.shared.lifelineClick(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: eventId, eventName: eventName, latitude: lat ?? "", longitude: long ?? "")
            
            self.removePopUpViewFromSuperview()
        } else {
            self.removePopUpViewFromSuperview()
        }
    }
    
    @objc func removePopUpViewFromSuperview() {
        self.wrongAnsPopupView.removeFromSuperview()
        self.wrongAnsPopupView = nil
        self.bgViewForPopup.removeFromSuperview()
        self.bgViewForPopup = nil
    }
    
    func addTapGestureForPopUp(view : UIView){
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.removePopUpViewFromSuperview))
        view.addGestureRecognizer(gestureRecognizer)
    }
    
    func setBackgoundForPopup(useLifeLinePopView : WrongAnswerAndUseLife){
        self.view.addSubview(bgViewForPopup)
        self.view.bringSubviewToFront(bgViewForPopup)
        self.bgViewForPopup.fillSuperview()
        self.view.addSubview(self.wrongAnsPopupView)
        self.view.bringSubviewToFront(self.wrongAnsPopupView)
    }
    
    func hideLifeBar(){
        tempView.isHidden = true
        tempView.translatesAutoresizingMaskIntoConstraints = false
        tempView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        tempView.updateConstraints()
    }
}
