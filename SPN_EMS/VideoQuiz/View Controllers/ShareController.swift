//
//  ShareController.swift
//  LogixEngage
//
//  Created by Abbas's Mac Mini on 08/05/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit
import LogixEngageFramework
import GoogleInteractiveMediaAds

let gifCache = NSCache<AnyObject, AnyObject>()

class ShareController: UIViewController, IMAAdsLoaderDelegate, IMAAdsManagerDelegate {
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var timeToQuiz: UILabel!
    @IBOutlet weak var quizTitle: UILabel!
    @IBOutlet weak var liveUsersLabel: UILabel!
    @IBOutlet weak var liveUsersView : UIView!
    @IBOutlet weak var priceMoneyView : UIView!
    @IBOutlet weak var eventImgView : UIImageView!
    @IBOutlet weak var titleView : UIView!
    @IBOutlet weak var quizIsStrtingInLbl : UILabel!
    @IBOutlet weak var adsView: UIView!
    @IBOutlet weak var rewardDescriptionLabel : UILabel!
    var contentPlayer: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var rewardDescription : String!
    
    static let kTestAppContentUrl_MP4 = "https://storage.googleapis.com/gvabox/media/samples/stock.mp4"
    open var timeToStart: String!
    open var price: String!
    open var quizTitleStr: String!
    var countdownTimer: Timer!
    var ANIMATION_DURATION = -1
    var ANIMATION_DURATION_FOR_30MIN = -1
    open var eventStrtedMessage: EventStartedMessage!
    var liveTime : Date!
    var currentTime : Date!
    var eventImgUrl : String!

    var gradientLayer1 : CAGradientLayer!
    var gradientLayer2 : CAGradientLayer!
    var gradientLayer3 : CAGradientLayer!
    var gradientLayer4 : CAGradientLayer!

    var contentPlayhead: IMAAVPlayerContentPlayhead?
    var adsLoader: IMAAdsLoader!
    var adsManager: IMAAdsManager!
    var checkForLiveEvent = false

    static var kTestAppAdTagUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        liveUsersView.layer.cornerRadius = liveUsersView.frame.height / 2
        liveUsersView.clipsToBounds = true
        priceMoneyView.layer.cornerRadius = 5
        priceMoneyView.clipsToBounds = true
        gradientLayer1 = CAGradientLayer()
        gradientLayer2 = CAGradientLayer()
        gradientLayer3 = CAGradientLayer()
        gradientLayer4 = CAGradientLayer()
        
        if eventStrtedMessage != nil {
            if (ANIMATION_DURATION_FOR_30MIN >= 0){
                self.startTimer()
            }
            else{
                self.setTimer()
            }
            let userDefaults = UserDefaults.standard
            userDefaults.set(eventStrtedMessage.eventName, forKey: "eventName")
            userDefaults.synchronize()
            self.quizIsStrtingInLbl.text = "Stay tuned.\nQuiz is starting in"
            self.quizIsStrtingInLbl.textAlignment = .center
        }
        else if (ANIMATION_DURATION_FOR_30MIN >= 0){
            self.quizIsStrtingInLbl.text = "Stay tuned.\nQuiz is starting in"
            self.quizIsStrtingInLbl.textAlignment = .center
            self.startTimer()
        }
        else {
            if self.timeToQuiz != nil {
                self.timeToQuiz.text = timeToStart
            }
        }
        if self.price != nil {
            self.priceLabel.text = price
        }
        if self.quizTitleStr != nil {
            self.quizTitle.text = quizTitleStr
        }
        if let eventImgUrl = eventImgUrl {
            let url = URL(string: eventImgUrl)

            self.eventImgView.backgroundColor = Colors().white
            self.eventImgView.applyGradient(colours: [Colors().eventImgGradient1, Colors().eventImgGradient2], gradientOrientation: .vertical, gradientLayer: gradientLayer1)
            self.titleView.applyGradient(colours: [Colors().shareTitleGradient1,Colors().shareTitleGradient2], gradientOrientation: .vertical, gradientLayer: gradientLayer2)
            
            // for image or gif for event image view
            if(!eventImgUrl.contains("gif")){
                self.eventImgView.sd_setImage(with: url, completed: { (img, err, cache, url1) in
                   // self.eventImgView.removeGradient(gradientLayer: gradientLayer1)
                   // self.titleView.removeGradient(gradientLayer: gradientLayer2)
                    self.view.backgroundColor = Colors().black
                })
            }
            else{
                
                if let cachedGif = gifCache.object(forKey: "cachedGif" as AnyObject) {
                    self.eventImgView.image = UIImage.sd_image(withGIFData: (cachedGif as! Data))
                }
                else {
                    if let data = try? Data(contentsOf: url!) {
                        self.eventImgView.image = UIImage.sd_image(withGIFData: data)
                        gifCache.setObject(data as AnyObject, forKey: "cachedGif" as AnyObject)
                        self.view.backgroundColor = Colors().black
                    }
                }
            }
        }
        else{
            self.eventImgView.isHidden = true
            self.view.backgroundColor = Colors().white
            self.view.applyGradient(colours: [Colors().eventImgGradient1, Colors().eventImgGradient2], gradientOrientation: .vertical, gradientLayer: gradientLayer3)
            self.titleView.applyGradient(colours: [Colors().shareTitleGradient1,Colors().shareTitleGradient2], gradientOrientation: .vertical, gradientLayer: gradientLayer4)
        }
        if ShareController.kTestAppAdTagUrl != "" {
            setUpContentPlayer()
            setUpAdsLoader()
            requestAds()
        }
        if let rewardDescription = rewardDescription {
            self.rewardDescriptionLabel.text = rewardDescription
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.gradientLayer1.frame = self.eventImgView.bounds
        self.gradientLayer2.frame = self.titleView.bounds
        self.gradientLayer3.frame = self.view.bounds
        self.gradientLayer4.frame = self.titleView.bounds
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(willEnterForeground),
                                               name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    
    @objc func willEnterForeground() {
        if countdownTimer != nil {
            countdownTimer.invalidate()
            countdownTimer = nil
        }
        self.setTimer()
        
        if(adsManager != nil){
            adsManager.resume()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.contentPlayer?.pause()
    }
    @IBAction func backShare(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.adsView = nil
        self.contentPlayer?.pause()
    }
    
    // 30 Seconds Countdown
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        
        if ANIMATION_DURATION >= 0 {
            self.timeToQuiz.text = "\(timeFormatted(ANIMATION_DURATION))"
            ANIMATION_DURATION -= 1
        }
        else if ANIMATION_DURATION_FOR_30MIN >= 0 {
            let t1 = String(format: "%02d", (ANIMATION_DURATION_FOR_30MIN % 3600) / 60)
            let t2 = String(format: "%02d", (ANIMATION_DURATION_FOR_30MIN % 3600) % 60)
            self.timeToQuiz.text = t1 + ":" + t2
            self.ANIMATION_DURATION_FOR_30MIN -= 1
        }
        else {
            self.endTimer()
        }
    }
    private lazy var showVideoView: Void = {
        // Do something once
        self.showVideoViewController()
    }()
    func endTimer() {
        if countdownTimer != nil {
            countdownTimer.invalidate()
        }

        // Store Event Id...
        if let eventStrtedMessage = eventStrtedMessage {
            if(checkForLiveEvent){
                LGEngageManagerImp.shared.connectToEvent(user_id: UserDefaults.standard.string(forKey: "userId") ?? "", event_id: eventStrtedMessage.eventId)
            }
            UserDefaults.standard.set(eventStrtedMessage.eventId, forKey: "eventId")
            _ = showVideoView
        }
        else{
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d", seconds)
    }
    
    @objc func showVideoViewController() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VideoViewController") as! VideoViewController
        vc.eventStrtedMessage = self.eventStrtedMessage
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func setTimer() {
        let currentTime = Date()
        if let msg = eventStrtedMessage {
            if let liveTime = msg.liveTime {
                let timeT = LGEngageMessageUtils.isWithInEventStartTimeThreshold(q_time: liveTime.toDate()!, c_time: QuizUtilities.shared.toLocalTime(date: currentTime))
                self.ANIMATION_DURATION = Int(timeT)
                self.startTimer()
            }
        }
    }
    
    //MARK: Helper methods for IMAAds
    
    func setUpContentPlayer() {
        // Load AVPlayer with path to our content.
        guard let contentURL = URL(string: ShareController.kTestAppAdTagUrl) else {
            print("ERROR: please use a valid URL for the content URL")
            return
        }
        contentPlayer = AVPlayer(url: contentURL)
        
        // Create a player layer for the player.
        playerLayer = AVPlayerLayer(player: contentPlayer)
        
        // Size, position, and display the AVPlayer.
        playerLayer?.frame = adsView.layer.bounds
        adsView.layer.addSublayer(playerLayer!)
        
        // Set up our content playhead and contentComplete callback.
        contentPlayhead = IMAAVPlayerContentPlayhead(avPlayer: contentPlayer)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.contentDidFinishPlaying(_:)),
            name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
            object: contentPlayer?.currentItem);
    }
    @objc func contentDidFinishPlaying(_ notification: Notification) {
        // Make sure we don't call contentComplete as a result of an ad completing.
        if (notification.object as! AVPlayerItem) == contentPlayer?.currentItem {
            adsLoader.contentComplete()
        }
    }
    
    func setUpAdsLoader() {
        adsLoader = IMAAdsLoader(settings: nil)
        adsLoader.delegate = self
    }
    
    func requestAds() {
        // Create ad display container for ad rendering.
        let adDisplayContainer = IMAAdDisplayContainer(adContainer: adsView, companionSlots: nil)
        // Create an ad request with our ad tag, display container, and optional user context.
        let request = IMAAdsRequest(
            adTagUrl: ShareController.kTestAppAdTagUrl,
            adDisplayContainer: adDisplayContainer,
            contentPlayhead: contentPlayhead,
            userContext: nil)
        
        adsLoader.requestAds(with: request)
    }
    
    // MARK: - IMAAdsLoaderDelegate
    
    func adsLoader(_ loader: IMAAdsLoader!, adsLoadedWith adsLoadedData: IMAAdsLoadedData!) {
        // Grab the instance of the IMAAdsManager and set ourselves as the delegate.
        adsManager = adsLoadedData.adsManager
        adsManager.delegate = self
        
        // Create ads rendering settings and tell the SDK to use the in-app browser.
        let adsRenderingSettings = IMAAdsRenderingSettings()
        adsRenderingSettings.webOpenerPresentingController = self
        
        // Initialize the ads manager.
        adsManager.initialize(with: adsRenderingSettings)
    }
    
    func adsLoader(_ loader: IMAAdsLoader!, failedWith adErrorData: IMAAdLoadingErrorData!) {
        print("Error loading ads: \(adErrorData.adError.message)")
        contentPlayer?.play()
    }
    
    // MARK: - IMAAdsManagerDelegate
    
    func adsManager(_ adsManager: IMAAdsManager!, didReceive event: IMAAdEvent!) {
        if event.type == IMAAdEventType.LOADED {
            // When the SDK notifies us that ads have been loaded, play them.
            adsManager.start()
        }
        if event.type == IMAAdEventType.PAUSE {
            adsManager.pause()
        }
        if event.type == IMAAdEventType.RESUME {
           adsManager.resume()
        }
        /*case IMAAdEventType.PAUSE:
        setPlayButtonType(PlayButtonType.playButton)
        break
        case IMAAdEventType.RESUME:
        setPlayButtonType(PlayButtonType.pauseButton)
        break*/
    }
    
    func adsManager(_ adsManager: IMAAdsManager!, didReceive error: IMAAdError!) {
        // Something went wrong with the ads manager after ads were loaded. Log the error and play the
        // content.
        print("AdsManager error: \(error.message)")
        contentPlayer?.play()
    }
    
    func adsManagerDidRequestContentPause(_ adsManager: IMAAdsManager!) {
        // The SDK is going to play ads, so pause the content.
        contentPlayer?.pause()
    }
    
    func adsManagerDidRequestContentResume(_ adsManager: IMAAdsManager!) {
        // The SDK is done playing ads (at least for now), so resume the content.
        contentPlayer?.play()
        self.adsView.isHidden = true
    }
}
