//
//  LeaderboardTableViewCell.swift
//  LogixEngage
//
//  Created by Logituit on 02/07/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit

class LeaderboardTableViewCell : UITableViewCell{
    
    @IBOutlet weak var sequenceLabel      : UILabel!
    @IBOutlet weak var personName         : UILabel!
    @IBOutlet weak var coinsPrizeLabel    : UILabel!
    @IBOutlet weak var coinsPrizeView     : UIView!
    @IBOutlet weak var moneyPrizeLabel    : UILabel!
    @IBOutlet weak var moneyPrizeView     : UIView!
    @IBOutlet weak var thisIsYou          : UILabel!
    
    var gradientLayer                     : CAGradientLayer!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    override func awakeFromNib() {
        self.moneyPrizeView.backgroundColor = Colors().backgroundGrey
        self.coinsPrizeView.asCircle()
        self.moneyPrizeView.asCircle()
        self.selectionStyle = .none
        //self.coinsPrizeView.backgroundColor = UIColor.init(white: 1, alpha: 0.1)
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
