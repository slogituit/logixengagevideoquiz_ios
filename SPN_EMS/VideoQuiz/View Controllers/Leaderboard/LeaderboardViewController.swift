//
//  LeaderboardViewController.swift
//  LogixEngage
//
//  Created by Logituit on 02/07/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import LogixEngageFramework
import Toast_Swift

class LeaderboardViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var rankOneImageView     : UIImageView!
    @IBOutlet weak var rankTwoImageView     : UIImageView!
    @IBOutlet weak var rankThreeImageView   : UIImageView!
    @IBOutlet weak var rankTwoNameLabel     : UILabel!
    @IBOutlet weak var rankOneNameLabel     : UILabel!
    @IBOutlet weak var rankThreeNameLabel   : UILabel!
    @IBOutlet weak var firstRankLabel       : UILabel!
    @IBOutlet weak var secondRankLabel      : UILabel!
    @IBOutlet weak var thirdRankLabel       : UILabel!
    @IBOutlet weak var rankHoldersView      : UIView!
    @IBOutlet weak var firstRankView        : UIView!
    @IBOutlet weak var secondRankView       : UIView!
    @IBOutlet weak var thirdRankView        : UIView!
    @IBOutlet weak var underlineView        : UIView!
    @IBOutlet weak var navAndButtonsView    : UIView!
    @IBOutlet weak var thisWeekBtn          : UIButton!
    @IBOutlet weak var allTimeBtn           : UIButton!
    @IBOutlet weak var rankOneCoinsLabel    : UILabel!
    @IBOutlet weak var rankTwoCoinsLabel    : UILabel!
    @IBOutlet weak var rankThreeCoinsLabel  : UILabel!
    @IBOutlet weak var leaderBoardTableView : UITableView!
    @IBOutlet weak var noDataLabel          : UILabel!
    
    var currentUserId = UserDefaults.getVideoQuizDetails()?.id
    
    let myActivityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
    
    let engageManager       = LGEngageManagerImp.shared
    var weeklyBoard         = [WeeklyBoard]()
    var overallBoard        = [OverallBoard]()
    var ownRank             : OwnRank!
    var _id : String!
    var __v : Int!
    var tabIndex            = 0
    var isRefreshed         = false
    var rankNameLabelArr    = [UILabel]()
    var rankCoinsLableArr   = [UILabel]()
    var rankImageViewArr    = [UIImageView]()
    var timer : Timer!
    var weeklyRankersCount        = 3
    var overallRankersCount       = 3
    var rankLabelArray       = [UILabel]()
    var rankViewArray        = [UIView]()
    var gradientLayer1       : CAGradientLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstRankView.asCircle()
        secondRankView.asCircle()
        thirdRankView.asCircle()
        rankOneImageView.asCircle()
        rankTwoImageView.asCircle()
        rankThreeImageView.asCircle()
        firstRankView.asCircle()
        secondRankView.asCircle()
        thirdRankView.asCircle()
        firstRankLabel.asCircle()
        secondRankLabel.asCircle()
        thirdRankLabel.asCircle()
        
        rankImageViewArr    = [rankOneImageView, rankTwoImageView, rankThreeImageView]
        rankNameLabelArr    = [rankOneNameLabel,rankTwoNameLabel,rankThreeNameLabel]
        rankCoinsLableArr   = [rankOneCoinsLabel,rankTwoCoinsLabel,rankThreeCoinsLabel]
        
        self.rankLabelArray = [firstRankLabel, secondRankLabel, thirdRankLabel]
        self.rankViewArray = [firstRankView, secondRankView, thirdRankView]

        
        myActivityIndicator.center  = view.center
        myActivityIndicator.color   = Colors().orangeText
        view.addSubview(myActivityIndicator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.gradientLayer1 = CAGradientLayer()
        
        self.underlineView?.applyGradient(colours: [Colors().gradientOrange3, Colors().gradientOrange4], gradientOrientation: .horizontal, gradientLayer: self.gradientLayer1)
        self.firstRankLabel.backgroundColor     = Colors().backgroundOrange
        self.secondRankLabel.backgroundColor    = Colors().backgroundYellow
        self.thirdRankLabel.backgroundColor     = Colors().backgroundGreen
        self.noDataLabel.textColor              = Colors().white
        self.rankHoldersView.isHidden           = true
        self.noDataLabel.isHidden               = true
        self.getLeaderBoards()
        leaderBoardTableView.tableFooterView    = UIView()
        leaderBoardTableView.rowHeight          = 60
        self.thisWeekBtn.titleLabel?.font = UIFont.textStyle2
        self.allTimeBtn.titleLabel?.font = UIFont.textStyle5
        self.firstRankView.isHidden = true
        self.firstRankLabel.isHidden = true
        self.secondRankView.isHidden = true
        self.secondRankLabel.isHidden = true
        self.thirdRankView.isHidden = true
        self.thirdRankLabel.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.gradientLayer1.frame = self.underlineView.bounds
        self.leaderBoardTableView.reloadData()
    }
    
    func getLeaderBoards() {
        myActivityIndicator.startAnimating()
        engageManager.getLeaderBoard(delegate: self)
    }
    
    func loadLeaderBoardsRankersData() {
        self.rankOneImageView.asCircle()
        self.rankTwoImageView.asCircle()
        self.rankThreeImageView.asCircle()
        
        let tempUrl = StringConstants().defaultProfilePicUrl
        
        if(tabIndex == 0){
            
            if weeklyBoard.count == 0 {
                //show no data label and hide all others
                self.rankHoldersView.isHidden      = true
                self.noDataLabel.isHidden          = false
                self.leaderBoardTableView.isHidden = true
                return
            }
            
            //for first three Users in weekly leaderboard
            for i in 0 ..< self.weeklyRankersCount {
                rankNameLabelArr[i].text = weeklyBoard[i].userId.name
                let coins = Int(weeklyBoard[i].livCoins)
                let isCoins =  coins >= 1000
                if(isCoins){
                    let formattedCoinsValue = coins.getFormmatedCoins()
                    rankCoinsLableArr[i].text = formattedCoinsValue + " Pts"
                }
                else{
                    rankCoinsLableArr[i].text = coins.description + " Pts"
                }
                self.rankLabelArray[i].isHidden = false
                self.rankViewArray[i].isHidden = false
            }
            
            self.leaderBoardTableView.isHidden = false
            self.rankHoldersView.isHidden      = false
            self.noDataLabel.isHidden          = true
            
            if isRefreshed {
                self.leaderBoardTableView.reloadData()
                DispatchQueue.main.async {
                    for i in 0 ..< self.weeklyRankersCount {
                        if let url = URL(string: (self.weeklyBoard[i].userId.profilePic) ?? tempUrl){
                            self.rankImageViewArr[i].sd_setImage(with: url)
                            self.rankImageViewArr[i].backgroundColor = UIColor.gray
                        }
                        else {
                            let url = URL(string: tempUrl)
                            self.rankImageViewArr[i].sd_setImage(with: url)
                            self.rankImageViewArr[i].backgroundColor   = UIColor.gray
                        }
                    }
                }
            }
        }
        else{
            if overallBoard.count == 0 {
                //show no data label and hide all others
                self.rankHoldersView.isHidden      = true
                self.noDataLabel.isHidden          = false
                self.leaderBoardTableView.isHidden = true
                return
            }
            //for first three Users in all time leaderboard
            for i in 0 ..< self.overallRankersCount {
                rankNameLabelArr[i].text = overallBoard[i].userId.name
                let coins = Int(overallBoard[i].livCoins)
                let isCoins =  coins >= 1000
                if(isCoins){
                    let formattedCoinsValue = coins.getFormmatedCoins()
                    rankCoinsLableArr[i].text = formattedCoinsValue + " Pts"
                }
                else{
                    rankCoinsLableArr[i].text = coins.description + " Pts"
                }
                self.rankLabelArray[i].isHidden = false
                self.rankViewArray[i].isHidden = false
            }
            
            self.leaderBoardTableView.isHidden = false
            self.rankHoldersView.isHidden      = false
            self.noDataLabel.isHidden          = true
            
            self.leaderBoardTableView.reloadData()
            
            if isRefreshed {
                DispatchQueue.main.async {
                    for i in 0 ..< self.overallRankersCount {
                        if let url = URL(string: (self.overallBoard[i].userId.profilePic) ?? tempUrl) {
                            self.rankImageViewArr[i].sd_setImage(with: url)
                            self.rankImageViewArr[i].backgroundColor = UIColor.gray
                        }
                        else {
                            let url = URL(string: tempUrl)
                            self.rankImageViewArr[i].sd_setImage(with: url)
                            self.rankImageViewArr[i].backgroundColor   = UIColor.gray
                        }
                    }
                }
            }
            self.isRefreshed = false
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tabIndex == 0){
            if !self.ifMyRankIsInTheList() && self.ownRank != nil  {
                return weeklyBoard.count - 3
            } else {
                return weeklyBoard.count - 3
            }
        }
        else {
            if !self.ifMyRankIsInTheList() && self.ownRank != nil  {
                return overallBoard.count - 3
            } else {
                return overallBoard.count - 3
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tabIndex == 0){
            if !self.ifMyRankIsInTheList() && indexPath.row == 0 && self.ownRank != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! LeaderboardTableViewCell
                
                cell.personName.text = self.ownRank.userId.name
                cell.sequenceLabel.text = "\(self.ownRank.rankWeek ?? 0)"
                
                let coins = Int(self.ownRank.livCoinsWeek)
                let isCoins =  coins >= 1000
                if(isCoins){
                    let formattedCoinsValue = coins.getFormmatedCoins()
                    cell.coinsPrizeLabel.text = formattedCoinsValue + " Pts"
                }
                else{
                    cell.coinsPrizeLabel.text = coins.description + " Pts"
                }
                
                cell.thisIsYou.isHidden =  false
                cell.contentView.applyGradient(withColors: [Colors().gradientOrange3, Colors().gradientOrange4], gradient: cell.gradientLayer)
                let rgbValue = CGFloat(66/255)
                cell.coinsPrizeView.backgroundColor = UIColor.init(red: rgbValue, green: rgbValue, blue: rgbValue, alpha: 0.42)
                
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! LeaderboardTableViewCell
            
            let idx = indexPath.row + 3
            let weekly = self.weeklyBoard[idx]
            
            cell.personName.text = weekly.userId.name
            cell.sequenceLabel.text = "\(weekly.rankWeek ?? 0)"
            
            let coins = Int(weekly.livCoinsWeek)
            let isCoins =  coins >= 1000
            if(isCoins){
                let formattedCoinsValue = coins.getFormmatedCoins()
                cell.coinsPrizeLabel.text = formattedCoinsValue + " Pts"
            }
            else{
                cell.coinsPrizeLabel.text = coins.description + " Pts"
            }
            
            if (weekly.userId._id == currentUserId) {
                cell.contentView.applyGradient(withColors: [Colors().gradientOrange3, Colors().gradientOrange4], gradient: cell.gradientLayer)
                let rgbValue = CGFloat(66/255)
                cell.coinsPrizeView.backgroundColor = UIColor.init(red: rgbValue, green: rgbValue, blue: rgbValue, alpha: 0.42)
                cell.thisIsYou.isHidden = false
            }
            else{
                cell.thisIsYou.isHidden = true
                cell.contentView.applyGradient(withColors: [.black, .black], gradient: cell.gradientLayer)
                cell.coinsPrizeView.backgroundColor = UIColor.init(white: 1, alpha: 0.1)
            }
            return cell
        }
        else {
            if !self.ifMyRankIsInTheList() && indexPath.row == 0 && self.ownRank != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! LeaderboardTableViewCell
                
                cell.personName.text = self.ownRank.userId.name
                cell.sequenceLabel.text = "\(self.ownRank.rank ?? 0)"
                
                let coins = Int(self.ownRank.livCoins)
                let isCoins =  coins >= 1000
                if(isCoins){
                    let formattedCoinsValue = coins.getFormmatedCoins()
                    cell.coinsPrizeLabel.text = formattedCoinsValue + " Pts"
                }
                else{
                    cell.coinsPrizeLabel.text = coins.description + " Pts"
                }
        
                cell.thisIsYou.isHidden =  false
                cell.contentView.applyGradient(withColors: [Colors().gradientOrange3, Colors().gradientOrange4], gradient: cell.gradientLayer)
                let rgbValue = CGFloat(66/255)
                cell.coinsPrizeView.backgroundColor = UIColor.init(red: rgbValue, green: rgbValue, blue: rgbValue, alpha: 0.42)
                
                return cell
            }
            
            let cell : LeaderboardTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! LeaderboardTableViewCell
            
            if overallBoard.count > indexPath.row + 3 {
                if let userName = overallBoard[indexPath.row + 3].userId.name {
                    cell.personName.text = userName
                }
            }
            let myUserId = UserDefaults.getVideoQuizDetails()?.id
            
            cell.thisIsYou.isHidden = overallBoard[indexPath.row + 3].userId._id == currentUserId ? false : true
            
            if(overallBoard[indexPath.row + 3].userId._id == myUserId){
                cell.contentView.applyGradient(withColors: [Colors().gradientOrange3, Colors().gradientOrange4], gradient: cell.gradientLayer)
                let rgbValue = CGFloat(66/255)
                cell.coinsPrizeView.backgroundColor = UIColor.init(red: rgbValue, green: rgbValue, blue: rgbValue, alpha: 0.42)
            }
            else{
                cell.contentView.applyGradient(withColors: [.black, .black], gradient: cell.gradientLayer)
                cell.coinsPrizeView.backgroundColor = UIColor.init(white: 1, alpha: 0.1)
            }
            let overall = self.overallBoard[indexPath.row + 3]
            let coins = Int(overall.livCoins)
            let isCoins =  coins >= 1000
            if(isCoins){
                let formattedCoinsValue = coins.getFormmatedCoins()
                cell.coinsPrizeLabel.text = formattedCoinsValue + " Pts"
            }
            else{
                cell.coinsPrizeLabel.text = coins.description + " Pts"
            }
            cell.sequenceLabel.text = overallBoard[indexPath.row + 3].rank.description
            return cell
        }
    }
    
    @IBAction func thisWeekClick(_ sender: UIButton) {
        self.tabIndex = 0
        sender.titleLabel?.font = UIFont.textStyle2
        allTimeBtn.titleLabel?.font = UIFont.textStyle5
        underlineView.frame.origin = CGPoint(x: 0, y: underlineView.frame.origin.y)
        self.isRefreshed = true
        self.loadLeaderBoardsRankersData()
        
        let userId = UserDefaults.getVideoQuizDetails()?.id
        let cpCustomerId =  UserDefaults.getVideoQuizDetails()?.cpCustomerID
        let appVersion =  UserDefaults.getVideoQuizDetails()?.appVersion
        let advId =  UserDefaults.getVideoQuizDetails()?.advId
        let lat = UserDefaults.getVideoQuizDetails()?.latitude
        let long = UserDefaults.getVideoQuizDetails()?.longitude
        GoogleAnalyticsHelper.shared.leaderboardView(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, tabName: "This Week", scrolls: "0")
        SegmentAnalytics.shared.leaderboardView(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, tabName: "This Week", scrolls: "0", latitude: lat ?? "", longitude: long ?? "")
    }
    
    @IBAction func allTimeClick(_ sender: UIButton) {
        self.tabIndex = 1
        sender.titleLabel?.font = UIFont.textStyle2
        thisWeekBtn.titleLabel?.font = UIFont.textStyle5
        underlineView.frame.origin = CGPoint(x: sender.frame.origin.x, y: underlineView.frame.origin.y)
        self.isRefreshed = true
        self.loadLeaderBoardsRankersData()
        
        let userId = UserDefaults.getVideoQuizDetails()?.id
        let cpCustomerId =  UserDefaults.getVideoQuizDetails()?.cpCustomerID
        let appVersion =  UserDefaults.getVideoQuizDetails()?.appVersion
        let advId =  UserDefaults.getVideoQuizDetails()?.advId
        let lat = UserDefaults.getVideoQuizDetails()?.latitude
        let long = UserDefaults.getVideoQuizDetails()?.longitude
        GoogleAnalyticsHelper.shared.leaderboardView(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, tabName: "All Time", scrolls: "0")
        SegmentAnalytics.shared.leaderboardView(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, tabName: "All Time", scrolls: "0", latitude: lat ?? "", longitude: long ?? "")
    }
    
    @IBAction func backQuiz(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /// check if user rank is in top 8 or not
    func ifMyRankIsInTheList() -> Bool {
        var cnt = 0
        var isPresent = false
        
        if self.tabIndex == 0 {
            for user in weeklyBoard {
                let userId = user.userId._id
                if cnt < 8 {
                    if userId == currentUserId {
                        //return true
                        isPresent = true
                        break
                    }
                } else {
                    if let rank = ownRank?.rankWeek {
                        if userId == currentUserId {
                            weeklyBoard.remove(at: cnt)
                            break
                        }
                    }
                }
                cnt += 1
            }
            
            if !isPresent {
                if self.ownRank != nil {
                    if let newObj : WeeklyBoard = self.createDummyObjectForSelf() {
                        weeklyBoard.insert(newObj, at: 3)
                    }
                }
            }
        } else {
            cnt = 0
            for user in overallBoard {
                let userId = user.userId._id
                if cnt < 8 {
                    if userId == currentUserId {
                        isPresent = true
                    }
                } else {
                    if let rank = ownRank?.rank {
                        if userId == currentUserId {
                            overallBoard.remove(at: cnt)
                            break
                        }
                    }
                }
                cnt += 1
            }
            if !isPresent {
                if self.ownRank != nil {
                    if let newObj : OverallBoard = self.createDummyOverallObjectForSelf() {
                        overallBoard.insert(newObj, at: 3)
                    }
                }
            }
        }
        return isPresent
    }
    
    func createDummyObjectForSelf() -> WeeklyBoard {
        
//        if self.ownRank != nil {
            let obj = WeeklyBoard()
            obj.__v = self.ownRank.__v
            obj._id = self.ownRank._id
            obj.livCoinsWeek = self.ownRank.livCoinsWeek
            obj.livCoins = self.ownRank.livCoins
            obj.rank = self.ownRank.rank
            obj.rankWeek = self.ownRank.rankWeek
            obj.createdAt = self.ownRank.createdAt
            obj.updatedAt = self.ownRank.updatedAt
            obj.userId = self.ownRank.userId
            
            return obj
//        }
    }
    
    func createDummyOverallObjectForSelf() -> OverallBoard {
        
        //        if self.ownRank != nil {
        let obj = OverallBoard()
        obj.__v = self.ownRank.__v
        obj._id = self.ownRank._id
        obj.livCoinsWeek = self.ownRank.livCoinsWeek
        obj.livCoins = self.ownRank.livCoins
        obj.rank = self.ownRank.rank
        obj.rankWeek = self.ownRank.rankWeek
        obj.createdAt = self.ownRank.createdAt
        obj.updatedAt = self.ownRank.updatedAt
        obj.userId = self.ownRank.userId
        
        return obj
        //        }
    }
    
}


//#MARK: leader board response delegate methods
extension LeaderboardViewController : OnLeaderBoardResponseDelegate {
    
    func onResponse(response: LeaderBoardData) {
        //TBD : set datasource to table view
        myActivityIndicator.stopAnimating()
        self.isRefreshed = true
        self.__v = response.__v
        self._id = response._id
        self.overallBoard = response.overallBoard
        self.weeklyBoard  = response.weeklyBoard
        self.ownRank      = response.ownRank
        
        if self.weeklyBoard.count < 3 {
            self.weeklyRankersCount = weeklyBoard.count
        }
        if self.overallBoard.count < 3 {
            self.overallRankersCount = overallBoard.count
        }
        loadLeaderBoardsRankersData()
        self.tabIndex = 0
}
    
    func onFailure(error: String) {
        self.view.makeToast(error, duration: 3.0, position: .bottom)
        myActivityIndicator.stopAnimating()
        self.rankHoldersView.isHidden      = true
        self.noDataLabel.isHidden          = false
        self.leaderBoardTableView.isHidden = true
    }
}

extension UIView {
    func applyGradient(withColors colors: [UIColor], gradient: CAGradientLayer) {
        gradient.frame = self.bounds
        gradient.colors = colors.map { $0.cgColor }
        gradient.startPoint = .zero
        gradient.endPoint = CGPoint(x: 0, y: 1)
        
        if let topLayer = self.layer.sublayers?.first, topLayer is CAGradientLayer {
            topLayer.removeFromSuperlayer()
        }
        self.layer.insertSublayer(gradient, at: 0)
    }
}
