//
//  EngageEventHandler.swift
//  LogixEngage
//
//  Created by Apple on 11/07/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import Foundation
import UIKit
import os.log
import LogixEngageFramework
//import SonyAR
public protocol EngageEventHandlerProtocol {
    func pushVewController(vc: UIViewController)
    
    func pushVewControllerModally(vc: UIViewController, animated: Bool)
    
    func popViewController()
    
    func popToRootViewController()
}

@objc class EngageEventHandler: NSObject {
    
    private override init() {
        UserDefaults.standard.set(false, forKey: "DisconnectedDueToNetwork")
    } // Make init private for singleton class
    
    @objc static let shared     = EngageEventHandler()
    static var isBackBtnPressed = false
    var engageManager           = LGEngageManagerImp.shared
    let storyBoard              = UIStoryboard(name: StringConstants().videoQuizStoryboard, bundle:SPN_EMS_Bundle)
    var nextActivityMap         = [String: String]()
    var eventId                 : String!
    var state                   : LGEngageMessageUtils.EVENT_STATE = LGEngageMessageUtils.EVENT_STATE.EVENT_STATE_NOT_SET
    var delegate                : EngageEventHandlerProtocol!
    var questionViewController  : QuestionViewController!
    var timer                   : Timer!
    var wrongAnsPopupView       : WrongAnswerAndUseLife!
    
    
    public func startWebSocketSession(baseUrl: String, webSocketUrl: String, socketKey: String) {
        
        engageManager = LGEngageManagerImp.shared
        self.engageManager.setEngageMessageDelegate(delegate: self)
        _ = self.engageManager.startSession(baseUrl: baseUrl, webSocketUrl: webSocketUrl, socketKey: socketKey)
    }
}

//#MARK: LGEngageMessageDelegate implementation
extension EngageEventHandler: LGEngageMessageDelegate {
    
    func onSessionStarted() {
        os_log("Web Socket Session Started...", type:.debug)
        UIApplication.shared.isIdleTimerDisabled = true
        
        if self.eventId != nil {
            do {
                let jsonparams = [LGEngageMessageUtils.WS_MSG_FIELD_ACTION : LGEngageMessageUtils.WS_ACTION_TYPE_CONNECT_TO_EVENT,
                                  LGEngageMessageUtils.WS_MSG_FIELD_USER_ID: UserDefaults.getVideoQuizDetails()?.id,
                                  LGEngageMessageUtils.WS_MSG_FIELD_EVENT_ID: self.eventId]
                
                let request = try JSONSerialization.data(withJSONObject: jsonparams, options: [])
                // send ConnectToQuiz message to the server
                engageManager = LGEngageManagerImp.shared
                let msgString = String(data: request, encoding: String.Encoding.ascii)!
                engageManager.sendMessage(message: msgString)
                
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func onSessionStopped() {
        os_log("Web Socket Stopped...", type:.debug)
    }
    
    func onMessageReceived(jsonString: String) {
        os_log("onMessageReceived...", type:.debug)
    }
    
    func onEventStarted(msg: EventStartedMessage) {
        os_log("onEventStarted...", type:.debug)
        
        DispatchQueue.main.async {
            EngageEventHandler.isBackBtnPressed = false
            QuestionViewController.correct_ans_cnt = 0
            NotificationCenter.default.post(name: .updateEvents, object: nil)
        }
    }
    
    func onEventEnded(event_id: String, event_mode: Int) {
        os_log("Web Socket onEventEnded...", type:.debug)
        DispatchQueue.main.async {
            var topViewController: UIViewController! = UIViewController()
            
            if let topVC = UIApplication.getTopViewController() {
                topViewController = topVC
            }
            
            if event_mode == LGEngageMessageUtils.MODE_PEEK && !(topViewController.isKind(of: HomeQuizViewController.self)) {
                return
            }
            else if event_mode == LGEngageMessageUtils.MODE_PEEK && topViewController.isKind(of: HomeQuizViewController.self){
                NotificationCenter.default.post(name: .updateEvents, object: nil)
            }
            
            NotificationCenter.default.post(name: .stopPlayerSession, object: nil)
            self.cleanupOnEventEnd()
            if topViewController.isKind(of: QuestionViewController.self) {
                topViewController.dismiss(animated: false, completion: nil)
            }
            
            if !topViewController.isKind(of: HomeQuizViewController.self) {
                self.delegate.popToRootViewController()
            }
        }
    }
    
    func onError(errString: String) {
        os_log("Web Socket onError...", type:.debug)
        DispatchQueue.main.async {
            // TBD
            
        }
    }
    
    func onEventResult(msg: EventResultMessage) {
        os_log("Web Socket onEventResult...", type:.debug)
        self.handleEventResult(msg:msg)
    }
    
    func onAnswerResult(msg: AnswerResultMessage) {
        os_log("Web Socket onAnswerResult...", type:.debug)
        
        let currentActivity = UserDefaults.standard.integer(forKey: "currentActivity")
        QuizEngineRules.shared.setResult(currentQuestion: currentActivity, is_correct: msg.isResult())
        
        var newMap : [String: String] = [:]
        if(msg.getNextActivityId().isEmpty || msg.getNextActivityId() == ""){
            newMap[msg.getNextActivityIndex()] = msg.getNextActivityIndex()
        }
        else{
            newMap[msg.getNextActivityIndex()] = msg.getNextActivityId()
        }
        self.nextActivityMap = newMap
        QuestionViewController.nextActivityMap = self.nextActivityMap
        UserDefaults.standard.set(self.nextActivityMap, forKey: "nextActivityMap")
        
        if msg.isResult() {
            let totalCoins = UserDefaults.standard.integer(forKey: "coinsForCurrentQuiz") + QuestionViewController.coinsToEarn
            UserDefaults.standard.set(totalCoins, forKey: "coinsForCurrentQuiz")
            let coins = UserDefaults.standard.integer(forKey: "coins") + QuestionViewController.coinsToEarn
            UserDefaults.standard.set(coins, forKey: "coins")

            UserDefaults.standard.synchronize()
        }
        if !msg.getNextActivityId().isEmpty {
            QuestionViewController.nextActivityId = msg.nextActivityId
        }
        else{
            QuestionViewController.nextActivityId = msg.getNextActivityIndex()
        }
        
        let userId       = UserDefaults.getVideoQuizDetails()?.id
        let cpCustomerId =  UserDefaults.getVideoQuizDetails()?.cpCustomerID
        let appVersion   =  UserDefaults.getVideoQuizDetails()?.appVersion
        let advId        =  UserDefaults.getVideoQuizDetails()?.advId
        let lat          = UserDefaults.getVideoQuizDetails()?.latitude
        let long         = UserDefaults.getVideoQuizDetails()?.longitude
        let type         = UserDefaults.standard.string(forKey: "QuizType")
        var isSubmitted  = true
        if UserDefaults.standard.string(forKey: "selectedOption") == "-1" {
            isSubmitted  = false
            QuestionViewController.isLateJoined = true
        }
        let eventId = UserDefaults.standard.object(forKey: "LiveEventId") as? String ?? ""
        let eventName = UserDefaults.standard.object(forKey: "LiveEventName") as? String ?? ""
        let timeTakenToAns = UserDefaults.standard.value(forKey: "timeTakenToAnswer")
        var answersubmittedValue : String!
        var msgResultValue : String!
        
        if(isSubmitted == true){
            answersubmittedValue = "yes"
        }
        else{
            answersubmittedValue = "no"
        }
        
        if(msg.isResult() == true){
            msgResultValue = "yes"
        }else{
            msgResultValue = "no"
        }
        
        GoogleAnalyticsHelper.shared.quizAnswer(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: eventId, eventName: eventName, quizType: type ?? "", answerSubmitted: answersubmittedValue, answerCorrect: msgResultValue, timeToAnswer: timeTakenToAns as! String)
        
        SegmentAnalytics.shared.quizAnswer(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: eventId, eventName: eventName, quizType: type ?? "", answerSubmitted: answersubmittedValue, answerCorrect: msgResultValue, timeToAnswer: timeTakenToAns as! String, latitude: lat ?? "", longitude: long ?? "")
    }
    
    func onActivityPosted(msg: ActivityPostedMessage) {
        os_log("Web Socket onActivityPosted...", type:.debug)
        _ = UserDefaults.standard.integer(forKey: "life")
        
        if self.nextActivityMap.count != 0 && msg.activityDetails != nil {
            var key: String!
            for act in self.nextActivityMap {
                key = act.key
                break
            }
        }

        if EngageEventHandler.isBackBtnPressed && (Int(msg.getActivityDetails().getActivityIndex()) != 0) {
            QuestionViewController.nextActivityId = (Int(msg.getActivityDetails().getActivityIndex()) ?? 1000 + 1).description
        }

        let index = msg.getActivityDetails().getActivityIndex()
        if QuizEngineRules.shared.getIsAnswered(currentQuestion: Int(index)! + 1) {
            /* if previously answered , then don't show same question again
             +1 is because of key starts from 0 */
            return
        }
        if msg.activityDetails != nil {
            var cur_que_index = 0
            
            UserDefaults.standard.set(msg.getActivityDetails().getSubType(), forKey: "QuizType")
            var timeToAnswer = msg.getActivityDetails().getTimeToAnswer()
            if msg.getActivityDetails().length != nil {
             timeToAnswer += msg.getActivityDetails().getLength()
            }
            let postedAt = msg.getActivityDetails().getPostedAt()/1000
            let playerTime = VideoViewController.player_time/1000
            
            if playerTime >  postedAt && Int((playerTime - postedAt)) < timeToAnswer {
                
                self.questionViewController = self.storyBoard.instantiateViewController(withIdentifier: "QuestionViewController") as? QuestionViewController
                self.questionViewController.activityPostedMessage = msg.getActivityDetails()
                self.questionViewController.timeToAnswer = timeToAnswer - Int((playerTime - postedAt))
                self.questionViewController.activityResultMessage = nil
                self.questionViewController.currentActivity = Int(msg.getActivityDetails().getActivityIndex())! + 1
                cur_que_index = Int(msg.getActivityDetails().getActivityIndex())! + 1
                QuestionViewController.coinsToEarn = msg.getActivityDetails().getCoins()
                
                UserDefaults.standard.set(cur_que_index, forKey: "currentActivity")
                UserDefaults.standard.set(self.nextActivityMap, forKey: "nextActivityMap")
                
                if let topVC = UIApplication.getTopViewController() {
                    if(!topVC.isKind(of: QuestionViewController.self)){
                        self.delegate.pushVewControllerModally(vc: self.questionViewController, animated: false)
                    }
                }
            } else {
                self.questionViewController = self.storyBoard.instantiateViewController(withIdentifier: "QuestionViewController") as? QuestionViewController
                self.questionViewController.activityPostedMessage = msg.getActivityDetails()
                self.questionViewController.activityResultMessage = nil
                self.questionViewController.currentActivity = Int(msg.getActivityDetails().getActivityIndex())! + 1
                cur_que_index = Int(msg.getActivityDetails().getActivityIndex())! + 1
                QuestionViewController.coinsToEarn = msg.getActivityDetails().getCoins()
                self.questionViewController.timeToAnswer = timeToAnswer //- Int((playerTime - postedAt))
                UserDefaults.standard.set(cur_que_index, forKey: "currentActivity")
                UserDefaults.standard.set(self.nextActivityMap, forKey: "nextActivityMap")
                
                if let topVC = UIApplication.getTopViewController() {
                    if(!topVC.isKind(of: QuestionViewController.self)){
                        self.delegate.pushVewControllerModally(vc: self.questionViewController, animated: false)
                    }
                }
            }
        }
    }
    
    func onActivityResult(msg: ActivityResultMessage) {
        os_log("Web Socket onActivityResult...", type:.debug)
        
        DispatchQueue.main.async {
            if self.questionViewController != nil {
                self.questionViewController = nil
            }
            self.questionViewController = self.storyBoard.instantiateViewController(withIdentifier: "QuestionViewController") as? QuestionViewController
            
            self.questionViewController.currentActivity = Int(msg.activityIndex)! + 1
            UserDefaults.standard.set(Int(msg.activityIndex)! + 1, forKey: "currentActivity")
            
            if !QuizEngineRules.shared.getIsAnswered(currentQuestion: Int(msg.activityIndex)! + 1) {
                QuestionViewController.isLateJoined = true //don't show lifeline view
                QuestionViewController.coinsToEarn = 0
                UserDefaults.standard.set(-1, forKey: "selectedOption")
                UserDefaults.standard.synchronize()
            }
            
            self.questionViewController.activityResultMessage = msg
            self.questionViewController.activityPostedMessage = nil
            self.delegate.pushVewControllerModally(vc: self.questionViewController, animated: false)
        }
    }
    
    public func onLifeUsed(nextActivityId : String) {
        os_log("Web Socket onLifeUsed...", type:.debug)
        var newMap = UserDefaults.standard.dictionary(forKey: "nextActivityMap") as! [String : String]
        newMap[(nextActivityMap.first?.key)!] = nextActivityId
        self.nextActivityMap = newMap
        QuestionViewController.nextActivityMap = self.nextActivityMap
        QuestionViewController.nextActivityId = nextActivityId
        UserDefaults.standard.set(self.nextActivityMap, forKey: "nextActivityMap")
    }
    
    func onEventStatus(msg: EventStatusMessage) {
        os_log("Web Socket onEventStatus...", type:.debug)
        
        DispatchQueue.main.async {
            
            if !msg.getNextActivityId().isEmpty {
                if UserDefaults.standard.bool(forKey: "DisconnectedDueToNetwork") == false {
                    // fresh joining

                    UserDefaults.standard.set(false, forKey: "lateJoined")
                    var newMap : [String: String] = [:]
                    newMap[msg.getNextActivityIndex()] = msg.getNextActivityId()
                    self.nextActivityMap = newMap
                    UserDefaults.standard.set(self.nextActivityMap, forKey: "nextActivityMap")
                    QuestionViewController.nextActivityMap = self.nextActivityMap
                    QuestionViewController.nextActivityId = msg.getNextActivityId()
                } else {
                    // Do nothing
                }
                
            } else if !msg.getNextActivityIndex().isEmpty && msg.getNextActivityId().isEmpty {
                if UserDefaults.standard.bool(forKey: "DisconnectedDueToNetwork") == false {
                    UserDefaults.standard.set(true, forKey: "lateJoined")
                }
                let nextActivityIndex = Int(msg.nextActivityIndex)! + 1
                if !QuizEngineRules.shared.getIsAnswered(currentQuestion: nextActivityIndex) {
                    QuestionViewController.coinsToEarn = 0
                    UserDefaults.standard.set(-1, forKey: "selectedOption")
                    UserDefaults.standard.synchronize()
                }
                var newMap : [String: String] = [:]
                newMap[msg.getNextActivityIndex()] = nextActivityIndex.description
                self.nextActivityMap = newMap
                UserDefaults.standard.set(self.nextActivityMap, forKey: "nextActivityMap")
                QuestionViewController.nextActivityMap = self.nextActivityMap
                QuestionViewController.nextActivityId = nextActivityIndex.description
                QuestionViewController.isLateJoined = true
                UserDefaults.standard.set(msg.getNextActivityIndex(), forKey: "LiveEventCurrentIndex")
            }
            if UserDefaults.standard.bool(forKey: "DisconnectedDueToNetwork") == true {
                QuestionViewController.isLateJoined = false
                UserDefaults.standard.set(false, forKey: "lateJoined")
                UserDefaults.standard.set(false, forKey: "DisconnectedDueToNetwork")
                return
            }

            UserDefaults.standard.set(0, forKey: "coinsForCurrentQuiz")
            UserDefaults.standard.synchronize()

            QuizEngineRules.shared.reset()
            QuizEngineRules.shared.setTotalQuestions(totalQuestions: Int(msg.getTotalActivities()))
            
            if UserDefaults.standard.value(forKey: "lateJoined") as? Bool ?? false {
                // show pop up for late joined
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { //2 sec delay
                    self.showPopUpForYouAreLate()
                }
                UserDefaults.standard.set(false, forKey: "lateJoined")
            }
        }
    }
    
    func onEventUsers(event_id: String, num_users: String) {
        os_log("Web Socket onEventUsers...", type:.debug)
        UserDefaults.standard.set(num_users, forKey: "users_connected")
        UserDefaults.standard.synchronize()
    }
    
    //error for multiple connection for same user
    func onEventError(error_code: Int, error_msg: String) {
        switch error_code {
        case 100 :
           UIApplication.getTopViewController()?.view.makeToast("You joined late in the game !!", duration: 3.0, position: .bottom)
            UserDefaults.standard.set(-1, forKey: "selectedOption")
            UserDefaults.standard.synchronize()
        case 112 :
            UIApplication.getTopViewController()?.view.makeToast(error_msg, duration: 3.0, position: .bottom)
            self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(dismissRepeatedUser), userInfo: nil, repeats: false)
        default:
            os_log("Event Not Live....", type:.debug)
        }
    }
    
    @objc func dismissRepeatedUser(){
        
        engageManager.webSocketClient.disconnect()
        NotificationCenter.default.post(name: .muteVideo, object: nil)
        UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: true, completion: {
            UIApplication.shared.isIdleTimerDisabled = false
        })
    }
    
    private func handleEventResult(msg: EventResultMessage) {
        
        var isWinner = false
        do {
            isWinner = try QuizEngineRules.shared.isWinner(currentQuestion: QuizEngineRules.shared.getTotalQuestions())
        } catch {
            isWinner = false
        }
        
        if(isWinner){
            let vc = self.storyBoard.instantiateViewController(withIdentifier: "WinnerCongratsViewController") as! WinnerCongratsViewController
            UserDefaults.standard.set(false, forKey: "isLastAnswerCorrect")
            
            if(msg.leaderboardSponsor != nil){
                vc.leaderboardSponsor = msg.leaderboardSponsor
            }
            if(!msg.winnersList.isEmpty){
                
                let winner = msg.winnersList.last
                if let winner = winner {
                    let wonAmount = Double(winner.wonAmount) ?? 0.0
                    vc.cashWon = Int(wonAmount) 
                }
                if let winnerCount = msg.winnerCount {
                    vc.winnerCount = winnerCount
                }
            }
            else{
                vc.cashWon = 0
            }
            self.delegate.pushVewController(vc: vc)
        }
        else{
            let vc = self.storyBoard.instantiateViewController(withIdentifier: "NonWinnersViewController")as! NonWinnersViewController
            if(msg.leaderboardSponsor != nil){
                vc.leaderboardSponsor = msg.leaderboardSponsor
            }
            UserDefaults.standard.set(false, forKey: "isLastAnswerCorrect")
            vc.winnersList = msg.winnersList
            if let winnerCount = msg.winnerCount {
                vc.winnerCount = winnerCount
            }
            self.delegate.pushVewController(vc: vc)
        }
    }
    
    func cleanupOnEventEnd() {
        UserDefaults.standard.set(nil, forKey: "eventId")
        UserDefaults.standard.set(nil, forKey: "currentEventName")
        UserDefaults.standard.set(nil, forKey: "currentEventImage")
        UserDefaults.standard.set(nil, forKey: "nextActivityMap")
        UserDefaults.standard.set(nil, forKey: "QuizType")
        UserDefaults.standard.set(nil, forKey: "selectedOption")
        UserDefaults.standard.set(nil, forKey: "LiveEventId")
        UserDefaults.standard.set(nil, forKey: "LiveEventName")
        UserDefaults.standard.set(nil, forKey: "timeTakenToAnswer")
        UserDefaults.standard.set(nil, forKey: "currentActivity")
        UserDefaults.standard.set(nil, forKey: "LiveEventCurrentIndex")
        UserDefaults.standard.set(nil, forKey: "coinsForCurrentQuiz")
        UserDefaults.standard.set(nil, forKey: "coins")
        UserDefaults.standard.set("0", forKey: "users_connected")
        UserDefaults.standard.set(nil, forKey: "isLastAnswerCorrect")
        UserDefaults.standard.set("", forKey: "quizEntryTimeStamp")
        UserDefaults.standard.set(false, forKey: "DisconnectedDueToNetwork")

        EngageEventHandler.isBackBtnPressed = false

        self.nextActivityMap = [:]
        self.questionViewController = nil
        
        //resetting QuestionViewControllre static variables
        QuestionViewController.optionsArray = []
        QuestionViewController.nextActivityMap = [:]
        QuestionViewController.nextActivityId = nil
        QuestionViewController.coinsToEarn = 0
        QuestionViewController.outOfTheGame = false
        QuestionViewController.isLateJoined = false
    }
}

extension EngageEventHandler {
    func showPopUpForYouAreLate() {
      
        if let topVC = UIApplication.getTopViewController()
        {
            
            if let useLifeLinePopView = SPN_EMS_Bundle.loadNibNamed("WrongAnswerAndUseLife", owner: self, options: nil)?.first as? WrongAnswerAndUseLife {
                self.wrongAnsPopupView = useLifeLinePopView
                topVC.view.addSubview(self.wrongAnsPopupView)
                self.wrongAnsPopupView.titleLabel.text = StringConstants().youAreLate
                self.wrongAnsPopupView.descriptionLabel.text = StringConstants().youAreLateDescription
                self.wrongAnsPopupView.doneBtn.setTitle(StringConstants().wrongAnsBtnTitle, for: .normal)
                self.wrongAnsPopupView.doneBtn.tag = 4
                self.wrongAnsPopupView.keyImageVIew.isHidden = true
                self.wrongAnsPopupView.doneBtn.addTarget(self, action: #selector(self.popUpBtnPressed(_:)), for: .touchUpInside)
                self.wrongAnsPopupView.frame = CGRect(x: 0, y: ((topVC.view.frame.height) - 180), width: (topVC.view.frame.width), height: 180)
                self.addTapGestureForPopUp(view: useLifeLinePopView)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                    self.removePopUpViewFromSuperview()
                })
            }
        }
    }
    
    @objc func popUpBtnPressed(_ sender: UIButton) {
        if sender.tag == 4 {
            self.removePopUpViewFromSuperview()
            let userId          = UserDefaults.getVideoQuizDetails()?.id
            let cpCustomerId    =  UserDefaults.getVideoQuizDetails()?.cpCustomerID
            let appVersion      =  UserDefaults.getVideoQuizDetails()?.appVersion
            let advId           =  UserDefaults.getVideoQuizDetails()?.advId
            let eventId         = UserDefaults.standard.object(forKey: "LiveEventId") as? String ?? ""
            let eventName       = UserDefaults.standard.object(forKey: "LiveEventName") as? String ?? ""
            let currentIndex    = UserDefaults.standard.value(forKey: "LiveEventCurrentIndex") as? String ?? "0"
            let lat = UserDefaults.getVideoQuizDetails()?.latitude
            let long = UserDefaults.getVideoQuizDetails()?.longitude
            
            GoogleAnalyticsHelper.shared.continuePlayingClick(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: eventId, eventName: eventName, questionNo: currentIndex)
            SegmentAnalytics.shared.continueButtonClick(userId: userId!, cpCustomerId: cpCustomerId!, appVersion: appVersion!, advId: advId!, eventId: eventId, eventName: eventName, questionNo: currentIndex, latitude: lat ?? "" , longitude: long ?? "")
        }
    }
    
    @objc func removePopUpViewFromSuperview() {
        if self.wrongAnsPopupView != nil {
            self.wrongAnsPopupView.removeFromSuperview()
            self.wrongAnsPopupView = nil
        }
    }
    
    func addTapGestureForPopUp(view : UIView){
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.removePopUpViewFromSuperview))
        view.addGestureRecognizer(gestureRecognizer)
    }
}

