//
//  NonWinnersTableCell.swift
//  LogixEngage
//
//  Created by Abbas on 01/09/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit

class NonWinnersTableCell: UITableViewCell {

    @IBOutlet weak var sequenceLabel : UILabel!
    @IBOutlet weak var personName : UILabel!
    @IBOutlet weak var prizeLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
