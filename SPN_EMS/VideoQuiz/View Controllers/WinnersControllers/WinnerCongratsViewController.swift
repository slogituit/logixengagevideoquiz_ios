//
//  WinnerCongratsViewController.swift
//  LogixEngage
//
//  Created by Apple on 26/08/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit
import LogixEngageFramework

class WinnerCongratsViewController: UIViewController {
    
    @IBOutlet weak var cashEarned: UILabel!
    @IBOutlet weak var coinsEarned: UILabel!
    @IBOutlet weak var lifelines: UILabel!
    @IBOutlet weak var lifeLineImg : UIImageView!
    @IBOutlet weak var redeemYourMoneyBtn: UIButton!
    @IBOutlet weak var howToRedeemBtn: UIButton!
    @IBOutlet weak var dividerView1: UIView!
    @IBOutlet weak var dividerView2: UIView!
    @IBOutlet weak var liveUsersView: UIView!
    @IBOutlet weak var liveUsersLabel: UILabel!
    @IBOutlet weak var liveUsersImg : UIImageView!
    @IBOutlet weak var quizName: UILabel!
    @IBOutlet weak var tableForLifeline : UITableView!
    @IBOutlet weak var youHaveWon : UILabel!
    @IBOutlet weak var poweredByImage: UIImageView!
    @IBOutlet weak var poweredByLbl : UILabel!
    @IBOutlet weak var poweredByView : UIView!
    @IBOutlet weak var sponsorViewHeight: NSLayoutConstraint!

    var urlString : String!
    var winnerCount = 0
    var cashWon : Int = 0
    var leaderboardSponsor : ActivitySponsor!
    var lifeLineInfo = ["If you play the quiz for 7 continuous days and attempt all the question (without dropping out at any question), you will get a lifeline for the next week.",
    "The lifeline is valid for one use only and can be used only once in a quiz till the 9th question.",
    "Each lifeline expires within a week."]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.liveUsersView.isHidden = false
        let users_connected = UserDefaults.standard.string(forKey: "users_connected") ?? "1"
        let formattedUsers = Int(users_connected)?.getFormmatedCoins()
        self.liveUsersLabel.text = formattedUsers!
        self.cashEarned.text = "₹" + cashWon.withCommas()
        let currentEventName = UserDefaults.standard.string(forKey: "currentEventName")
        self.quizName.text = currentEventName
        self.liveUsersView.asCircle()
        self.howToRedeemBtn.isHidden = true
        self.tableForLifeline.tableFooterView = UIView()
        self.youHaveWon.font = UIFont.textStyle4
        self.setUI()
        if leaderboardSponsor != nil {
            self.setSponsors()
        }
        else{
            self.sponsorViewHeight.constant = 0
        }

        self.poweredByView.isUserInteractionEnabled = true
        self.poweredByView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openUrl)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.redeemYourMoneyBtn.applyGradient(colours: [Colors().gradientOrange3, Colors().gradientOrange4], gradientOrientation: .horizontal)
        
        self.dividerView1.backgroundColor = Colors().dividerBackgroundGrey
        self.dividerView2.backgroundColor = Colors().dividerBackgroundGrey
        
        self.cashEarned.textColor = Colors().orangeText
        self.coinsEarned.textColor = Colors().orangeText
        self.lifelines.textColor = Colors().orangeText
        
        self.redeemYourMoneyBtn.layer.cornerRadius = 5.0
        self.redeemYourMoneyBtn.clipsToBounds = true
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    func setUI() {
       let lifeCount = UserDefaults.standard.integer(forKey: "life")
        
        if(lifeCount <= 0){
            self.lifeLineImg.isHidden = true
            self.tableForLifeline.isHidden = true
            self.lifelines.isHidden = true
            self.lifeLineImg.isHidden = true
        }
        else{
            self.lifelines.text = lifeCount.description
        }
        let totalCoins = UserDefaults.standard.integer(forKey: "coins")
        self.coinsEarned.text = totalCoins.withCommas()
    }
    
    func setSponsors() {
        if let leaderboardSponsor = self.leaderboardSponsor {
            if let descriptionKey = leaderboardSponsor.descriptionKey {
                let attributedString = NSMutableAttributedString(string: descriptionKey, attributes: [
                    .font: UIFont.systemFont(ofSize: 11.0, weight: .regular),
                    .kern: 0.0
                    ])
               // attributedString.addAttribute(.foregroundColor, value: UIColor.black, range: NSRange(location: StringConstants().PoweredBy.count + 1, length: name.count))
                self.poweredByLbl.attributedText = attributedString
                self.poweredByLbl.isHidden = false
            }
            if let url = leaderboardSponsor.url {
                self.urlString = url
            }
            if let url = URL(string: leaderboardSponsor.logoSmall){
               // self.poweredByImage.sd_setImage(with: url)
                if let url = URL(string: leaderboardSponsor.logoSmall){
                    //                self.poweredByImg.sd_setImage(with: url)
                    if let data = try? Data(contentsOf: url) {
                        let i = UIImage(data: data)
                        let img = poweredByImage.resizeImage(image: i!, targetSize: CGSize(width: 40, height: 40))
                        self.poweredByImage.image = img
                    }
                    self.poweredByImage.isHidden = false
                }
            }
        }
    }
}

extension WinnerCongratsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lifeLineInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "lifeLineCell", for: indexPath) as! LifeLineDescTableCell
        cell.lifeLineDesc.text = "●" + self.lifeLineInfo[indexPath.row].description
        return cell
    }
    
    @objc func openUrl(){
        if self.urlString != nil {
            self.urlString.openUrl()
        }
    }
}
