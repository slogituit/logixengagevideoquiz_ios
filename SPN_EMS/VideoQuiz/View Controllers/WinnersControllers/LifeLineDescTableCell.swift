//
//  LifeLineDescTableCell.swift
//  SPN_EMS
//
//  Created by Abbas on 09/10/19.
//  Copyright © 2019 Sourcebits. All rights reserved.
//

import UIKit

class LifeLineDescTableCell: UITableViewCell {

    @IBOutlet weak var lifeLineDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
