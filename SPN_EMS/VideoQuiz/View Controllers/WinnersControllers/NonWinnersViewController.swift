//
//  NonWinnersViewController.swift
//  LogixEngage
//
//  Created by Abbas on 27/08/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit
import LogixEngageFramework

class NonWinnersViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var liveUsers: UILabel!
    @IBOutlet weak var liveUsersView: UIView!
    @IBOutlet weak var liveUsersImg : UIImageView!
    @IBOutlet weak var quizName: UILabel!
    @IBOutlet weak var youHaveWonAmnt : UILabel!
    @IBOutlet weak var winnersOfTheDay : UILabel!
    @IBOutlet weak var leaderboardBtn : UIButton!
    @IBOutlet weak var noWinnersView: UIView!
    @IBOutlet weak var noWinnersLabel : UILabel!
    @IBOutlet weak var poweredByLbl : UILabel!
    @IBOutlet weak var poweredByImg : UIImageView!
    @IBOutlet weak var poweredByView : UIView!
    @IBOutlet weak var sponsorViewHeight: NSLayoutConstraint!

    var winnersList = [Winners]()
    var leaderboardSponsor : ActivitySponsor!
    var winnerCount = 0
    var urlString : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let c1 = UserDefaults.standard.integer(forKey: "coinsForCurrentQuiz")
  
        let str1 = NSMutableAttributedString(string: "You have won ")
        let str2 = NSMutableAttributedString(string: c1.description, attributes: [NSAttributedString.Key.foregroundColor : UIColor.orange])
        let str3 =  NSMutableAttributedString(string: " points.")
        str1.append(str2)
        str1.append(str3)
        
        self.youHaveWonAmnt.font = UIFont.textStyle4
        self.youHaveWonAmnt.attributedText = str1
        self.liveUsersView.isHidden = false
        self.winnersOfTheDay.font = UIFont.textStyle6
        self.leaderboardBtn.titleLabel?.font = UIFont.textStyle6
        let users_connected = UserDefaults.standard.string(forKey: "users_connected") ?? "1"
        let formattedUsers = Int(users_connected)?.getFormmatedCoins()
        self.liveUsers.text = formattedUsers!
        let currentEventName = UserDefaults.standard.string(forKey: "currentEventName")
        self.quizName.text = currentEventName
        self.liveUsersView.asCircle()
        tableView.tableFooterView = UIView()
        if winnersList.count == 0 {
            self.noWinnersLabel.textAlignment = .center
            self.noWinnersLabel.horizontallyCenterInSuperview()
            self.noWinnersLabel.backgroundColor = Colors().white
            self.noWinnersView.isHidden = false
            self.noWinnersLabel.isHidden = false
            self.tableView.isHidden = true
        } else {
            self.noWinnersView.isHidden = true
            self.noWinnersLabel.isHidden = true
            self.tableView.isHidden = false
        }
        if leaderboardSponsor != nil {
            self.setSponsors()
        }
        else{
            self.sponsorViewHeight.constant = 0
        }
        self.poweredByView.isUserInteractionEnabled = true
        self.poweredByView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openUrl)))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return winnersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NonWinnersTableCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NonWinnersTableCell
        cell.sequenceLabel.text = (indexPath.row + 1).description
        cell.personName.text = winnersList[indexPath.row].userId.name ?? "User"
        let prizeInDouble = Double(winnersList[indexPath.row].wonAmount) ?? 0.0
        cell.prizeLabel.text = "₹" + (Int(prizeInDouble).withCommas().description)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func viewLeaderboardPressed(_ sender : UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LeaderboardViewController") as! LeaderboardViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setSponsors() {
        if let leaderboardSponsor = self.leaderboardSponsor {
            if let descriptionKey = leaderboardSponsor.descriptionKey {
                let attributedString = NSMutableAttributedString(string: descriptionKey, attributes: [
                    .font: UIFont.systemFont(ofSize: 11.0, weight: .regular),
                    .kern: 0.0
                    ])
               
                self.poweredByLbl.attributedText = attributedString
                self.poweredByLbl.isHidden = false
            }
            if let url = leaderboardSponsor.url {
                self.urlString = url
            }
            if let url = URL(string: leaderboardSponsor.logoSmall){
//                self.poweredByImg.sd_setImage(with: url)
                if let data = try? Data(contentsOf: url) {
                    let i = UIImage(data: data)
                    let img = poweredByImg.resizeImage(image: i!, targetSize: CGSize(width: 40, height: 40))
                self.poweredByImg.image = img
                }
                self.poweredByImg.isHidden = false
            }
        }
    }
    @objc func openUrl(){
        if self.urlString != nil {
            self.urlString.openUrl()
        }
    }
}
extension UIImageView {
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio = targetSize.width / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width : size.width * heightRatio, height : size.height * heightRatio)
        } else {
            newSize = CGSize(width : size.width * widthRatio, height : size.height *  widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x : 0, y:  0,width : newSize.width, height : newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

