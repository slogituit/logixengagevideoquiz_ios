//
//  VideoViewController.swift
//  LogixEngage
//
//  Created by Abbas's Mac Mini on 14/05/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import UIKit
import AVFoundation
import LogixEngageFramework

class VideoViewController: UIViewController {
    
    @IBOutlet weak var videoView        : UIView! // Video View
    @IBOutlet weak var currentTimeLbl   : UILabel!
    @IBOutlet weak var endTimeLbl       : UILabel!
    @IBOutlet weak var progressView     : UIProgressView!
    @IBOutlet weak var backButton       : UIButton!
    
    static var player_time              : Double = 0
    private var playerItemContext                = 0
    var player                          : AVPlayer!
    open var eventStrtedMessage         : EventStartedMessage!
    var timer                           : Timer!
    var url                             : URL!
    var asset                           : AVAsset!
    var playerItem                      : AVPlayerItem!
    var playerLayer                     : AVPlayerLayer!
    var currentTimeString               : String!
    var endTimeString                   : String!
    var reachability                    : Reachability!
    var gradientLayer1                  : CAGradientLayer!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.gradientLayer1 = CAGradientLayer()
        self.view.applyGradient(colours: [Colors().eventImgGradient1, Colors().eventImgGradient2], gradientOrientation: .vertical, gradientLayer: gradientLayer1)
        
        self.setupPlayer()
        self.view.bringSubviewToFront(backButton)
        videoView.bringSubviewToFront(backButton)
        
        //TBD
        reachability = Reachability()!
        self.setObservers()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.gradientLayer1.frame = self.view.bounds
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if player != nil {
            self.player.volume = 1.0
        }
        self.view.bringSubviewToFront(currentTimeLbl)
        videoView.bringSubviewToFront(currentTimeLbl)
        self.currentTimeLbl.isHidden = true

    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if player != nil {
            self.player.volume = 0.5
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let topVC = UIApplication.getTopViewController() {
        
            if (topVC.isKind(of: HomeQuizViewController.self)) {
                //cleanup video player
                self.cleanupVideoPlayer()
            }
        }
    }
    
    func setObservers(){
        NotificationCenter.default.addObserver( self, selector: #selector(self.networkReachabilityChanged),
            name:  NSNotification.Name(rawValue: "rechabilityChangedNotification"), object:nil )
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.muteVideo),
        name: NSNotification.Name(rawValue: "muteVideo"), object: nil)
        
        NotificationCenter.default.addObserver( self, selector: #selector(self.unmuteVideo),
        name: NSNotification.Name(rawValue: "unmuteVideo"), object: nil)
        
        NotificationCenter.default.addObserver( self, selector: #selector(self.stopPlayerSession),
        name: NSNotification.Name(rawValue: "stopPlayerSession"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.videoWillEnterForeground),
        name: UIApplication.didBecomeActiveNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }

    @objc func videoWillEnterForeground() {
        //self.setObservers()
        
        if player != nil {
            player.pause()
            playerLayer.removeAllAnimations()
            playerLayer = nil
            self.player = nil
            self.playerItem?.removeObserver(self, forKeyPath: "status", context: &playerItemContext)
        }
        if(self.player == nil) {
            self.setupPlayer()
            if UserDefaults.standard.bool(forKey: "DisconnectedDueToNetwork") == false {
                
                let userId     = UserDefaults.getVideoQuizDetails()?.id
                if let eventID = UserDefaults.standard.value(forKey: "eventId") {
                    LGEngageManagerImp.shared.disconnectFromEvent(user_id: userId!, event_id: eventID as! String)
                    UserDefaults.standard.set(true, forKey: "DisconnectedDueToNetwork")
                    EngageEventHandler.isBackBtnPressed = true
                NotificationCenter.default.post(name: .appActiveStateNotification, object: self.reachability)
                    
                    LGEngageManagerImp.shared.connectToEvent(user_id: userId!, event_id: eventID as! String)
                }
            }
        }
        
        self.view.bringSubviewToFront(backButton)
        videoView.bringSubviewToFront(backButton)
        self.view.bringSubviewToFront(currentTimeLbl)
        videoView.bringSubviewToFront(currentTimeLbl)
        self.currentTimeLbl.isHidden = true
    }
    
    @objc func didEnterBackground() {
        if(playerLayer != nil) {
            player.pause()
            playerLayer.removeAllAnimations()
            playerLayer = nil
            self.player = nil
            self.playerItem?.removeObserver(self, forKeyPath: "status", context: &playerItemContext)
        }
        
        var topViewController: UIViewController! = UIViewController()
        
        if let topVC = UIApplication.getTopViewController() {
            topViewController = topVC
        }
        if topViewController.isKind(of: QuestionViewController.self) {
            topViewController.dismiss(animated: false, completion: nil)
        }
        else if !topViewController.isKind(of: VideoViewController.self) {
            self.navigationController?.popToViewController(self, animated: true)
        }
    }
    
    @objc func muteVideo(){
        if player != nil {
            self.player.isMuted = true
        }
    }
    
    @objc func unmuteVideo(){
        if player != nil {
            self.player.isMuted = false
        }
    }
    
    @objc func stopPlayerSession(){
        self.cleanupVideoPlayer()
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func networkReachabilityChanged( notification: NSNotification ){
        
        guard let reachability: Reachability = notification.object as? Reachability ?? self.reachability else { return }
        
        if reachability.connection == .none {
            //navigationController?.popToRootViewController(animated: true)
            if(playerLayer != nil) {
                player.pause()
                playerLayer = nil
                self.player = nil
                self.playerItem?.removeObserver(self, forKeyPath: "status", context: &playerItemContext)
            }
        }
        else {
            if reachability.connection == .wifi || reachability.connection == .cellular {
                
                //if(self.player == nil) {
                self.setupPlayer()
                //}
                self.view.bringSubviewToFront(backButton)
                videoView.bringSubviewToFront(backButton)
                self.view.bringSubviewToFront(currentTimeLbl)
                videoView.bringSubviewToFront(currentTimeLbl)
                self.currentTimeLbl.isHidden = true
            }
        }
    }
    
    func setupPlayer(){
        
        if eventStrtedMessage != nil {
            url = URL(string: eventStrtedMessage.videoUrl.trimmingCharacters(in: .whitespacesAndNewlines))
        } else {
            url = URL(string: "http://219.91.251.234:8080/hls/stream.m3u8")!
        }
        
        asset       = AVAsset(url: url)
        playerItem  = AVPlayerItem(asset: asset)
        self.player = AVPlayer(playerItem: playerItem)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        player.isMuted      = false
        playerLayer.frame   = self.view.frame
        videoView.layer.addSublayer(playerLayer)
        
        playerItem.addObserver(self, forKeyPath: "status", options: [], context: &playerItemContext)
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        // Only handle observations for the playerItemContext
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath,
                               of: object,
                               change: change,
                               context: context)
            return
        }
        
        if (keyPath == "status") {
            if player != nil {
                if (player.status == .readyToPlay) {
                    player.play()
                    startTimer()
                } else if (player.status == .failed) {
                    // something went wrong. player.error should contain some information
                }
            }
        }
    }
    
    @objc func updateTime() {
        DispatchQueue.main.async {
            if self.player != nil {
                if let pos = self.player.currentItem?.currentTime(){
                    
                    let progressTime = self.player.currentItem?.currentTime().seconds
                    VideoViewController.player_time = progressTime! * 1000
                    LGEngageManagerImp.shared.onPlayerTimeUpdate(time: VideoViewController.player_time)
                    
                    let secondsText = String(format: "%02d", (Int(progressTime!) % 60)+1)
                    let minutesText = String(format: "%02d", Int(progressTime!) / 60)
                    var endSecondsText : String!
                    var endMinuteText : String!
                    
                    if((((Int(progressTime!) % 60)+10) >= 60)){
                        endSecondsText = String(format: "%02d", ((Int(progressTime!) % 60)+10)-60)
                        endMinuteText  = String(format: "%02d", (Int(progressTime!) / 60)+1)
                    }
                    else{
                        endSecondsText = String(format: "%02d", ((Int(progressTime!) % 60)+10))
                        endMinuteText  = String(format: "%02d", Int(progressTime!) / 60)
                    }
                    self.currentTimeLbl.text = "\(minutesText):\(secondsText)"
                    self.endTimeLbl.text     = "\(endMinuteText!):\(endSecondsText!)"
                }
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.cleanupVideoPlayer()
        let userId          = UserDefaults.getVideoQuizDetails()?.id
        let eventId         = UserDefaults.standard.string(forKey: "eventId")
        
        if let userId = userId, let eventId = eventId {
            LGEngageManagerImp.shared.disconnectFromEvent(user_id: userId, event_id: eventId)
        }
        
        UserDefaults.standard.set(nil, forKey: "eventId")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "rechabilityChangedNotification"), object: nil)
        UserDefaults.standard.set(true, forKey: "DisconnectedDueToNetwork")
        EngageEventHandler.isBackBtnPressed = true

        if player != nil {
            self.playerItem?.removeObserver(self, forKeyPath: "status", context: &playerItemContext)
        }
        
        navigationController?.popToRootViewController(animated: true)
    }

    
    func cleanupVideoPlayer() {
        if(player != nil) {
            self.player.pause()
            self.playerItem?.removeObserver(self, forKeyPath: "status", context: &playerItemContext)
            self.playerItem = nil
            self.player = nil
        }
        if(playerLayer != nil) {
            playerLayer.removeFromSuperlayer()
        }
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        NotificationCenter.default.removeObserver(self)
    }

}

extension DateComponentsFormatter {
    func difference(from fromDate: Date, to toDate: Date) -> String? {
        self.allowedUnits = [.year,.month,.weekOfMonth,.day]
        self.maximumUnitCount = 1
        self.unitsStyle = .full
        return self.string(from: fromDate, to: toDate)
    }
}
