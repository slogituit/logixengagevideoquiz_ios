//
//  OTPResponse.swift
//  LogixEngage
//
//  Created by Apple on 09/07/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import Foundation


class OTPResponse : NSObject, Codable {
    var status  : Int!
    var message : String!
    var userId  : String!
    
    init(status: Int, message: String, userId: String) {
        self.status = status
        self.message = message
        self.userId = userId
    }
}
