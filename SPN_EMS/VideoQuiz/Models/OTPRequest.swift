//
//  OTPRequest.swift
//  LogixEngage
//
//  Created by Apple on 09/07/19.
//  Copyright © 2019 Logituit. All rights reserved.
//

import Foundation

public class OtpRequest: NSObject {
    var action : String? = ""
    var otp    : String? = ""
    var userId : String? = ""
    
    init(action : String, otp: String, userId: String) {
        self.action = action
        self.otp = otp
        self.userId = userId
    }
    
    func toDict()-> [String: Any] {
        return ["action": self.action,
                "otp": self.otp,
                "userId": self.userId]
    }
}
