//
//  SPNEMSViewController.swift
//  SPN_EMS
//
//  Created by Prateek Kumar on 07/08/18.
//  Copyright © 2018 Sourcebits. All rights reserved.
//

import UIKit

@objc public class SPNEMSMainViewController: UIViewController {

    var channelId: Int
    var showId: Int
    var pageId: Int
    lazy var apiClient = { return ConfigAPI() }()

    @objc public init(channelId: Int, showId: Int, pageId: Int) { //, cpCustomerId: NSNumber, socialId: NSNumber, adId: NSNumber, deviceId: NSNumber, anonymousId: NSNumber) {
        self.channelId = channelId
        self.showId = showId
        self.pageId = pageId
        super.init(nibName: "SPNEMSMainViewController", bundle: Bundle(for: SPNEMSMainViewController.self))
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        getServiceConfig()
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }

    func getServiceConfig() {
        showActivityIndicator()
        let configRequest = ConfigRequest(channelId: 2, programId: 3, pageId: 0)
        apiClient.serviceConfig(configRequest: configRequest, completion: { [weak self] success, response, error in
            DispatchQueue.main.async {
                hideActivityIndicator()
                if let config = response {
                    UserDefaults.storeServiceConfig(configInfo: config)
                    self?.loadSplashScreen(config: config)
                } else {
                    if let errorMessage = error {
                        displayError(title: "Error", message: errorMessage)
                    }
                }
            }
        })
    }

    func loadSplashScreen(config: ConfigDetails) {

        if let splashController = SPN_EMS_Storyboard.instantiateViewController(withIdentifier: "\(SplashViewController.self)") as? SplashViewController {
            splashController.config = config
            splashController.dimissBlock = {
                self.remove(asChildViewController: splashController)
                self.loadSocialLoginView(config: config)
            }
            add(asChildViewController: splashController)
        }
    }

    func loadSocialLoginView(config: ConfigDetails) {

        if let user = UserDefaults.getCurrentUserDetails() {
            if user.mobileVerified == 1 {
                if let serviceContentViewController = SPN_EMS_Storyboard.instantiateViewController(withIdentifier: "\(ServiceContentViewController.self)") as? ServiceContentViewController {
                    let navController = UINavigationController(rootViewController: serviceContentViewController)
                    add(asChildViewController: navController)
                }
            } else {
                if let phoneNumberVc = SPN_EMS_Storyboard.instantiateViewController(withIdentifier: "\(PhoneNumberViewController.self)") as? PhoneNumberViewController {
                    let navController = UINavigationController(rootViewController: phoneNumberVc)
                    add(asChildViewController: navController)
                }
            }
        } else {
            if let socialLoginViewController = SPN_EMS_Storyboard.instantiateViewController(withIdentifier: "\(SocialLoginViewController.self)") as? SocialLoginViewController {
                socialLoginViewController.config = config
                let socialLoginNav = UINavigationController(rootViewController: socialLoginViewController)
                add(asChildViewController: socialLoginNav)
            }
        }
    }

    // MARK: - Add & Remove ChildView
    fileprivate func add(asChildViewController viewController: UIViewController) {

        // Add Child View Controller
        addChildViewController(viewController)

        // Add Child View as Subview
        view.addSubview(viewController.view)

        // Configure Child View
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }

    fileprivate func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)

        // Remove Child View From Superview
        viewController.view.removeFromSuperview()

        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }

}
