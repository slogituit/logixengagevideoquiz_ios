//
//  Constants.swift
//  ISPSDK
//
//  Created by Ganesh Kumar on 01/06/18.
//  Copyright © 2018 Sourcebits. All rights reserved.
//

import Foundation

let SPN_EMS_Bundle = Bundle(identifier: "com.sourcebits.SPN-EMS")!
let SPN_EMS_Storyboard = UIStoryboard(name: "SPNEMS", bundle: SPN_EMS_Bundle)
let DeviceType = "iOS"

let progressViewSeparatorPadding: CGFloat = 10
let progressViewDefaultCellRadius: CGFloat = 10
let progressViewHighlightCellRadius: CGFloat = 14
