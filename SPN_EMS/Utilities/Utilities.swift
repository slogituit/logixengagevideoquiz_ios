//
//  Utilities.swift
//  ISPSDK
//
//  Created by Ganesh Kumar on 04/06/18.
//  Copyright © 2018 Sourcebits. All rights reserved.
//

import UIKit
import CoreTelephony

let containerViewTag = 999
let DeviceUdid = UIDevice.current.identifierForVendor?.uuidString ?? ""
let DeviceOS = UIDevice.current.systemVersion

func LocalizedString(_ key: String) -> String {
    return NSLocalizedString(key, tableName: nil, bundle: SPN_EMS_Bundle, value: "", comment: "")
}

func showActivityIndicator() {

    guard var topController = UIApplication.shared.keyWindow?.rootViewController else { return }
    while topController.presentedViewController != nil {
        topController = topController.presentedViewController!
    }
    guard let rootView = topController.view else { return }

    if (rootView.viewWithTag(containerViewTag) != nil) {
        return
    }

    let container: UIView = UIView()
    container.tag = containerViewTag
    container.frame = rootView.frame
    container.center = rootView.center
    container.backgroundColor = UIColor.clear

    let loadingView: UIView = UIView()
    loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
    loadingView.center = rootView.center
    loadingView.backgroundColor = UIColor.activityIndicatorBackgroundColor
    loadingView.clipsToBounds = true
    loadingView.layer.cornerRadius = 10

    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
    activityIndicator.style = .whiteLarge
    activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)

    loadingView.addSubview(activityIndicator)
    container.addSubview(loadingView)
    rootView.addSubview(container)
    UIApplication.shared.beginIgnoringInteractionEvents()

    activityIndicator.startAnimating()
}

func hideActivityIndicator() {

    guard var topController = UIApplication.shared.keyWindow?.rootViewController else { return }
    while topController.presentedViewController != nil {
        topController = topController.presentedViewController!
    }
    guard let rootView = topController.view else { return }

    if UIApplication.shared.isIgnoringInteractionEvents {
        UIApplication.shared.endIgnoringInteractionEvents()
    }

    if (rootView.viewWithTag(containerViewTag) != nil) {
        rootView.viewWithTag(containerViewTag)?.removeFromSuperview()
    }
}
