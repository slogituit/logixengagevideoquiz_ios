////
////  Extensions.swift
////  ISPSDK
////
////  Created by Ganesh Kumar on 04/06/18.
////  Copyright © 2018 Sourcebits. All rights reserved.
////

extension UIColor {

    static let jacarta = UIColor(red:0.28, green:0.18, blue:0.42, alpha:1)
    static let activityIndicatorBackgroundColor = UIColor(red: 68.0/255.0, green: 68.0/255.0, blue: 68.0/255.0, alpha: 0.75)
    static let progressViewHighlightColor = UIColor(red: 0.23, green: 0.35, blue: 0.6, alpha: 1.0)
    static let progressViewDefaultColor = UIColor(red: 0.82, green: 0.84, blue: 0.89, alpha: 1.0)
    static let profileViewShadowColor = UIColor(red:0, green:0, blue:0, alpha:0.13)
}

extension Notification.Name {

    static let UnauthorizedNotificationName = Notification.Name("UNAUTHORIZED")

}

