//
//  UserDefaults.swift
//  SPN_EMS
//
//  Created by Prateek Kumar on 09/08/18.
//  Copyright © 2018 Sourcebits. All rights reserved.
//

import Foundation

let userDetailsKey = "CurrentUserDetails"
let configKey = "ConfigurationDetails"
let parentAppDetailsKey = "ParentAppDetails"
let profileDisplayCountkey = "ProfileDisplayCount"
let userDetailsForVideoQuizKey = "userDetailsForVideoQuiz"

extension UserDefaults {

    /// Storing and Getting the details coming from Parent App
    class func storeParentAppDetails(details: AppDetails) {

        let data  = NSKeyedArchiver.archivedData(withRootObject: details)
        let defaults = UserDefaults.standard
        defaults.set(data, forKey: parentAppDetailsKey)
    }

    class func getParentAppInfo() -> AppDetails? {

        if let data = UserDefaults().data(forKey: parentAppDetailsKey), let appDetails = NSKeyedUnarchiver.unarchiveObject(with: data) as? AppDetails {
            return appDetails
        }
        return nil
    }
    /// Storing and Getting the details for video quiz
    class func storeVideoQuizDetails(details: UserDetailsForQuiz) {

        let data  = NSKeyedArchiver.archivedData(withRootObject: details)
        let defaults = UserDefaults.standard
        defaults.set(data, forKey: userDetailsForVideoQuizKey)
    }

    class func getVideoQuizDetails() -> UserDetailsForQuiz? {

        if let data = UserDefaults().data(forKey: userDetailsForVideoQuizKey), let appDetails = NSKeyedUnarchiver.unarchiveObject(with: data) as? UserDetailsForQuiz {
            return appDetails
        }
        return nil
    }
}
