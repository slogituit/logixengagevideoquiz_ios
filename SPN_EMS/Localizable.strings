/* 
  Localizable.strings
  ISPSDK

  Created by Ganesh Kumar on 01/06/18.
  Copyright © 2018 Sourcebits. All rights reserved.
*/

"errorMessage"                          = "Something went wrong!";
"invalidJSON"                           = "Invalid JSON format";
"cancel"                                = "Cancel";
"error"                                 = "Error";
"success"                               = "Success";

"enterOtpMessage"                       = "Please enter the verification code sent to your phone number above.";

// Registration
"emailAddress"                          = "Email Address";
"fullName"                              = "Full Name";
"otp"                                   = "OTP";
"emailOtpDescription"                   = "Please enter the OTP so we can verify your email address.";
"mobileOtpDescription"                  = "Please enter the OTP so we can verify your mobile number.";
"mobileNumber"                          = "Mobile Number";

// Phone number
"invalidMobileError"                    = "Please enter a valid Mobile Number.";
"verifyMessage"                         = "By tapping Verify, a SMS may be sent.";
"verify"                                = "Verify";
"mobileNumber"                          = "Mobile Number";

"invalidOtpError"                       = "The code you entered is not valid. Please request a new one below.";
"locationServicesDisabledTitle"         = "Location Services Disabled";
"locationServicesDisabledMessage"       = "Please enable Location Services in Settings";
"Ok"                                    = "OK";

// Profile Screen
"next"                                  = "Next";
"skip"                                  = "Skip";
"done"                                  = "Done";

"usernameTitle"                         = "Your Name";
"usernameError"                         = "Enter valid Name";

"emailTitle"                            = "Email Address";
"emailError"                            = "Enter valid Email Address";

"genderTitle"                           = "Gender";
"genderError"                           = "Select valid Gender";

"birthdayTitle"                         = "Birthday";
"birthdayPlaceholder"                   = "00 / 00 / 0000";
"birthdayError"                         = "Select valid Birthday";

"ageRangeTitle"                         = "Age Range";
"ageRangeError"                         = "Select valid Age Range";

"relationshipStatusTitle"               = "Relationship Status";
"relationshipStatusError"               = "Select valid Relationship Status";

"stateTitle"                            = "State";
"stateError"                            = "Select valid State";

"cityTitle"                             = "City";
"cityError"                             = "Select valid City";

// Picture Consent
"pictureConsentMessage"                 = "ALLOW THIS AVATAR TO BE USED IN ";
"thisProgram"                           = "THIS PROGRAM";

// Verify Mobile
"requestNewCodeInOneMin"                = "Request new code in 01:00";
"requestNewCodeIn"                      = "Request new code in ";

// WebView
"takeAVideo"                            = "Take a video";
"takeAPhoto"                            = "Take a photo";
"chooseFromGallery"                     = "Choose from gallery";
"fileUploadSuccess"                     = "File upload successfully";
"fileUploadSizeExceeds"                 = "File exceeds the maximum allowed limit.";
"uploadStatusSuccess"                   = "Success";
"uploadStatusFailure"                   = "Failure";

// GA Analytics Events
"screenView"                            = "screenview";
"notApplicable"                         = "NA";
"splashScreen"                          = "splash screen";
"event"                                 = "event";
"loginScreen"                           = "login screen";
"loginCategory"                         = "login";
"locationCategory"                      = "location consent";
"loginSkipCategory"                     = "login skip";
"termsOfUseScreen"                      = "terms of use screen";
"pictureConsentScreen"                  = "picture consent screen";
"cameraAccessCategory"                  = "camera access";
"phoneNumberScreen"                     = "phone number screen";
"OTPScreen"                             = "OTP screen";
"EMSInteractiveScreen"                  = "EMS interactive screen";
"introVideoScreen"                      = "intro video screen";
"introVideoSkip"                        = "intro video skip";
"click"                                 = "click";
"userDetailsSkip"                       = "user details skip";
"skipStep"                              = "skip step";
"identify"                              = "identify";
"userIdentityCategory"                  = "User Identity";
"t&cCategory"                           = "T&C";

// Segment Analytics Events
"screenName"                            = "screen_name";
"homeButtonTapped"                      = "Home Button Tapped";
"loginType"                             = "login_type";
"userSignedIn"                          = "User Signed In";
"value"                                 = "value";
"locationConsentQuestionAnswered"       = "Location Consent Question Answered";
"pictureConsentQuestionAnswered"        = "Picture Consent Question Answered";
"eventType"                             = "1";
"userType"                              = "2";
"verified"                              = "verified";
"identifyUser"                          = "Identify User";
"phone"                                 = "phone";
"channelId"                             = "channelId";
"showId"                                = "showId";
"userSkippedIntroVideo"                 = "User Skipped Intro Video";
"step"                                  = "step";
"skip_type"                             = "skip_type";
"userSkippedUserDetailsInput"           = "User Skipped User Details Input";
"user_id"                               = "user_id";
"username"                              = "username";
"email"                                 = "email";
"gender"                                = "gender";
"birthday"                              = "birthday";
"age_range"                             = "age_range";
"marital_status"                        = "marital_status";
"state"                                 = "state";
"city"                                  = "city";
"platform"                              = "platform";
"platformType"                          = "iOS";
